# Uncomment the next two lines to enable the admin:
from articles.views import ArticleList, ArticleView
from company.views import counter, RegisterView, CompanyView, CompanyList, \
    AddRespond, FrontView, StatView, TenderAdd, \
    RespondList, OfferView, EditProfile, Avatar, \
    OrdersSend, CompanyScript, DeliveryZonePriceView, \
    CompetitionView, CompetitionDetailView, \
    CompetitionListView
from company.ajax import poll_post, vote
from django.contrib import admin
from django.contrib.staticfiles.urls import static
from django.views.generic.base import RedirectView
from cms.sitemaps import CMSSitemap

from news.views import NewsList, NewsView, AddComment
from apps.comments.views import AddLike

from awards.views import FrontAwards, VoteAwards, AddVote,\
                         RequestForm, SuccessRequest
import settings

from adminplus.sites import AdminSitePlus
from django.conf.urls.defaults import patterns, include, url
from sitemap import CompanyObjects


sitemaps = {
    'objects': CompanyObjects,
}
admin.site = AdminSitePlus()
admin.autodiscover()


urlpatterns = patterns('',
    (r'^ckeditor/', include('ckeditor.urls')),
    (r'^admin/', include(admin.site.urls)),

    url(r'^order/', include('order.urls', 'order')),
    url(r'^', include('company.urls')),
    url(r'^awards/$', FrontAwards.as_view(), name='awards'),
    url(r'^awards/vote/(?P<type>[0-9A-Za-z-_.//]+)/$', VoteAwards.as_view(), name='awards_vote'),
    url(r'^awards/request/$', RequestForm.as_view(), name='awards_request'),
    url(r'^awards/request/success/$', SuccessRequest.as_view(), name='awards_success'),
    url(r'^awards/add/vote/$', AddVote.as_view(), name='awards_add_vote'),
    url(r'^tender/$', TenderAdd.as_view(), name='tender'),

    url(r'^like/add/(?P<pk>\d+)/$', AddLike.as_view(), name="add_like"),
    url(r'^news/$', NewsList.as_view(), name='news_list'),
    url(r'^news/comment/$', AddComment.as_view(), name='add_comment'),
    url(r'^news/(?P<pk>[0-9A-Za-z-_.//]+)/$', NewsView.as_view(), name='news'),
    url(r'^articles/$', ArticleList.as_view(), name='article_list'),
    url(r'^articles/(?P<pk>[0-9A-Za-z-_.//]+)/$', ArticleView.as_view(), name='article'),
    # url(r'^category/(?P<pk>[0-9A-Za-z-_.//]+)/', CategoryView.as_view(template_name='category.html')),
    # url('^api/buy/product/$', CartAddItemView.as_view(action='post'), name='add_to_cart' ),
    # url(r'^shop/cart/$', CartView.as_view(), name='cart'),
    # url(r'^shop/checkout/$', CheckoutView.as_view(template_name='checkout.html'), name='checkout_selection'),

    # (r'^shop/', include(shop_urls)),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
# (r'^accounts/logout/$', RedirectView.as_view(url='/')),
    url(r'^accounts/', include('registration.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^robots.txt', include('robots.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'', include('comments.urls')),
    url(r'^landging/', include('cms_page.urls')),
    (r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    # (r'^sitemap/$', 'django.contrib.sitemaps.views.sitemap', {
    #     'sitemaps': {'cmspages': CMSSitemap},
    #     'mimetype': 'text/html',
    #     'template_name': 'inclusion/site_map.html'
    #     }),
    url(r'^', include('cms.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
