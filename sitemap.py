from django.contrib.sitemaps import Sitemap

from company.models import Company


class CompanyObjects(Sitemap):
    priority = 0.5

    def items(self):
        return Company.objects.filter(active=True)

    def location(self, obj):
        return obj.get_absolute_url()
