#!/usr/bin/env bash
dtime=`date +%Y-%m-%d-%s`
backup_folder=/home/dostmarket_new/backup/
mysql_user=new_dostmarket
mysql_password=123321
mysql_database_name=new_dostmarket
mysqldump --user=$mysql_user --password=$mysql_password $mysql_database_name | gzip > $backup_folder/$dtime-$i.sql.gz
