# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.shortcuts import get_object_or_404


from .models import Page


class PageView(TemplateView):
    template_name = 'cms_page/page.html'

    def get_context_data(self, **kwargs):
        ctx = super(PageView, self).get_context_data(**kwargs)
        ctx['page'] = get_object_or_404(Page, slug=self.kwargs.get('page_slug'))
        return ctx


class PageListView(ListView):
    model = Page
    context_object_name = 'pages'