# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Page.color'
        db.add_column('cms_page_page', 'color',
                      self.gf(u'colorful.fields.RGBColorField')(default=1),
                      keep_default=False)


        # Changing field 'Page.description'
        db.alter_column('cms_page_page', 'description', self.gf('django.db.models.fields.TextField')(max_length=2500, null=True))

    def backwards(self, orm):
        # Deleting field 'Page.color'
        db.delete_column('cms_page_page', 'color')


        # Changing field 'Page.description'
        db.alter_column('cms_page_page', 'description', self.gf('ckeditor.fields.RichTextField')(max_length=2500, null=True))

    models = {
        'cms_page.page': {
            'Meta': {'object_name': 'Page'},
            'background_image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'color': (u'colorful.fields.RGBColorField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2500', 'null': 'True', 'blank': 'True'}),
            'h1': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'h2bottom': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'h2top': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'seo': ('ckeditor.fields.RichTextField', [], {'max_length': '2500', 'null': 'True', 'blank': 'True'}),
            'seo_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cms_page']