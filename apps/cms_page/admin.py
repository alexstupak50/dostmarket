# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import Page


class CourseAdmin(admin.ModelAdmin):
    model = Page
    prepopulated_fields = {"slug": ("h1",)}
    list_display = ('title', 'slug', 'is_active')
    search_fields = ('title', 'slug', 'h1')


admin.site.register(Page, CourseAdmin)