# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from .views import PageView, PageListView


urlpatterns = patterns('',
    url(r'^(?P<page_slug>[\w\-]+)/$', PageView.as_view(), name='page'),
    url(r'^page/list/$', PageListView.as_view(), name='page_list'),
)


