# -*- coding: utf-8 -*-
from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail.fields import ImageField
from colorful.fields import RGBColorField
from django.core.urlresolvers import reverse

from company.models import get_file_path


class Page(models.Model):
    title = models.CharField(verbose_name=u'Title', max_length=255, blank=True, null=True,
                             help_text=u'Переписать то, что будет изображено в заголовке браузера или закладках.')
    h1 = models.CharField(verbose_name=u'Заголовок 1', max_length=255, blank=True, null=True)
    slug = models.SlugField(u'url')
    h2top = models.CharField(verbose_name=u'Подзаголовок 1', max_length=255, blank=True, null=True)
    h2bottom = models.CharField(verbose_name=u'Подзаголовок 2', max_length=255, blank=True, null=True)
    link = models.CharField(verbose_name=u'Ссылка кнопки', max_length=255, blank=True, null=True,
                            help_text=u'Пример: /order/form/2',)
    background_image = ImageField(verbose_name=u'Фон', upload_to=get_file_path, null=True, blank=True)
    color = RGBColorField(u'Цвет под картинкой', default='#499fc2')
    is_active = models.BooleanField(u'Активна страница', default=True)
    seo = RichTextField(verbose_name=u'Seo', max_length=2500, blank=True, null=True)
    keywords = models.TextField(
        verbose_name=u'Мета Keywords', blank=True, null=True,
        help_text=u'Список ключевых слов, разделённых запятыми, иногда используемый поисковыми роботами.')
    description = models.TextField(
        verbose_name=u'Meta Description', max_length=2500, blank=True, null=True)
    seo_title = models.CharField(max_length=255, verbose_name=u'Seo тайтл', blank=True, null=True,)

    class Meta:
        verbose_name_plural = u'Страницы новые'
        verbose_name = u'Страницу'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('page', kwargs={'page_slug': self.slug})