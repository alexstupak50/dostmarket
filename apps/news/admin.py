# vim:fileencoding=utf-8
from django.contrib import admin
from news.models import News, Category, Comment
from sorl.thumbnail import ImageField
from sorl.thumbnail.admin.current import AdminImageWidget

class NewsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'date']
    list_filter = ['category']
    list_display_links = ['title']
    filter_horizontal = ['category']
    formfield_overrides = {
        ImageField: {'widget': AdminImageWidget},
        }

class CommentAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'news', 'created', 'moderate']
    list_filter = ['news', 'moderate']
    list_display_links = ['name']
    list_editable = ['moderate']

admin.site.register(News, NewsAdmin)
admin.site.register(Category)
# admin.site.register(Comment, CommentAdmin)