# vim:fileencoding=utf-8
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from news.forms import AddCommentForm
from news.models import News, Comment
from django.contrib.messages.constants import ERROR, INFO
from apps.articles.models import Article


class NewsList(ListView):
    template_name = "news/news_list.html"
    paginate_by = 10
    model = News
    context_object_name = 'list'

    def get_context_data(self, **kwargs):
        ctx = super(NewsList, self).get_context_data(**kwargs)
        ctx['articles'] = Article.objects.all()[:7]
        # News.objects.all()
        # from news.models import News
        return ctx

    def get_queryset(self):
        tag = self.request.GET.get('tag', False)
        if tag:
            queryset = self.model.objects.filter(category__name=tag)
        else:
            queryset = self.model.objects.all()
        return queryset


class NewsView(DetailView):
    model = News
    template_name = 'news/news.html'
    context_object_name = 'news'

    def get_context_data(self, **kwargs):
        ctx = super(NewsView, self).get_context_data(**kwargs)
        ctx['form'] = AddCommentForm(initial={'news': ctx['object'].id})
        return ctx


class AddComment(CreateView):
    model = Comment
    form_class = AddCommentForm
    template_name = 'news/add_comment.html'

    def get_success_url(self):
        news_id = self.request.POST.get('news', False)
        if news_id:
            url = get_object_or_404(News, id=news_id).get_absolute_url()
        else:
            url = '/'
        messages.add_message(self.request, INFO, u'Комментарий добавлен, в ближайшее время он будет рассмотрен администрацией сайта')
        return url
