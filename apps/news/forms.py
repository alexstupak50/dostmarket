from captcha.fields import CaptchaField
from django.forms.models import ModelForm
from django import forms
from news.models import Comment

__author__ = 'Ivan'


class AddCommentForm(ModelForm):
    class Meta(object):
        model = Comment
        exclude = ['created', 'moderate']

    captcha = CaptchaField()

