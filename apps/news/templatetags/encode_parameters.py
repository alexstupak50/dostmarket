from distutils.command.clean import clean
from urllib import urlencode
from django import template
import string

from apps.articles.models import Article
from apps.news.models import News

register = template.Library()


@register.inclusion_tag('articles/left_nav.html',
                        takes_context=True)
def left_nav(context):
    return {
        'request': context['request'],
        'news': News.objects.all()[:7],
        'articles': Article.objects.all()[:7],
    }

def encode_parameters(value,arg):
    s = {}
    e_s = {}
    s.update(value)
    for k, v in s.items():
        if not isinstance(v[0], unicode):
            v[0] = v[0].encode('utf-8')
        e_s[k] = v[0]
    s = e_s
    if s.__contains__('page'):
        s.__setitem__('page', arg)
    else:
        s.__setitem__('page', arg)
    s = urlencode(s,doseq=True)
    return s

register.filter('encode_parameters', encode_parameters)


  