# vim:fileencoding=utf-8

from django.utils import simplejson
from django.shortcuts import get_object_or_404
from company.models import Company, Poll, Vote
from django.forms.models import ModelForm
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Sum
import json
import uuid
from django import http, forms
from django.views.generic.base import TemplateView
from django.core.mail import send_mail
from django.conf import settings

class JSONResponseMixin(object):
    def render_to_response(self, context):
        "Returns a JSON response containing 'context' as payload"
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        "Construct an `HttpResponse` object."
        return http.HttpResponse(content,
            content_type='application/json; charset=UTF-8',
            **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)


class PollForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField(max_length=255)

def poll_post(request):
    if request.is_ajax():
        form = PollForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            companies = request.session.get('_poll_companies', [])
            if request.session.get('_poll_count', False):
                if not request.POST.get('id', None) in companies:
                    request.session['_poll_count'] = int(request.session.get('_poll_count', 0)) + 1
            else:
                request.session['_poll_count'] = 1
            if request.session['_poll_count'] < 3 and not request.POST.get('id', None) in companies:
                companies.append(request.POST.get('id', None))
                request.session['_poll_companies'] = companies
                company = get_object_or_404(Company, id=int(request.POST.get('id', None)))
                poll_uuid = uuid.uuid4().get_hex()
                poll = Poll.objects.create(company=company, choice=request.POST.get('choice', None), name=data['name'], email=data['email'], uuid=poll_uuid)
                send_mail(u'Подтверждение голосования', u'Здравствуйте. Вы проголосовали на сайте dostmarket.ru. Для подтверждения Вашего голоса пожалуйста пройдите по ссылке http://dostmarket.ru/poll/confirm/%s/' % poll_uuid, settings.DEFAULT_FROM_EMAIL, [data['email']])
                company = get_object_or_404(Company, id=int(request.POST.get('id', None)))
                message = {
                    'valid':True,
                    'pro':company.pro,
                    'con':company.con,
                    'message':True,
                }
            else:
                if request.session['_poll_count'] < 3:
                    message = {
                        'valid':True,
                        'message':False,
                        'limit':True,
                        }
                else:
                    message = {
                        'valid':True,
                        'message':False,
                        'limit':False,
                    }
        else:
            message = {
                'valid':False
                }
    else:
        message = u"No XHR"
    message = simplejson.dumps(message)
    return HttpResponse(message, 'application/json')


def vote(request):
    company = Company.objects.get(pk=request.GET.get('company'))

    if request.user.is_authenticated() and request.is_ajax() and \
            request.user.orders.filter(performer=company.user, status=2).exists():
        if not Vote.objects.filter(company=company,
                                   user=request.user,
                                   vote_type=request.GET.get('type')).exists():
            p = Vote()
            p.company_id = request.GET.get('company')
            p.user = request.user
            p.vote_type = request.GET.get('type')
            p.scope = request.GET.get('score')
            p.save()
            scope = int(request.GET.get('score'))
            vote_type = int(request.GET.get('type'))
            try:
                if vote_type == 1:
                    company.total_quality_service = (company.total_quality_service + scope) / 2.
            except ZeroDivisionError:
                pass

            try:
                if vote_type == 2:
                    company.total_conform_terms_delivery = (company.total_conform_terms_delivery + scope) / 2.
            except ZeroDivisionError:
                pass

            try:
                if vote_type == 3:
                    company.total_convenience_location_offices = (company.total_convenience_location_offices+ scope) / 2.
            except ZeroDivisionError:
                pass

            try:
                if vote_type == 4:
                    company.total_neatness_politeness_courier = (company.total_neatness_politeness_courier+scope) / 2.
            except ZeroDivisionError:
                pass

            try:
                if vote_type == 5:
                    company.total_price_quality = (company.total_price_quality+scope) / 2.
            except ZeroDivisionError:
                pass

            company.total_scope = (company.total_quality_service + company.total_conform_terms_delivery + company.total_convenience_location_offices + company.total_neatness_politeness_courier + company.total_price_quality) / float(5)
            company.save()
        return HttpResponse(str('%.2f' % company.total_scope).replace('.', ','))
