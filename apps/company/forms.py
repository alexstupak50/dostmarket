# vim:fileencoding=utf-8
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField
from django.shortcuts import get_object_or_404
from company.models import Company, Respond, Tender, DeliveryZonePrice,\
                           DeliveryCompetition, PaymentType,\
                           CompanyPayment, CompanyPaymentField
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.html import strip_tags, mark_safe
from django.utils.translation import ugettext_lazy as _
from django.forms import Field
from django.core.exceptions import ObjectDoesNotExist
from django.forms.models import ModelForm
import urllib
import urllib2
from order.utils import get_object_or_none
import json
from smart_selects.widgets import ChainedSelect
from geo.models import Country, City, Region
from registration.forms import RegistrationForm
from django import forms
from django.forms.widgets import TextInput
from pytils.translit import slugify
from order.forms import RemoveEmptyLabelMixin
from order.models import OrderCategory
from order.utils import check_address
from .models import TYPES_CARRIER, COMPANY_TYPE
from apps.geo.models import Region, City
from .widget import RadioSelectCustom
from smart_selects.form_fields import ChainedModelChoiceField
from apps.order.utils import RemoveEmptyLabelMixin


EMPTY_VALUES = (None, '')

attrs_dict = {'class': 'required'}


class CityCleanMixin(object):
    def clean_city2(self):
        cd = self.cleaned_data
        url = 'http://maps.googleapis.com/maps/api/geocode/json'
        country = self.cleaned_data['country']
        city = self.cleaned_data['city2']
        if not country:
            return None
        if len(city.split(',')) > 1 and u'город' in city.split(',')[1]:
            city = city.split(',')[0]
        data = ' '.join([country.name, city])
        params = {
            'sensor': 'false',
            'address': data,
            }
        # проверяем город на юникод, если всё нормально передаём в гет запрос и ищем в гугле
        try:
            params['address'] = unicode(params['address']).encode('utf-8')
        except (UnicodeEncodeError, UnicodeDecodeError):
            raise forms.ValidationError(
                {'city2': [_(u'Недопустимые символы в названии города')]})
        encoded_params = urllib.urlencode(params)

        url = url + '?' + encoded_params + '&language=ru'
        try:
            response = urllib2.urlopen(url)
            result = json.loads(response.read())
            if not result['results']:
                raise forms.ValidationError(
                    {'city2': [_(u'Произошла ошибка, возможно вы указали не существующий адрес.')]})
                cd['city2'] = None
                return cd
        except urllib2.URLError:
            raise forms.ValidationError(
                {'city2': [_(u'Произошла ошибка, возможно вы указали не существующий адрес.')]})
        try:
            for addr in result['results'][0]['address_components']:
                if addr['types'][0] == "administrative_area_level_1":
                    cd['region'] = addr['long_name']
                if addr['types'][0] == "locality":
                    cd['city2'] = addr['long_name']
        except KeyError as e:
            raise forms.ValidationError(
                {'city2': [_(u'Произошла ошибка, возможно вы указали не существующий адрес.')]})
        try:
            cd['country'] = country
            region = get_object_or_none(Region, country=country, name=cd.get('region', None))
            city = City.objects.filter(name=cd['city2'], region=region)
            if city:
                cd['city2'] = city[0]
                cd['region'] = region
            else:
                # гугл может не правильно отдавать регион для городов, поему даем шанс
                # найти регион через город
                city = City.objects.filter(name=cd['city2'])
                if city:
                    cd['city2'] = City.objects.filter(name=cd['city2'])[0]
                    if cd['city2'].region:
                        cd['region'] = cd['city2'].region
                    else:
                        cd['region'] = None
            if not cd['city2']:
                cd['city2'] = None
            if not city:
                raise forms.ValidationError(
                    {'city2': [_(u'Произошла ошибка, возможно вы указали не существующий адрес.')]})
        except ObjectDoesNotExist:
            raise forms.ValidationError(
                {'city2': [_(u'Произошла ошибка, возможно вы указали не существующий адрес.')]})
        return cd['city2']


class Registration(RegistrationForm):
    first_name = forms.CharField(max_length=250, label=u'Имя', required=True)

    def __init__(self, *args, **kwargs):

        super(Registration, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.HiddenInput()
        self.fields['username'].required = False

    def clean_email(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.

        """
        existing = User.objects.filter(
            Q(username__iexact=self.cleaned_data['email']) | Q(email=self.cleaned_data['email']) \
            | Q(username=self.cleaned_data['username']) | Q(username=self.cleaned_data['email']))
        if existing.exists():
            raise forms.ValidationError(_(u"Пользователь с таким email уже существует"))
        else:
            return self.cleaned_data['email']


class HoneypotWidget(TextInput):
    is_hidden = True

    def __init__(self, attrs=None, html_comment=False, *args, **kwargs):
        self.html_comment = html_comment
        super(HoneypotWidget, self).__init__(attrs, *args, **kwargs)
        if not self.attrs.has_key('class'):
            self.attrs['style'] = 'display:none'

    def render(self, *args, **kwargs):
        value = super(HoneypotWidget, self).render(*args, **kwargs)
        if self.html_comment:
            value = '<!-- %s -->' % value
        return value


class HoneypotField(Field):
    widget = HoneypotWidget

    def clean(self, value):
        if self.initial in EMPTY_VALUES and value in EMPTY_VALUES or value == self.initial:
            return value
        raise ValidationError('Anti-spam field changed in value.')


class CompanyInfo(CityCleanMixin, ModelForm):
    custom_errors = {
        'required': u'Укажите местонахождение вашей компании'
    }
    agree = forms.BooleanField()
    type_carrier = forms.IntegerField(
        label=u'Что перевозим', widget=forms.RadioSelect(choices=TYPES_CARRIER))
    # country = forms.CharField(required=False)
    # country_services1 = forms.ModelChoiceField(queryset=Country.objects.all(),
    #                                           widget=RadioSelectCustom)
    type = forms.CharField(label=u'тип', widget=forms.RadioSelect(choices=COMPANY_TYPE))
    # city2 = forms.CharField(required=False)

    class Meta(object):
        model = Company
        exclude = ['user', 'head2', 'site_active', 'logo',
                   'alternative', 'pro', 'con', 'comments', 'active',
                   'title', 'h1', 'h2', 'seo_description', 'seo_keywords',
                   'ip', 'city', 'city2', 'total_scope', 'total_price_quality',
                   'total_neatness_politeness_courier', 'total_convenience_location_offices',
                   'total_conform_terms_delivery', 'total_quality_service']
        widgets = {
            'country_services': RadioSelectCustom,
            }

    def __init__(self, *args, **kwargs):
        how = kwargs.pop('how', None)
        super(CompanyInfo, self).__init__(*args, **kwargs)
        self.fields['country'].error_messages = self.custom_errors
        self.fields['site'].required = False
        self.fields['categories'].widget = forms.CheckboxSelectMultiple()
        self.fields['categories'].choices = [
            (e.pk, e.icon, e.name) for e in OrderCategory.objects.all()\
                .only('pk', 'icon', 'name')]
        if how == 'company':
            for field in ['country_services']:
                self.fields[field].required = True
        for field in ['country']:
            self.fields[field].required = True
        if self.data.get('categories'):
            self.fields['categories'].initial = [int(pk) for pk in self.data.getlist('categories')]

    def clean_description(self):
        descriprion = self.cleaned_data['description']
        return strip_tags(descriprion)


class CompanyEdit(ModelForm):
    custom_errors = {
        'required': u'Укажите местонахождение вашей компании'
    }
    first_name = forms.CharField(label=u'Имя', required=False)
    type_carrier = forms.IntegerField(
        label=u'Тип перевозчика', widget=forms.RadioSelect(choices=TYPES_CARRIER), required=False)
    country2 = forms.ModelChoiceField(
        queryset=Country.objects.all(), required=False)
    region2 = ChainedModelChoiceField(app_name='geo',
                                      model_name='Region',
                                      chain_field='country2',
                                      model_field='country',
                                      auto_choose=False,
                                      show_all=False,
                                      queryset=Region.objects.all(),
                                      required=False,
    )

    class Meta(object):
        model = Company
        exclude = ['user', 'head2', 'site_active', 'logo',
                   'alternative', 'pro', 'con', 'comments', 'active',
                   'title', 'h1', 'h2', 'seo_description', 'seo_keywords',
                   'ip', 'email', 'type', 'region', 'country',
                   'total_scope', 'total_price_quality', 'total_neatness_politeness_courier',
                   'total_convenience_location_offices', 'total_conform_terms_delivery', 'total_quality_service']

        widgets = {
            'country_services': forms.CheckboxSelectMultiple,
            }

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        if instance and instance.description:
            kwargs.update(initial={
                'description': mark_safe(strip_tags(instance.description)),
            })
        super(CompanyEdit, self).__init__(*args, **kwargs)
        self.fields['country2'].error_messages = self.custom_errors
        self.fields['categories'].widget = forms.CheckboxSelectMultiple()
        self.fields['phone'].required = False
        # if 'country2' not in self.initial:
        self.initial['country2'] = instance.country
        if instance.region:
            self.fields['region2'].initial = instance.region
        self.fields['categories'].choices = [
            (e.pk, e.icon, e.name) for e in OrderCategory.objects.all().only('pk', 'icon', 'name')]
        for field in ['country2',]:
            self.fields[field].required = True
        if kwargs.get('instance'):
            if args[0]:
                self.fields['categories'].initial = [int(x) for x in args[0].getlist('categories')]
            else:
                self.fields['categories'].initial = [
                    e.pk for e in kwargs['instance'].categories.all().only('pk')]

    def clean_description(self):
        return strip_tags(self.cleaned_data['description'])

    def save(self, commit=True):
        data = self.cleaned_data
        obj = super(CompanyEdit, self).save(commit=False)
        obj.categories.clear()
        obj.categories.add(*data['categories'])
        obj.country = data['country2']
        obj.region = data['region2']
        if commit:
            obj.save()
        return obj


class AddRespondForm(ModelForm):
    parent = forms.IntegerField(required=False)
    company = forms.IntegerField()
    last = HoneypotField()
    # captcha = CaptchaField()

    class Meta(object):
        model = Respond
        exclude = ['created', 'moderate', 'ip', 'uuid', 'confirm', 'parent', 'company']
        initial = {'mark': 'neitral'}

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(AddRespondForm, self).__init__(*args, **kwargs)
        if self.user:
            del self.fields['email']
            del self.fields['name']

    def clean_company(self):
        return get_object_or_404(Company, id=self.cleaned_data['company'])
        # def clean_order(self):
    #     order = self.cleaned_data["order"]
    #     if self.cleaned_data['mark'] == 'negative' and not order:
    #         raise forms.ValidationError(u'Поле обязательно для заполнения')
    #     return order


class TenderForm(ModelForm):
    captcha = CaptchaField()

    class Meta(object):
        model = Tender
        exclude = ['created']


class CompetitionForm():
    cargo = forms.IntegerField(label=u'Что перевозим', widget=forms.RadioSelect(choices=DeliveryCompetition.CARGO))
    weight = forms.IntegerField(label=u'Вес', widget=forms.Select(choices=DeliveryCompetition.WEIGHT))
    width = forms.CharField(label=u'Ширина', max_length=255, required=False)
    height = forms.CharField(label=u'Высота', max_length=255, required=False)
    length = forms.CharField(label=u'Длина', max_length=255, required=False)
    delivery_date = forms.DateField(label=u'Дата доставки', input_formats=["%Y-%m-%d"])
    delivery_time = forms.CharField(label=u'Желаемое время доставки', max_length=20, required=False)
    from_address = forms.CharField(label=u'Адрес', max_length=100, widget=forms.TextInput(attrs={'placeholder': u'г.Москва, улица Сретенка, дом 17, корп.2, кВ (офис) 34'}))
    from_fio = forms.CharField(label=u'ФИО отправителя / Организация', max_length=100)
    from_phone = forms.CharField(label=u'Телефон', max_length=50)
    from_email = forms.EmailField(label=u'Ваш E-mail')
    to_address = forms.CharField(label=u'Адрес', max_length=100, widget=forms.TextInput(attrs={'placeholder': u'г.Москва, улица Сретенка, дом 17, корп.2, кВ (офис) 34'}))
    to_fio = forms.CharField(label=u'ФИО получателя / Организация', max_length=100)
    to_phone = forms.CharField(label=u'Телефон', max_length=50)
    cargo_description = forms.CharField(label=u'Текст отзыва', widget=forms.Textarea, max_length=2500, required=False)
    urgency = forms.BooleanField(label=u'Срочность', required=False)


class DeliveryCompetitionForm(forms.ModelForm):
    class Meta:
        model = DeliveryCompetition
        exclude = ('status',)


class BaseProfile(ModelForm):
    phone = forms.CharField(label=u'Телефон', required=False)

    class Meta(object):
        model = User
        exclude = ['password', 'username', 'is_staff', 'is_active',
                   'is_superuser', 'user_permissions', 'groups',
                   'last_login', 'date_joined', 'email']

class ChangeAvatar(forms.ModelForm):
    class Meta(object):
        model = Company
        fields = ['logo']


class PollForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField(max_length=255)


class DeliveryZonePriceForm(forms.ModelForm):

    class Meta:
        model = DeliveryZonePrice
        exclude = ('company',)


def payment_form_generator(company):
    for payment in PaymentType.objects.all():
        yield PaymentProfileForm(payment=payment, company=company)


class PaymentProfileForm(forms.Form):
    SHOW_FIELDS = (
        (True, u'Да'),
        (False, u'Нет'),
    )

    def __init__(self, payment, company, *args, **kwargs):
        super(PaymentProfileForm, self).__init__(*args, **kwargs)
        self.payment = payment
        self.company = company
        self.form_name = payment.name
        self.form_pk = payment.pk

        if self.payment.show_fields:
            self.fields['show_fields'] = forms.TypedChoiceField(label='',
                                                                coerce=lambda x: x == 'True',
                                                                choices=self.SHOW_FIELDS,
                                                                widget=forms.RadioSelect())
        else:
            for field in self.payment.paymenttypefield_set.all():
                self.fields[slugify(field.name)] = forms.CharField(label=field.name,
                                                                   max_length=100)

        try:
            company = CompanyPayment.objects.get(company=self.company,
                                                 payment_type=self.payment)

            if self.payment.show_fields:
                if company.show_fields:
                    self.fields['show_fields'].initial = True
                else:
                    self.fields['show_fields'].initial = False
            else:
                for field in company.companypaymentfield_set.all():
                    for field_name in field.payment.payment_type.paymenttypefield_set.all():
                        self.fields[slugify(field_name.name)].initial = field.value

        except CompanyPayment.DoesNotExist:
            if self.payment.show_fields:
                self.fields['show_fields'].initial = False

    def clean(self):
        for field in self.payment.paymenttypefield_set.all():
            if not self.cleaned_data.get(slugify(field.name)):
                raise forms.ValidationError(u'Все поля обязательны')
        return self.cleaned_data

    def save(self):
        payment, created = CompanyPayment.objects.get_or_create(company_id=self.company.pk,
                                                                payment_type=self.payment)

        for field in self.payment.paymenttypefield_set.all():
            if self.cleaned_data.get(slugify(field.name)):
                try:
                    p = CompanyPaymentField.objects.get(payment=payment)
                except CompanyPaymentField.DoesNotExist:
                    p = CompanyPaymentField()
                    p.payment = payment

                p.value = self.cleaned_data.get(slugify(field.name))
                p.save()

        if self.payment.show_fields:
            payment.show_fields = self.cleaned_data['show_fields']
            payment.save()


class FeedBackForm(forms.Form):
    subject = forms.CharField(max_length=255, required=True, label=_(u'тема сообщения'))
    email = forms.EmailField(label=_(u'электронная почта'))
    text = forms.CharField(widget=forms.Textarea, label=_(u'сообщение'), required=True)
    name = forms.CharField(max_length=255, required=True, label=_(u'Ваше имя'))