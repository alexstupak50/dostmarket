# -*- coding: utf-8 -*-
import uuid
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand

from apps.company.models import Company
from django.utils.html import mark_safe, strip_tags
from order.models import OrderCategory, Route
from geo.models import Country


class Command(BaseCommand):
    def handle(self, *args, **options):
        action = args[0]
        if action == 'create_route':
            category = OrderCategory.objects.all()
            companies = Company.objects.filter(categories__isnull=True)
            for company in companies.filter(type='transport'):
                company.categories.add(*category)
            for company in companies.exclude(type='transport'):
                company.categories.add(category.get(id=16))
            for company in Company.objects.filter(country__isnull=True):
                company.country = Country.objects.get(id=1)
                company.save()
            Route.objects.all().delete()
            for company in Company.objects.all():
                try:
                    route = Route.objects.create(
                        user=company.user,
                        country=company.country,
                        region=company.region or None,
                        only_category=True,
                        default=True
                        )
                    route.categories.add(*company.categories.all())
                    company.send_mail = True
                    company.save()
                except:
                    print 'error %s' % company

        elif action == 'strip_name':
            for company in Company.objects.all():
                company.name = company.name.replace('"', '').strip()
                company.description = mark_safe(strip_tags(company.description))
        elif action == 'send_mail':
            for company in Company.objects.all():
                # company.name = company.name.replace('"', '').strip()
                # company.description = mark_safe(strip_tags(company.description))
                company.send_mail = True
                company.save()
        elif action == 'auto_uuid':
            for company in Company.objects.all():
                # company.name = company.name.replace('"', '').strip()
                # company.description = mark_safe(strip_tags(company.description))
                company.uuid = uuid.uuid4().get_hex()
                company.save()

        elif action == 'remove_logo':
            """
            if logo do not exist - remove
            """
            for company in Company.objects.all():
                if not default_storage.exists(company.logo):
                    company.logo = None
                    company.save()


