# -*- coding: utf-8 -*-
import urllib
from apps.company.models import Company
from django.core.management.base import BaseCommand
from django.template import loader
from django.template.context import Context


class Command(BaseCommand):
    def handle(self, *args, **options):
        for company in Company.objects.all():
            #'total_scope', total_scope, vote_average
            list_fields = [
                'total_quality_service', 'total_conform_terms_delivery',
                'total_convenience_location_offices', 'total_neatness_politeness_courier', 'total_price_quality']
            for field in list_fields:
                setattr(company, field, 2.1)
            company.total_scope = company.vote_average()
            company.save()

