# vim:fileencoding=utf-8
import urllib
from company.models import Poll
from django.core.management.base import BaseCommand
from django.template import loader
from django.template.context import Context


class Command(BaseCommand):
    def handle(self, *args, **options):
        qs = Poll.objects.all()
        for i in qs:
            i.save()
            print i.id

