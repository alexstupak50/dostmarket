# vim:fileencoding=utf-8
from django.core.mail.message import EmailMessage
from django.template import loader, RequestContext
from django.template.context import Context
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.http import Http404
import settings
from PIL import Image, ImageFont, ImageDraw
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
import calendar
import os
import uuid
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string
from django.utils import simplejson
import mailer
from django.contrib.messages.constants import INFO
from django.contrib import messages
from django.shortcuts import get_object_or_404, render_to_response
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, UpdateView
from django.views.generic import CreateView
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.utils import timezone
from django.db import IntegrityError
from django.contrib.auth.models import User
from registration.forms import RegistrationForm
from registration.models import RegistrationProfile
from .forms import CompanyInfo, AddRespondForm, TenderForm, BaseProfile,\
                   CompanyEdit, ChangeAvatar, DeliveryZonePriceForm,\
                   CompetitionForm, DeliveryCompetitionForm,\
                   payment_form_generator, PaymentProfileForm, Registration, FeedBackForm
from .models import Company, Respond, Poll, Tender, SpecialOffer,\
                    Settings, DeliveryZonePrice, DeliveryCompetition,\
                    PaymentType, CompanyPayment, CompanyPaymentField, Vote
from news.models import News
from order.forms import RouteForm, ChangePasswordForm
from order.utils import JsonResponse

from order.models import Order, Profile, OrderBet, OrderCategory, TmpUsers, Route
from articles.models import Article
from cms_page.models import Page


class Redirect(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        return redirect(reverse('article', kwargs={'pk': 29}))


def get_object_or_none(klass, *args, **kwargs):
    try:
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        return None


class LoginRequiredMixin(object):

    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).get(*args, **kwargs)

    @method_decorator(login_required)
    def post(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).post(*args, **kwargs)


class RegisterChoiceView(TemplateView):
    template_name = 'registration/registration_choice.html'


class RegisterView(FormView):
    form_class = Registration

    def get_template_names(self):
        return ['registration/registration_%s.html' % self.kwargs.get('type')]

    def get_context_data(self, **kwargs):
        ctx = super(RegisterView, self).get_context_data(**kwargs)
        ctx['how'] = self.kwargs.get('type')
        if self.request.method == "POST":
            ctx['form2'] = CompanyInfo(self.request.POST, initial={'type': type}, how=ctx['how'])
        else:
            ctx['form2'] = CompanyInfo(initial={'type': ctx['how']})
        return ctx

    def post(self, request, *args, **kwargs):
        form2 = CompanyInfo(request.POST)
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        cform = True
        if self.kwargs.get('type') != 'customer':
            cform = form2.is_valid()
        if form.is_valid() and cform:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def send_user_mail(self, new_user, form, company=False):
        ctx_dict = {'user': new_user, 'password': form.cleaned_data['password1']}
        subject = render_to_string('registration/subject_registration.txt',
                                   ctx_dict)
        subject = ''.join(subject.splitlines())
        if company:
            message = render_to_string('registration/registration_email_company.txt',
                                       ctx_dict)
        else:
            message = render_to_string('registration/registration_email.txt',
                                       ctx_dict)
        new_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

    def form_valid(self, form):
        # new_user = RegistrationProfile.objects.create_inactive_user(
        #     form.cleaned_data['email'], form.cleaned_data['email'],
        #     form.cleaned_data['password1'], 'dostmarket.gremlin.webfactional.com', send_email=False)
        # new_user.first_name = form.cleaned_data['first_name']
        # new_user.save()
        new_user = User.objects.create_user(
            form.cleaned_data['email'], form.cleaned_data['email'], form.cleaned_data['password1'])
        new_user.first_name = form.cleaned_data['first_name']
        new_user.save()
        try:
            if self.kwargs.get('type') == 'company':
                self.send_user_mail(new_user, form, True)
            else:
                self.send_user_mail(new_user, form)
        except:
            #SMTPDataError
            #TODO прописать ошибки
            pass
        if self.request.POST.get('phone'):
            user_profile, create = Profile.objects.get_or_create(
                user=new_user, phone=self.request.POST.get('phone'))
        if self.kwargs.get('type') == 'company':
            self.save_object(new_user, CompanyInfo, Company)
            send_mail(
                u'Новый исполнитель',
                'http://dostmarket.ru/admin/company/company/%s/' % new_user.company.id,
                settings.DEFAULT_FROM_EMAIL, [settings.A_EMAIL]
            )
        return HttpResponseRedirect(reverse('registration_complete'))

    def save_object(self, new_user, form, model):
        info_form = form(self.request.POST)
        # if info_form.is_valid():
        s = info_form.save()
        s.send_mail = True
        s.user = new_user
        s.save()
        #TODO разобраться что за бред?
        p = model.objects.get(user=new_user)
        p.ip = get_client_ip(self.request)
        # create route
        route = Route.objects.create(
            user=new_user,
            country=info_form.cleaned_data['country'],
            region=info_form.cleaned_data['region'] or None,
            only_category=True,
            default=True
        )
        route.categories.add(*info_form.cleaned_data['categories'])
        # COMPANY_TYPE = (
        #     ('transport', 'Транспортная компания'),
        #     ('courier', 'Курьерская служба'),
        # )
        # if self.kwargs.get('type') == 'carrier':
        #     p.type = 'courier'
        # else:
        #     p.type = 'transport'
        # Убрать
        p.active = True
        p.save()


class CompanyView(ListView):
    template_name = 'company/company.html'
    context_object_name = 'list'
    model = Respond
    object = None
    paginate_by = 5

    def get_object(self, **kwargs):
        if 'index.php' in self.request.get_full_path():
            raise Http404
        if not self.object:
            self.object = get_object_or_404(Company, type=self.kwargs['type'], id=self.kwargs['pk'], active=True)
        return self.object

    def get_queryset(self):
        mark = self.request.GET.get('mark', False)
        qs = Respond.objects.filter(company=self.get_object(), moderate=True, mptt_level=0, confirm=True).order_by('-created')
        if mark:
            qs = qs.filter(mark=mark)
        return qs

    def get_context_data(self, **kwargs):
        ctx = super(CompanyView, self).get_context_data(**kwargs)
        ctx['company'] = self.get_object()
        initial = {'mark': 'neitral', 'company': ctx['company']}
        if self.request.user.is_active:
            initial['email'] = self.request.user.email
            initial['name'] = '%s %s' % (self.request.user.first_name, self.request.user.last_name)
            if not self.request.user.first_name and not self.request.user.last_name:
                initial['name'] = self.request.user.username
        ctx['form'] = AddRespondForm(initial=initial)
        if self.request.user.is_authenticated():
            ctx['is_can_respont'] = self.request.user.orders.filter(performer=ctx['company'].user, status=2).exists()
        else:
            ctx['is_can_respont'] = False
        return ctx


MULTI_FILTERS = ['size', 'fabric', 'type', 'country', 'color']


ITEM_ON_PAGE = ['10', '25', '50']

SORT = ['name', 'city', 'site', 'comments', 'total_scope']
BY = ['a', 'd']


class CompanyList(ListView):
    context_object_name = "list"
    template_name = "company/company_list.html"
    qs = None
    rating = False

    def sort_parameter(self):
        sort = self.request.GET.get('sort', False)
        by = self.request.GET.get('by', False)
        if sort and by and sort in SORT and by in BY:
            if by == 'd':
                return '-' + sort
            else:
                return sort
        return None

    def get_queryset(self):
        city = 0
        try:
            if self.request.GET.get('city', 0):
                city = int(self.request.GET.get('city', 0))
        except ValueError:
            pass
#        city = int(self.request.GET.get('city',0))
        types = self.kwargs['type']
        if city:
            qs = Company.objects.filter(city=city, active=True).order_by('-total_scope')
        else:
            qs = Company.objects.filter(active=True).order_by('-total_scope')
        if self.sort_parameter():
            qs = qs.order_by(self.sort_parameter())
        if types == 'all':
            return qs
        else:
            return qs.filter(type=types)

    def get_paginate_by(self, queryset):
        p = self.request.GET.get('items_on_page', None)
        if p and p in ITEM_ON_PAGE:
            return int(p)
        else:
            return 10

    def get_context_data(self, **kwargs):
        ctx = super(CompanyList, self).get_context_data(**kwargs)
        ctx['cur_type'] = self.kwargs['type']
        return ctx


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class AddRespond(CreateView):
    model = Respond
    form_class = AddRespondForm
    template_name = 'company/add_respond.html'

    def get_initial(self):
        initial = {'mark': 'neitral', 'parent': None, 'company': None}
        if self.request.GET.get('parent', False):
            initial['parent'] = int(self.request.GET['parent'])
            initial['company'] = Respond.objects.get(id=int(self.request.GET['parent']), company__active=True, confirm=True).company
        # if self.request.user.is_active:
        #     initial['email'] = self.request.user.email
        #     initial['name'] = '%s %s' % (self.request.user.first_name, self.request.user.last_name)
        #     if not self.request.user.first_name and not self.request.user.last_name:
        #         initial['name'] = self.request.user.username
        return initial

    def get_form_kwargs(self):
        kwargs =  super(AddRespond, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


    def form_valid(self, form):
        respond_uuid = uuid.uuid4().get_hex()
        o = form.save(commit=False)
        if self.request.user.is_active:
            o.email = self.request.user.email
            o.name =  '%s %s' % (self.request.user.first_name, self.request.user.last_name)
            if not self.request.user.first_name and not self.request.user.last_name:
                o.name = self.request.user.username
        o.ip = get_client_ip(self.request)
        o.uuid = respond_uuid
        o.company = form.cleaned_data.get('company')
        parent = form.cleaned_data.get('parent')
        if parent:
            o.parent = self.model.objects.get(id=parent)
        o.save()
        user_email = o.email if form.cleaned_data.get('email') else self.request.user.email
        if not user_email:
            user_email = self.request.user.username if '@' in self.request.user.username else None
        if user_email:
            send_mail(
                u'Подтверждение отзыва',
                u'Здравствуйте. Вы оставили отзыв на сайте dostmarket.ru. '
                u'Для подтверждения Вашего отзыва пожалуйста пройдите по ссылке http://dostmarket.ru/respond/confirm/%s/' % respond_uuid,
                settings.DEFAULT_FROM_EMAIL, [user_email])
        return super(AddRespond, self).form_valid(form)

    def get_success_url(self):
        return '/'
        # company_id = self.request.POST.get('company', False)
        # if company_id:
        #     url = get_object_or_404(Company, id=company_id).get_absolute_url()
        # else:
        #     url = '/'
        # messages.add_message(self.request, INFO, u'Отзыв добавлен, для подтверждения пожалуйста пройдите по ссылке, указанной в письме')
        # return url


class FrontView(TemplateView):
    template_name = "front.html"

    def get_context_data(self, **kwargs):
        ctx = super(FrontView, self).get_context_data(**kwargs)
        city = int(self.request.GET.get('city', 1))
        ctx['orders'] = Order.objects.filter(is_hide=False).order_by('-is_main', '-created')[:9]
        ctx['page'] = get_object_or_none(Page, slug='main', is_active=True)
        qty = Settings.objects.get(value='top_front').value2
        ctx['top_company'] = Company.objects.filter(active=True, logo__isnull=False).order_by('-total_scope', 'name')[0:qty]
        ctx['list_respond'] = Respond.objects.filter(moderate=True, mptt_level=0, company__active=True, confirm=True).order_by('-created')[0:4]
        return ctx


class StatView(DetailView):
    model = Company
    template_name = "company/chart.html"

    def get_context_data(self, **kwargs):
        ctx = super(StatView, self).get_context_data(**kwargs)
        months_name = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май',
                       u'Июнь', u'Июль', u'Август', u'Сентябрь',
                       u'Октябрь', u'Ноябрь', u'Декабрь']
        months_num = self.get_last_months(datetime.today(), 6)
        ctx['months_name'] = [months_name[i[1] - 1] for i in months_num]
        data_votes = []

        for i in self.get_last_months(datetime.today(), 6):
            data = self.q(ctx['object'], i[1], i[0], 1)
            try:
                total_quality_service = (data.aggregate(Sum('scope'))['scope__sum'] or 0) / len(data)
            except ZeroDivisionError:
                total_quality_service = 0

            data = self.q(ctx['object'], i[1], i[0], 2)
            try:
                total_conform_terms_delivery = (data.aggregate(Sum('scope'))['scope__sum'] or 0) / len(data)
            except ZeroDivisionError:
                total_conform_terms_delivery = 0

            data = self.q(ctx['object'], i[1], i[0], 3)
            try:
                total_convenience_location_offices = (data.aggregate(Sum('scope'))['scope__sum'] or 0) / len(data)
            except ZeroDivisionError:
                total_convenience_location_offices = 0

            data = self.q(ctx['object'], i[1], i[0], 4)
            try:
                total_neatness_politeness_courier = (data.aggregate(Sum('scope'))['scope__sum'] or 0) / len(data)
            except ZeroDivisionError:
                total_neatness_politeness_courier = 0

            data = self.q(ctx['object'], i[1], i[0], 5)
            try:
                total_price_quality = (data.aggregate(Sum('scope'))['scope__sum'] or 0) / len(data)
            except ZeroDivisionError:
                total_price_quality = 0

            data_votes.append((total_quality_service + total_conform_terms_delivery + total_convenience_location_offices + total_neatness_politeness_courier + total_price_quality) / float(5))

            ctx['data_votes'] = data_votes
        return ctx

    def get_last_months(self, start_date, months):
        for i in range(months):
            yield (start_date.year, start_date.month)
            start_date += relativedelta(months=-1)

    def q(self, obj, m, y, t):
        return Vote.objects.filter(company=obj, created__month=m,
                                   created__year=y, vote_type=t)


class TenderAdd(Redirect, CreateView):
    form_class = TenderForm
    model = Tender
    success_url = '/tender/send/'
    template_name = 'company/tender.html'

    def form_valid(self, form):
        self.object = form.save()
        # msg = render_to_string('company/new_tender.html', {'tender': self.object})
        set = Settings.objects.get(value='send_fast_order')
        if self.request.GET.get('company', False):
            email = Company.objects.get(id=int(self.request.GET.get('company')), active=True).email
#            send_mail(u'Новая заявка с сайта Dostmarket.ru', msg, 'robot@dostmarket.ru', [email])
            self.send_letter(self.object, [email])
        else:
            if form.cleaned_data['type']:
                email = Company.objects.filter(type=form.cleaned_data['type'], active=True).order_by('-total_scope')[0:set.value2].values_list('email', flat=True)
            else:
                email = Company.objects.filter(active=True).order_by('-total_scope')[0:set.value2].values_list('email', flat=True)
            if set.active:
                for mail in email:
                    self.send_letter(self.object, [mail])
#                    send_mail(u'Новая заявка с сайта Dostmarket.ru', msg, 'robot@dostmarket.ru', [mail])
        send_mail(u'Новая заявка с сайта Dostmarket.ru', 'Ваша заявка отправлена', settings.DEFAULT_FROM_EMAIL, [self.object.email])
        return HttpResponseRedirect(self.get_success_url())

    def send_letter(self, tender, mail):
        t = loader.get_template('letter_dost/letter.html')
        context = {'tender': tender}
        context = Context(context)
        html = (t.render(context)).encode('utf-8')
        msg = EmailMessage(u'Новая заявка с сайта Dostmarket.ru', html, 'robot@dostmarket.ru', mail, headers={})
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()

    def get(self, *args, **kwargs):
        if u'москва' in self.request.GET.get('point_a', '').lower() or u'москва' in self.request.GET.get('point_b', '').lower():
            query_string = self.request.META["QUERY_STRING"]
            return HttpResponseRedirect('%s?%s' % (reverse('competition'), query_string))
        return super(TenderAdd, self).get(*args, **kwargs)


class RespondList(ListView):
    template_name = 'company/respond_list.html'
    context_object_name = 'list'
    model = Respond
    paginate_by = 10


    def get_context_data(self, **kwargs):
        ctx = super(RespondList, self).get_context_data(**kwargs)
        ctx['company_list'] = Company.objects.all().order_by('name')
        company_id = self.request.GET.get('id', None)
        if company_id != 'undefined' and company_id:
            ctx['mark_name'] = get_object_or_none(Company, id=company_id).name
        ctx['form'] = AddRespondForm(initial={'mark': 'neitral'})
        return ctx

    def get_queryset(self):
        mark = self.request.GET.get('mark', False)
        company_id = self.request.GET.get('id', None)
        qs = Respond.objects.filter(
            moderate=True, mptt_level=0, company__active=True, confirm=True).order_by('-created')
        if mark:
            qs = qs.filter(mark=mark)
        if company_id != 'undefined' and company_id:
            qs = qs.filter(company=company_id)
        return qs


class OfferView(DetailView):
    model = SpecialOffer
    template_name = 'company/offer.html'


class EditProfile(LoginRequiredMixin, UpdateView):
    form_class = BaseProfile

    def get_template_names(self):
        if self.request.user.is_carrier():
            return ['registration/profile_company.html']
        return super(EditProfile, self).get_template_names()

    def get_initial(self):
        init = super(EditProfile, self).get_initial()
        try:
            init.update({'phone': self.request.user.profile.phone})
        except Profile.DoesNotExist:
            pass
        return init

    def get_object(self, queryset=None):
        return self.request.user


    def get_context_data(self, **kwargs):
        ctx = super(EditProfile, self).get_context_data(**kwargs)
        user = self.request.user
        if not user.is_carrier():
            ctx['object_list'] = Order.objects.filter(user=user).order_by('-created')
            ctx['orders_run'] = ctx['object_list'].filter(status=1)
        if user.is_carrier():
            ctx['object_list'] = OrderBet.objects.filter(user=user).order_by('-created')
            ctx['bets_win'] = ctx['object_list'].filter(order__status=5, status=1)
        ctx['routes'] = self.get_object().routes.all()
        ctx['route_form'] = RouteForm()
        ctx['password_form'] = ChangePasswordForm(self.request.user)
        if self.request.user.is_carrier():
            ctx['form2'] = CompanyEdit(self.request.POST or None, instance=self.object.company)
            ctx['payment_form_generator'] = payment_form_generator(self.request.user.company)
        return ctx

    def form_valid(self, form):
        form.save()
        profile, create = Profile.objects.get_or_create(user=self.request.user)
        if form.cleaned_data.get('phone'):
            profile.phone = form.cleaned_data['phone']
            profile.save()
        if self.request.user.is_company() or self.request.user.is_courier():
            p = Company.objects.get(user=self.request.user)
            p.ip = get_client_ip(self.request)
            p.save()
            # if self.request.POST.get('type', False):
            post_copy = self.request.POST.copy()
            post_copy['is_courier'] = True if p.is_courier() else False
            info_form = CompanyEdit(post_copy, instance=self.object.company)
            if info_form.is_valid():
                info_form.save()
                try:
                    first_route = p.user.routes.filter(default=True)[0]
                    if first_route:
                        first_route.country = info_form.cleaned_data['country2']
                        first_route.region = info_form.cleaned_data['region2']
                        first_route.save()
                        first_route.categories.clear()
                        first_route.categories.add(*info_form.cleaned_data['categories'])

                except IndexError:
                    pass
            else:
                return super(EditProfile, self).form_invalid(form)
        messages.add_message(self.request, INFO, u'Информация обновлена')
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/'))


class Avatar(LoginRequiredMixin, UpdateView):
    model = Company
    form_class = ChangeAvatar
    success_url = '/accounts/profile/'
    template_name = 'company/change_avatar.html'

    def get_initial(self):
        init = {}
        init['user'] = self.request.user
        return init


class OrdersSend(LoginRequiredMixin, DetailView):
    model = Tender
    template_name = 'company/tender.html'
    subscribers = True

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        set = Settings.objects.get(value='send_fast_order')
        if self.object.type:
            email = Company.objects.filter(type=self.object.type, active=True).order_by('-total_scope')[0:set.value2].values_list('email', flat=True)
        else:
            email = Company.objects.filter(active=True).order_by('-total_scope')[0:set.value2].values_list('email', flat=True)
        for mail in email:
            self.send_letter(self.object, [mail])
#            send_mail(u'Новая заявка с сайта Dostmarket.ru', msg, 'robot@dostmarket.ru', [mail])
        messages.add_message(self.request, INFO, u'Заявка разослана компаниям')
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/'))

    def send_letter(self, tender, mail):
        t = loader.get_template('letter_dost/letter.html')
        context = {'tender': tender}
        context = Context(context)
        html = (t.render(context)).encode('utf-8')
        msg = EmailMessage(u'Новая заявка с сайта Dostmarket.ru', html, 'robot@dostmarket.ru', mail, headers={})
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()


def counter(request, **kwargs):
    image = Image.open(os.path.join(settings.MEDIA_ROOT, "counter", 'counter.png'))
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype(os.path.join(settings.MEDIA_ROOT, "counter", 'ho.ttf'), 12)
    companies = Company.objects.filter(type=kwargs['type'], active=True).order_by('-total_scope')
    try:
        company = companies.get(id=int(kwargs['id']))
        place = str(list(companies).index(company) + 1)
        draw.text((26, 17), place, font=font)
        image.save(os.path.join(settings.MEDIA_ROOT, "counter", 'counter_%s.png' % kwargs['id']), 'png')
        return HttpResponse(open(os.path.join(settings.MEDIA_ROOT, "counter", 'counter_%s.png' % kwargs['id']), 'rb').read(), mimetype="image/png")
    except ObjectDoesNotExist:
        return HttpResponse()

class CompanyScript(DetailView):
    model = Company
    template_name = 'company/script.js'

    def get_context_data(self, **kwargs):
        ctx = super(CompanyScript, self).get_context_data(**kwargs)
        companies = Company.objects.filter(type=self.object.type, active=True).order_by('-total_scope')
        place = list(companies).index(self.object) + 1
        page = place // 10
        ctx['place'] = place
        ctx['page'] = page + 1
        return ctx


class CompetitionView(Redirect, TemplateView):
    '''
    оформление доставки по МСК и МО
    '''
    template_name = 'company/competition.html'

    def get_context_data(self, **kwargs):
        ctx = super(CompetitionView, self).get_context_data(**kwargs)
        ctx['form'] = CompetitionForm()
        if hasattr(ctx['form'], 'fields'):
            ctx['form'].fields['from_address'].initial = self.request.GET.get('point_a')
            ctx['form'].fields['to_address'].initial = self.request.GET.get('point_b')

        if self.request.GET.get('company'):
            company = self.request.GET.get('company')
            payment = PaymentType.objects.filter(companypayment__company=company)
            epayment = payment.exclude(emoney=False)
        else:
            payment = PaymentType.objects.all()
            epayment = payment.exclude(emoney=False)

        ctx['payment'] = payment
        ctx['epayment'] = epayment
        return ctx


class CompetitionListView(Redirect, LoginRequiredMixin, ListView):
    '''
    В профиле ссылка Заказы
    Список всех заказов юзера/компании
    '''
    template_name = 'company/competition_list.html'

    def get_queryset(self):
        user = self.request.user
        if user.is_carrier():
            objects_list = OrderBet.objects.filter(user=user).order_by('-created')
        else:
            objects_list = Order.objects.filter(user=user).order_by('-created')
        return objects_list

    def get_context_data(self, **kwargs):
        ctx = super(CompetitionListView, self).get_context_data(**kwargs)
        # if not self.request.user.is_carrier():
        #     ctx['orders_run'] = self.get_queryset().filter(status=1)
        # if self.request.user.is_carrier():
        #     ctx['bets_win'] = self.get_queryset().filter(order__status=5, status=1)
        return ctx

class CompetitionDetailView(Redirect, DetailView):
    '''
    Подробный просмотр заказа
    В ссылке имеется uuid (любой юзер, имеющий ссылку может просмотреть)
    '''
    template_name = 'company/competition_detail.html'

    def get_object(self):
        uuid = self.kwargs.get('uuid', None)
        return get_object_or_404(DeliveryCompetition, uuid=uuid)


class DeliveryZonePriceView(Redirect, LoginRequiredMixin, UpdateView):
    '''
    Прейскурант компаний по зонам
    '''
    template_name = 'company/zone.html'
    form_class = DeliveryZonePriceForm
    success_url = '/accounts/profile/zone/'

    def get_object(self):
        if not hasattr(self.request.user, 'company'):
            raise Http404
        obj, created = DeliveryZonePrice.objects.get_or_create(company=self.request.user.company)
        return obj

    def form_valid(self, form):
        p = form.save(commit=False)
        p.company = self.request.user.company
        p.save()
        messages.add_message(self.request, INFO, u'Данные сохранены')
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryZonePriceView, self).get_context_data(**kwargs)
        ctx['zone_weight'] = DeliveryZonePrice.zone_fields
        return ctx


@login_required
def competition_complite(request, uuid):
    '''
    Меняем статус заказа на выполнено
    '''
    p = get_object_or_404(DeliveryCompetition, uuid=uuid,
                          company__user=request.user)
    p.status = 1
    p.save()
    title = u'Ваша доставка успешно выполнена.'
    msg = u'Здравствуйте, Ваша доставка, оформленная на сайте DostMarket.ru успешно  выполнена.\nСпасибо, что воспользовались нашим сервисом.'
    send_mail(title, msg, settings.DEFAULT_FROM_EMAIL, [p.from_email])
    return HttpResponseRedirect(reverse('competition_list'))


@login_required
def companies_payment_add(request):
    data = {}

    if request.is_ajax and request.POST and request.user.company:
        action = request.GET.get('action')
        payment = get_object_or_404(PaymentType, pk=action)
        form = PaymentProfileForm(payment=payment, company=request.user.company, data=request.POST)

        if form.is_valid():
            form.save()
            data = simplejson.dumps({'success': True})
        else:
            data = simplejson.dumps(dict([(k, [unicode(e) for e in v]) for k, v in form.errors.items()]))

    return HttpResponse(data, mimetype='application/json')


def ajax_competition_validate(request):
    '''
    Валидируем форму на заказа
    '''
    if request.is_ajax and request.POST:
        form = CompetitionForm(request.POST)
        if form.is_valid():
            data = {'success': True}
            data = simplejson.dumps(data)
            return HttpResponse(data, mimetype='application/json')
        else:
            data = simplejson.dumps(dict([(k, [unicode(e) for e in v]) for k, v in form.errors.items()]))
            return HttpResponse(data, mimetype='application/json')
    raise Http404


def ajax_get_company(request):
    '''
    Возвращаем компании, которые подходят по параметрам
    '''
    if request.is_ajax:
        weight = request.POST.get('weight', None)
        dist = request.POST.get('dist')
        company = request.POST.get('company', None)
        payment = request.POST.get('payment_type', None)
        zone = {'moscow': 1, 'mkad': 2, 'mkad_5km': 3, 'mkad_10km': 4,
                'mkad_25km': 5, 'mkad_50km': 6, 'mkad_100km': 7}
        kw = {'companypayment__payment_type__pk': payment}

        if 'no_mkad' in dist:
            raise Http404

        if int(weight) > 10:
            weight = 'ot10'
        dist = 'deliveryzoneprice__zone%s_%skg' % (zone[dist], weight)

        data = Company.objects.filter(city=1,
                                      active=True,
                                      deliveryzoneprice__company__isnull=False)\
                              .filter(**kw)\
                              .values('pk', 'name', 'logo', 'type',
                                      dist, 'deliveryzoneprice__urgency')\
                              .order_by('-%s' % dist)

        try:
            if PaymentType.objects.get(pk=payment).show_fields:
                data = data.filter(companypayment__show_fields=True).distinct()
        except PaymentType.DoesNotExist:
            pass

        if not company is None:
            data = data.filter(pk=company)

        data = simplejson.dumps(list(data))
        return HttpResponse(data, mimetype='application/json')


def ajax_get_payment(request, pk, payment_type):
    if request.is_ajax:
        company = get_object_or_404(Company, pk=pk)
        data = CompanyPaymentField.objects.filter(payment__company=company,
                                                  payment__company__active=True,
                                                  payment__payment_type__pk=payment_type)\
                                          .values('value',
                                                  'payment__payment_type__name',
                                                  'payment__show_fields')
        if not data:
            data = CompanyPayment.objects.filter(company=company,
                                                 company__active=True,
                                                 payment_type__pk=payment_type)\
                                         .values_list('payment_type__name',
                                                      'show_fields')
            datap = {}
            datap['payment__payment_type__name'] = data[0][0]
            datap['payment__show_fields'] = data[0][1]
            data = simplejson.dumps([datap])
        else:
            data = simplejson.dumps(list(data))
        return HttpResponse(data, mimetype='application/json')


def ajax_delivery_competition(request):
    if request.is_ajax and request.POST:
        form = DeliveryCompetitionForm(request.POST)

        if form.is_valid():
            p = form.save(commit=False)
            if request.user.is_authenticated():
                p.user = request.user
            p.save()

            if p.company.send_mail:
                msg = u'На сайте DostMarket.ru была оформлена заявка на доставку в вашей компании.\n'
                msg += u'Согласно нашим договоренностям, пользователь ждет звонка от вашего менеджера в течении 15 минут для уточнения условия доставки.\n\n'
                msg += u'Подробную информацию о заказе вы можете увидеть, перейдя по ссылке http://dostmarket.ru/competition/%s/.\n\n' % p.uuid
                msg += u'С уважением, DostMarket'
                send_mail(u'Новый заказ на сайте DostMarket.ru', msg,
                          settings.DEFAULT_FROM_EMAIL, [p.company.email])

            msg = u'Вы оформили заявку на доставку на сайте DostMarket.ru.\n'
            msg += u'В течении 15 минут с вами должен связаться менеджер выбранной вами курьерской службы. Если в течении этого времени вам никто не позвонит, вы вправе выбрать другую компанию. А мы в свою очередь разъясним ситуацию и примем меры.\n\n'
            msg += u'Спасибо, что воспользовались нашим сервисом.\n\n'
            msg += u'С уважением, DostMarket'
            send_mail(u'Оформление заявки на сайте DostMarket.ru', msg,
                      settings.DEFAULT_FROM_EMAIL, [p.from_email])

        data = simplejson.dumps({'url': 'http://dostmarket.ru/competition/%s/' % p.uuid})
        return HttpResponse(data, mimetype='application/json')


def poll_confirm(request, poll_uuid):
    poll = get_object_or_404(Poll, uuid=poll_uuid, confirm=False)
    poll.confirm = True
    poll.save(recalc=True)
    return render_to_response('company/poll_confirm.html',
                              context_instance=RequestContext(request))


def respond_confirm(request, respond_uuid):
    respond = get_object_or_404(Respond, uuid=respond_uuid, confirm=False)
    respond.confirm = True
    respond.save()

    if respond.mptt_level == 0:
        msg = render_to_string('company/new_respond.html', {'respond': respond})
        send_mail(u'Новый отзыв на сайте Dostmarket.ru', msg,
                  settings.DEFAULT_FROM_EMAIL, [respond.company.email])
    else:
        msg = render_to_string('company/new_respond2.html', {'respond': respond})
        send_mail(u'Получен ответ на Ваш отзыв', msg,
                  settings.DEFAULT_FROM_EMAIL, [respond.parent.email])

    msg = render_to_string('company/new_respond.html', {'respond': respond})
    send_mail(u'Получен отзыв', msg,
              settings.DEFAULT_FROM_EMAIL, [settings.A_EMAIL])
    return render_to_response('company/respond_confirm.html',
                              context_instance=RequestContext(request))


class Contact(FormView):
    form_class = FeedBackForm
    template_name = 'company/contacts.html'
    success_url = '/'

    def form_valid(self, form):
        cd = form.cleaned_data
        text = u'Емейл отправителя - %s \n %s' % (cd['email'], cd['text'])
        mailer.send_mail(cd['subject'], text, settings.DEFAULT_FROM_EMAIL, [settings.DEFAULT_FROM_EMAIL,])
        return super(Contact, self).form_valid(form)


class ResetPassword(FormView):
    # template_name = 'registration/password_reset_form3.html'
    template_name2 = 'inclusion/form_password_reset.html'
    success_url = '/accounts/profile/'
    form_class = ChangePasswordForm

    def get_template_names(self):

        if 'account' in self.request.META.get('HTTP_REFERER'):
            return self.template_name2
        return super(ResetPassword, self).get_template_names()


    def get_context_data(self, **kwargs):
        ctx = super(ResetPassword, self).get_context_data(**kwargs)
        ctx['password_form'] = ctx.get('form')
        return ctx

    def get_form_kwargs(self):
        kwargs = super(ResetPassword, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        user.set_password(form.cleaned_data['password2'])
        try:
            user.tmpusers.delete()
        except TmpUsers.DoesNotExist:
            pass
            user.save()
        data = dict(valid='True')
        return JsonResponse(data)


class SiteMap(TemplateView):
    template_name = 'inclusion/site_map.html'

    def get_context_data(self, **kwargs):
        from cms.utils.moderator import get_page_queryset
        cxt = super(SiteMap, self).get_context_data(**kwargs)
        page_queryset = get_page_queryset(None)
        cxt['urlset'] = page_queryset.published().filter(login_required=False)
        cxt['company'] = Company.objects.filter(active=True).order_by('-total_scope')
        cxt['news'] = News.objects.all()
        cxt['articles'] = Article.objects.all()
        cxt['pages'] = Page.objects.filter(is_active=True).exclude(slug='main')
        return cxt
