# vim:fileencoding=utf-8
from django import template
from django.utils.html import escape
from django.utils.text import normalize_newlines
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe, SafeData
from django import template

from ..models import DeliveryZonePrice

register = template.Library()

def switch_class(value):
    mark = {
        'positive': 'ellipse-good',
        'negative': 'ellipse-bad',
        'neitral': 'ellipse-neutral'
    }
    return mark[value]


def place(value,arg):
    if arg != 1:
        return value+(arg*10)-10
    return value


def percent(value,arg):
    if float(arg) == 0:
        return 0
    return int((float(arg)/float(value))*100)


def get_zone(value):
    zone = [x for x in DeliveryZonePrice.zone.itervalues()]
    return zone[(int(value.replace('zone', '').replace('_1kg', '')) - 1)]


def get_at_index(list, index):
    return list[index]


class AssignNode(template.Node):
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def render(self, context):
        context[self.name] = self.value.resolve(context, True)
        return ''


def do_assign(parser, token):
    """
    Assign an expression to a variable in the current context.

    Syntax::
        {% assign [name] [value] %}
    Example::
        {% assign list entry.get_related %}

    """
    bits = token.contents.split()
    if len(bits) != 3:
        raise template.TemplateSyntaxError("'%s' tag takes two arguments" % bits[0])
    value = parser.compile_filter(bits[2])
    return AssignNode(bits[1], value)




@register.simple_tag(takes_context=True)
def type_user(context, *args):
    """
    Возращает в зависимости от типа юзера соответсвующие строку
    в args передаем строки в таком порядке:
        company
        courier
        user
    """
    user = context['user']
    for str in args:
        if user.check_type_user() == 'company':
            return args[0]
        elif user.check_type_user() == 'courier':
            return args[1]
        else:
            return args[2]
    return


@register.assignment_tag(takes_context=True)
def get_count_new_bets(context, order, *args, **kwargs):
    user = context['user']
    if order and user == order.user and not user.is_carrier():
        new_bets = order.get_new_bets()
        count = new_bets.count()
        new_bets.update(is_read=True)
        return ' [+%s]' % count if count else ''
    return ''


def get_auto_selected(val):
    from order.models import Order, OrderBet
    if val and isinstance(val[0], OrderBet):
        return [order for order in val if order.order.auto_selected
                and order.order.status != 1 and order.status != 2]
    return [order for order in val if order.auto_selected and order.get_win_bet().status != 2]


@register.filter(is_safe=True, needs_autoescape=True)
@stringfilter
def linebreaksbr_custom(value, autoescape=None):
    """
    Converts all newlines in a piece of plain text to HTML line breaks
    (``<br />``).
    """
    autoescape = autoescape and not isinstance(value, SafeData)
    value = normalize_newlines(value)
    if autoescape:
        value = escape(value)
    if '<p' in value:
        return mark_safe(value)
    return mark_safe(value.replace('\n', '<br/>'))

@register.filter
def error(val):
    return 'error' if val.errors else ''


def paginator2(context, adjacent_pages=4):
    page_obj = context['page_obj']
    page = page_obj.number
    paginator = context['paginator']
    last_page = paginator.page_range[-1]
    page_numbers = [n for n in \
                    range(page - adjacent_pages, page + adjacent_pages + 1) \
                    if n > 0 and n <= last_page]
    ctx = {
        'paginator': paginator,
        'page_obj': page_obj,
        'range': page_numbers,
        'show_first': 1 not in page_numbers,
        'show_last': last_page not in page_numbers,
        'last_page': last_page,
        'request': context['request']

    }
    return ctx

def paginator(context, adjacent_pages=4):
    page_obj = context['page_obj']
    page = page_obj.number
    paginator = context['paginator']
    last_page = paginator.page_range[-1]
    page_numbers = [n for n in \
                    range(page - adjacent_pages, page + adjacent_pages + 1) \
                    if n > 0 and n <= last_page]
    ctx = {
        'paginator': paginator,
        'page_obj': page_obj,
        'range': page_numbers,
        'show_first': 1 not in page_numbers,
        'show_last': last_page not in page_numbers,
        'last_page': last_page,
        'request': context['request']
    }
    return ctx

register.inclusion_tag('inclusion/paginator.html', takes_context=True)(paginator2)
register.inclusion_tag('inclusion/pagination.html', takes_context=True)(paginator)


@register.filter()
def replace_paws(value):
    return value.replace('"', "'")

register.filter('percent', percent)
register.filter('get_auto_selected', get_auto_selected)
register.filter('place', place)
register.filter('switch_class', switch_class)
register.filter('get_zone', get_zone)
register.filter('get_at_index', get_at_index)
register.tag('assign', do_assign)


