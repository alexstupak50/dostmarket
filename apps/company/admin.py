# vim:fileencoding=utf-8
from django.contrib import admin
from sorl.thumbnail import ImageField
from sorl.thumbnail.admin.current import AdminImageWidget
from ckeditor.widgets import CKEditorWidget
from django.utils.html import mark_safe, strip_tags
from django import forms
from django.contrib.auth.models import User
from company.models import Company, City, Respond, Poll, Tender, SpecialOffer, \
    Banner, Settings, DeliveryCompetition, PaymentType, \
    PaymentTypeField, DeliveryZonePrice, Vote


class UserForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.order_by('-id'))
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = User

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        if instance:
            kwargs.update(initial={
                'description': mark_safe(strip_tags(instance.description))
            })
        super(UserForm, self).__init__(*args, **kwargs)


class RespondForm(forms.ModelForm):
    class Meta:
        model = Respond

    def __init__(self, *args, **kwargs):
        from django.utils.html import mark_safe, strip_tags
        instance = kwargs.get('instance', None)
        if instance:
            kwargs.update(initial={
                'text': mark_safe(strip_tags(instance.text))
            })
        super(RespondForm, self).__init__(*args, **kwargs)


class TypeListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = u'Тип перевозчика'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'type'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('transport', u'Транспортые компании'),
            ('courier', u'Курьерские компании'),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() == 'transport':
            return queryset.filter(type='transport')
        if self.value() == 'courier':
            return queryset.filter(type='courier')


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'email']
    list_filter = ['type', TypeListFilter, 'active']
    list_display_links = ['name']
    # exclude = ('city', )
    filter_horizontal = ['alternative', 'categories']
    ordering = ('-id',)
    formfield_overrides = {
        ImageField: {'widget': AdminImageWidget},
        }
    search_fields = ('name', 'email', 'user__email')
    form = UserForm


class RespondAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'company', 'mark', 'created', 'moderate', 'confirm']
    list_filter = ['company', 'mark', 'moderate', 'confirm']
    list_display_links = ['name']
    list_editable = ['moderate']
    form = RespondForm

    def queryset(self, request):
        qs = super(RespondAdmin, self).queryset(request)
        return qs.filter(confirm=True)


# class PollAdmin(admin.ModelAdmin):
#     list_display = ['company', 'name', 'email', 'created', 'choice', 'confirm']
#     list_filter = ['company', 'created', 'choice', 'confirm']
#     list_display_links = ['company']
#     date_hierarchy = 'created'


class TenderAdmin(admin.ModelAdmin):
    list_display = ['name', 'cargo', 'point_a', 'point_b', 'created', 'send_link']


class SettingsAdmin(admin.ModelAdmin):
    list_display = ['title', 'value', 'value2', 'active']
    list_editable = ['active', 'value2']


class SpecialOfferAdmin(admin.ModelAdmin):
    list_display = ['company', 'offer', 'start_date', 'end_date']
    list_filter = ['company']


class DeliveryCompetitionAdmin(admin.ModelAdmin):
    list_display = ('cargo', 'company', 'status', 'created')
    list_filter = ('status', 'company')


class PaymentTypeFieldInlineAdmin(admin.TabularInline):
    model = PaymentTypeField
    extra = 1


class PaymentTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'order')
    list_editable = ('order',)
    inlines = [PaymentTypeFieldInlineAdmin]


class DeliveryZonePriceAdmin(admin.ModelAdmin):
    list_display = ('company',)
    search_fields = ('company',)
    fieldsets = [
        (None, {'fields': ['company', 'urgency']})
    ]
    for key, value in DeliveryZonePrice.zone.items():
        append = (value, {'fields': ['zone%s_%s' % (key, x) for x in DeliveryZonePrice.zone_fields]})
        fieldsets.append(append)


admin.site.register(Company, CompanyAdmin)
# admin.site.register(SpecialOffer, SpecialOfferAdmin)
# admin.site.register(City)
admin.site.register(Banner)
admin.site.register(Vote)
admin.site.register(Settings, SettingsAdmin)
# admin.site.register(Poll, PollAdmin)
# admin.site.register(Tender, TenderAdmin)
admin.site.register(Respond, RespondAdmin)
# admin.site.register(DeliveryCompetition, DeliveryCompetitionAdmin)
admin.site.register(PaymentType, PaymentTypeAdmin)
# admin.site.register(DeliveryZonePrice, DeliveryZonePriceAdmin)
