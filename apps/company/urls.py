# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from company.ajax import poll_post
from company.ajax import vote
from django.views.generic.base import RedirectView
from .views import AddRespond, StatView, OfferView, counter, CompanyScript, CompanyView, CompanyList, CompetitionView, \
    CompetitionListView, CompetitionDetailView, FrontView, OrdersSend, EditProfile, DeliveryZonePriceView, Avatar, \
    RegisterView, RegisterChoiceView, Contact, RespondList, ResetPassword, SiteMap


urlpatterns = patterns('',
    url(r'^$', FrontView.as_view(), name='front'),
    url(r'^order/form/$', FrontView.as_view(template_name='front_category_form.html'), name='front'),
    url(r'^companies/respond/$', AddRespond.as_view(), name='add_respond'),
    url(r'^companies/payment/add/$', 'company.views.companies_payment_add', name='companies_payment_add'),
    url(r'^companies/(?P<type>[0-9A-Za-z-_.//]+)/(?P<id>[0-9A-Za-z-_.//]+)/offer/(?P<pk>[0-9A-Za-z-_.//]+)/$', OfferView.as_view(), name='special_offer'),
    url(r'^companies/(?P<type>[0-9A-Za-z-_.//]+)/(?P<pk>[0-9A-Za-z-_.//]+)/stat/$', StatView.as_view(), name='stat'),
    url(r'^companies/(?P<type>[0-9A-Za-z-_.//]+)/(?P<id>[0-9A-Za-z-_.//]+)/counter/$', counter, name='counter'),
    url(r'^companies/(?P<type>[0-9A-Za-z-_.//]+)/(?P<pk>[0-9A-Za-z-_.//]+)/script/$', CompanyScript.as_view(), name='script'),
    url(r'^companies/(?P<type>[0-9A-Za-z-_.//]+)/(?P<pk>[0-9A-Za-z-_.//]+)/$', CompanyView.as_view(), name='company'),
    url(r'^rating/(?P<type>[0-9A-Za-z-_.//]+)/$', CompanyList.as_view(rating=True, template_name='company/rating_list.html'), name='rating_list'),
    url(r'^companies/(?P<type>[0-9A-Za-z-_.//]+)/$', CompanyList.as_view(), name='company_list'),
    url(r'^competition/$', CompetitionView.as_view(), name='competition'),
    url(r'^competition/list/$', CompetitionListView.as_view(), name='competition_list'),
    url(r'^competition/(?P<uuid>[0-9A-Za-z-_.//]+)/$', CompetitionDetailView.as_view(), name='competition_detail'),
    url(r'^competition_complite/(?P<uuid>[0-9A-Za-z-_.//]+)/$', 'company.views.competition_complite', name='competition_complite'),
    url(r'^ajax_competition_validate/$', 'company.views.ajax_competition_validate', name='ajax_competition_validate'),
    url(r'^ajax_get_company/$', 'company.views.ajax_get_company', name='ajax_get_company'),
    url(r'^ajax_get_payment/(?P<pk>[0-9A-Za-z-_.//]+)/(?P<payment_type>[0-9A-Za-z-_.//]+)/$', 'company.views.ajax_get_payment', name='ajax_get_payment'),
    url(r'^ajax_delivery_competition/$', 'company.views.ajax_delivery_competition', name='ajax_delivery_competition'),
    url(r'^poll/confirm/(?P<poll_uuid>[0-9A-Za-z-_.//]+)/$', 'company.views.poll_confirm', name='poll_confirm'),
    url(r'^respond/confirm/(?P<respond_uuid>[0-9A-Za-z-_.//]+)/$', 'company.views.respond_confirm', name='respond_confirm'),
    url(r'^tender/send/(?P<pk>[0-9A-Za-z-_.//]+)/$', OrdersSend.as_view(), name='tender_send'),

    url(r'^accounts/register/(?P<type>["customer"|"company"]+)/$', RegisterView.as_view(), name='registration_register'),
    url(r'^accounts/register/$',  RedirectView.as_view(url='/accounts/register/choice/')),
    url(r'^accounts/register/choice/$', RegisterChoiceView.as_view(), name='registration_choice'),
    url(r'^password/reset/$', ResetPassword.as_view(), name='reset_password'),
    url(r'^accounts/profile/$', EditProfile.as_view(template_name='registration/profile.html'), name='account'),
    url(r'^accounts/profile/zone/$', DeliveryZonePriceView.as_view(), name='account_zone'),

    url(r'^respondes/$', RespondList.as_view(), name='respond_list'),
    url(r'^api/poll/$', poll_post, name='poll'),
    url(r'^rating/$', vote, name='rating'),
    url(r'^contacts/$', Contact.as_view(), name='contacts'),
    url(r'^avatar/upload/(?P<pk>[0-9A-Za-z-_.//]+)/', Avatar.as_view(), name='change_avatar'),
    url(r'^sitemap/$', SiteMap.as_view(), name='sitemap'),
)


