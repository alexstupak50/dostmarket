# -*- coding: utf-8 -*-
from datetime import date
from django.conf import settings
from company.models import City, Respond, Company, SpecialOffer, Banner, Settings
from order.models import Order, OrderBet, OrderCategory

__author__ = 'Ivan'

def allCity(request):
    city = City.objects.all()
    return {'all_city': city}


def new_orders(request):
    """
    Новые оповищения в личном кабинете
    """
    user = request.user
    ctx = dict()
    # ctx['orders'] = Order.objects.filter(status=0, is_hide=False).order_by('-is_main', '-created')[:6]
    if user.is_authenticated():
        if not request.user.is_carrier():
            count = Order.objects.get_count_new_bet(request.user)
            count = '%s' % count if count > 0 else ''
            ctx['count_new_bet'] = count
        if request.user.is_carrier():
            count = OrderBet.objects.filter(user=user).order_by('-created').filter(
                order__auto_selected=False, order__status=5, status=1).count()
            ctx['count_new_bet'] = '%s' % count if count > 0 else ''
    return ctx


def respond_block(request):
    return {'respond_block': Respond.objects.filter(moderate=True, company__active=True, confirm=True).order_by('-created')[0:4]}

def top_company(request):
    qty = Settings.objects.get(value='top_right').value2
    return {'top_company': Company.objects.filter(active=True, logo__isnull=False).order_by('-total_scope', 'name')[0:qty], 'qty_top':qty}

def special_offer(request):
    today = date.today()
    return {'special_offer': SpecialOffer.objects.filter(start_date__lte=today, end_date__gte=today)[0:4]}


def banners(request):
    banners = {
        'banner_top': Banner.objects.filter(place='top', publish=True)[0:1],
        'banner_right': Banner.objects.filter(place='right', publish=True)[0:1],
        'categories': OrderCategory.objects.all(),
    }
    return banners

def debug(context):
  return {'DEBUG': settings.DEBUG}