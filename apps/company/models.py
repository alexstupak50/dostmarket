# vim:fileencoding=utf-8
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from smart_selects.db_fields import ChainedForeignKey
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
import os
from datetime import date, datetime
import uuid
import settings
from sorl.thumbnail.fields import ImageField
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.dispatch import receiver
from uuidfield import UUIDField
from collections import OrderedDict
from order.models import Profile

TYPES_CARRIER = (
    (1, u'Физическое лицо'),
    (2, u'Юридическое лицо')
)

COUNTY_ALLOWED_CODE = ['UA', 'RU', 'BY', 'KZ']

COMPANY_TYPE = (
    ('transport', 'Транспортная компания'),
    ('courier', 'Курьерская служба'),
)

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(getattr(settings, 'PRODUCT_IMAGE_UPLOAD_TO', 'company/'), filename)


class Company(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Название компании')
    logo = ImageField(verbose_name=u'Логотип', upload_to=get_file_path, null=True, blank=True)
    type_carrier = models.SmallIntegerField(
        u'Тип перевозчика', choices=TYPES_CARRIER, null=True, blank=True)
    categories = models.ManyToManyField(
        'order.OrderCategory',
        verbose_name=u'Категории', null=True, blank=True)
    type = models.CharField(max_length=255, verbose_name=u'Тип услуг', choices=COMPANY_TYPE, null=True, blank=True)
    country = models.ForeignKey(
        'geo.Country', verbose_name=_(u'Страна'),
        null=True, blank=True)
    region = ChainedForeignKey(
        'geo.Region', verbose_name=_(u'Область'), null=True, blank=True,
        chained_field='country', chained_model_field='country', auto_choose=True)
    city2 = ChainedForeignKey(
        'geo.City', verbose_name=_(u'Город'), null=True, blank=True,
        chained_field='region', chained_model_field='region', auto_choose=True)
    city = models.CharField(
        verbose_name=u'Город', null=True, max_length=255, blank=True)
    phone = models.CharField(max_length=255, verbose_name=u'Телефон')
    email = models.EmailField(max_length=255, verbose_name=u'E-mail')
    country_services = models.ManyToManyField(
        'geo.Country', verbose_name=u'Страны обслуживания',
        help_text=u'Только для компаний', blank=True, null=True, related_name='country_service')
    show_email = models.BooleanField(u'Отображать e-mail на сайте', default=True)
    send_mail = models.BooleanField(u'Получать уведомления о новых заказах', default=True)
    site = models.CharField(max_length=255, verbose_name=u'Сайт', blank=True, null=True)
    site_active = models.BooleanField(verbose_name=u'Ссылка на сайт', default=False)
    # description = RichTextField(verbose_name=u'О компании', config_name='simple2', max_length=2500, blank=True, null=True)
    description = models.TextField(verbose_name=u'О компании', max_length=2500, blank=True, null=True)
    user = models.OneToOneField(User, verbose_name=u'Пользователь', blank=True, null=True)
    alternative = models.ManyToManyField('self', verbose_name=u'Кто вместо', blank=True, null=True, symmetrical=False)
    comments = models.IntegerField(default=0)
    title = models.CharField(max_length=255, verbose_name=u'Seo тайтл', blank=True, null=True)
    h1 = models.CharField(max_length=255, verbose_name=u'Seo h1', blank=True, null=True)
    h2 = models.CharField(max_length=255, verbose_name=u'Seo h2', blank=True, null=True)
    head2 = models.CharField(max_length=255, verbose_name=u'Заголовок отзывов', null=True, blank=True)
    seo_description = models.TextField(verbose_name=u'Seo Description', blank=True, null=True)
    seo_keywords = models.TextField(verbose_name=u'Seo Keywords', blank=True, null=True)
    active = models.BooleanField(u'Активна', default=False)
    ip = models.IPAddressField(verbose_name=u'IP-адрес', blank=True, null=True)
    total_quality_service = models.FloatField(u'Качество сервиса', default=2.0)
    total_conform_terms_delivery = models.FloatField(u'Соответствие заявленным срокам доставки', default=2.0)
    total_convenience_location_offices = models.FloatField(u'Удобство расположения офисов', default=2.0)
    total_neatness_politeness_courier = models.FloatField(u'Опрятность и вежливость курьеров', default=2.0)
    total_price_quality = models.FloatField(u'Соотношение "цена-качество"', default=2.0)
    total_scope = models.FloatField(u'Общий балл', default=2.0)
    uuid = models.CharField(
        u'Идентификатор', max_length=64, default=lambda: uuid.uuid4().get_hex(), editable=False)

    class Meta(object):
        verbose_name = u'Компания или курьер'
        verbose_name_plural = u'Компании и курьеры'
        ordering = ['-comments']

    def __unicode__(self):
        return self.name

    def is_courier(self):
            return self.type == 'courier'

    def get_city(self):
        if self.city:
            return City.objects.get(id=self.city).name
        return

    def has_payment(self):
        return bool(CompanyPayment.objects.filter(company=self))

    def vote_average(self):
        return (self.total_quality_service + self.total_conform_terms_delivery + self.total_convenience_location_offices + self.total_neatness_politeness_courier + self.total_price_quality) / 5

    @property
    def get_address(self):
        if self.city2 and self.region:
            return u'%s, %s' % (self.city2, self.region)
        elif self.city2:
            return self.city2
        else:
            return ''

    @permalink
    def get_absolute_url(self):
        return 'company', (self.type, self.id, ), {}

    # def count_active_comments(self):
    #     return self.respondes.count()


def check_type_user(self):
    try:
        return 'courier' if self.company.is_courier() else 'company'
    except Company.DoesNotExist:
        return 'user'


def get_phone(self):
    try:
        return Profile.objects.get(user=self).phone
    except Profile.DoesNotExist:
        try:
            return self.company.phone
        except Company.DoesNotExist:
            return


def is_company(self):
    try:
        return not self.company.is_courier()
    except Company.DoesNotExist:
        return False


def is_courier(self):
    try:
        return self.company.is_courier()
    except Company.DoesNotExist:
        return False


def is_carrier(self):
    try:
        return self.company
    except Company.DoesNotExist:
        return False

User.add_to_class('check_type_user', check_type_user)
User.add_to_class('get_phone', get_phone)
User.add_to_class('is_company', is_company)
User.add_to_class('is_courier', is_courier)
User.add_to_class('is_carrier', is_carrier)


class Vote(models.Model):
    VOTE_TYPE = (
        (1, u'Качество сервиса'),
        (2, u'Соответствие заявленным срокам доставки'),
        (3, u'Удобство расположения офисов'),
        (4, u'Опрятность и вежливость курьеров'),
        (5, u'Соотношение "цена-качество"'),
    )
    company = models.ForeignKey(Company, verbose_name=u'Компания')
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    vote_type = models.IntegerField(u'Тип голосования', choices=VOTE_TYPE)
    created = models.DateTimeField(u'Дата голосования', auto_now_add=True)
    scope = models.PositiveIntegerField(u'Оценка')

    class Meta:
        verbose_name = verbose_name_plural = u'Голосование'

    def __unicode__(self):
        return self.company.name


class City(models.Model):
    class Meta(object):
        verbose_name = u'Город'
        verbose_name_plural = u'Города'
        ordering = ['name']

    name = models.CharField(max_length=255, verbose_name=u'Название города')

    def __unicode__(self):
        return self.name

MARK = (
    ('negative', u'Отрицательный'),
    ('positive', u'Положительный'),
    ('neitral', u'Нейтральный')
)


class Respond(MPTTModel):
    company = models.ForeignKey(Company, verbose_name=u'Компания', related_name='respondes')
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата', null=True, blank=True)
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    email = models.EmailField(max_length=255, verbose_name=u'E-mail')
    mark = models.CharField(max_length=50, choices=MARK, verbose_name=u'Оценка')
    order = models.CharField(max_length=50, verbose_name=u'Номер накладной', null=True, blank=True)
    text = models.TextField(verbose_name=u'Текст отзыва', max_length=2500)
    moderate = models.BooleanField(verbose_name=u'Модерация', default=False)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    ip = models.IPAddressField(verbose_name=u'IP-адрес', blank=True, null=True)
    uuid = models.CharField(u'Идентификатор', max_length=64)
    confirm = models.BooleanField(u'Подтверждён', default=False)

    class MPTTMeta:
        level_attr = 'mptt_level'

    class Meta(object):
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'
        ordering = ['-created']

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            Respond.tree.insert_node(self, self.parent)
        super(Respond, self).save(*args, **kwargs)
        c = Respond.objects.filter(company=self.company, moderate=True, confirm=True).count()
        self.company.comments = c
        self.company.save()

POLL = (
    ('pro', u'За'),
    ('con', u'Против')
)

class Poll(models.Model):
    class Meta(object):
        verbose_name = u'Голос'
        verbose_name_plural = u'Голоса'
        ordering = ['-created']

    company = models.ForeignKey(Company, verbose_name=u'Компания', related_name='polls')
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата', null=True, blank=True)
    choice = models.CharField(max_length=10, choices=POLL)
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    email = models.EmailField(max_length=255, verbose_name=u'E-mail')
    uuid = models.CharField(u'Идентификатор', max_length=64)
    confirm = models.BooleanField(u'Подтверждён', default=False)

#    def __unicode__(self):
#        return "%s" % self.company


    # def save(self, force_insert=False, force_update=False, using=None, recalc=False):
    #     super(Poll, self).save()
    #     today = date.today()
    #     if recalc:
    #         polls = Poll.objects.filter(company=self.company, created__month=today.month, created__year=today.year, confirm=True)
    #         qty = polls.count()
    #         if qty:
    #             pro = (float(polls.filter(choice='pro').count())/qty)*100
    #             con = (float(polls.filter(choice='con').count())/qty)*100
    #             company = Company.objects.filter(id=self.company.id, active=True)
    #             company.update(pro=int(pro), con=int(con))
    #         else:
    #             company = Company.objects.filter(id=self.company.id, active=True)
    #             company.update(pro=0, con=0)


class Tender(models.Model):
    class Meta(object):
        verbose_name = u'Заявка на доставку'
        verbose_name_plural = u'Заявки на доставку'
        ordering = ['-created']

    name = models.CharField(max_length=255, verbose_name=u'Имя')
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата', null=True, blank=True)
    point_a = models.CharField(max_length=255, verbose_name=u'Откуда')
    point_b = models.CharField(max_length=255, verbose_name=u'Куда')
    cargo = models.CharField(max_length=255, verbose_name=u'Что перевозим')
    weight = models.CharField(max_length=255, verbose_name=u'Вес', null=True, blank=True)
    width = models.CharField(max_length=255, verbose_name=u'Ширина', null=True, blank=True)
    height = models.CharField(max_length=255, verbose_name=u'Высота', null=True, blank=True)
    length = models.CharField(max_length=255, verbose_name=u'Длина', null=True, blank=True)
    cargo_description = models.TextField(verbose_name=u'Текст отзыва', max_length=2500,null=True, blank=True)
    phone = models.CharField(max_length=255, verbose_name=u'Телефон', null=True, blank=True)
    email = models.CharField(max_length=255, verbose_name=u'E-mail')
    date = models.CharField(max_length=255, verbose_name=u'Число', null=True, blank=True)
    time = models.CharField(max_length=255, verbose_name=u'Время', null=True, blank=True)
    type = models.CharField(max_length=255, verbose_name=u'Тип', null=True, blank=True)

    def send_link(self):
        return u'<a href="/tender/send/%s/">Разослать</a>' % self.id
    send_link.allow_tags = True


class SpecialOffer(models.Model):
    class Meta(object):
        verbose_name = u'Специальное предложение'
        verbose_name_plural = u'Специальные предложения'

    company = models.ForeignKey(Company, verbose_name=u'Компания', related_name='offers')
    start_date = models.DateField(verbose_name=u'Дата начала')
    url = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Ссылка на сайт')
    end_date = models.DateField(verbose_name=u'Дата конца')
    offer = models.CharField(max_length=255, verbose_name=u'Акция')
    description = RichTextField(verbose_name=u'Описание акции', config_name='simple', max_length=2500, blank=True, null=True)


    def __unicode__(self):
        return "%s" % self.offer

    @permalink
    def get_absolute_url(self):
        return 'special_offer', (self.company.type, self.company.id, self.id,), {}

PLACE_BANNER = (
    ('top', u'Сверху'),
    ('right', u'Справа')
)

TARGET_BANNER = (
    ('_blank', u'Загружает страницу в новое окно браузера.'),
    ('_self', u'Загружает страницу в текущее окно.')
)


class Banner(models.Model):
        class Meta(object):
            verbose_name = u'Баннер'
            verbose_name_plural = u'Баннеры'
            ordering = ['id']

        title = models.CharField(max_length=255, verbose_name=u'Название')
        url = models.CharField(max_length=255, verbose_name=u'Ссылка',null=True, blank=True)
        place = models.CharField(max_length=255, verbose_name=u'Место размещения', choices=PLACE_BANNER)
        target = models.CharField(max_length=255, verbose_name=u'Назначение', default='_self', choices=TARGET_BANNER)
        image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path, null=True, blank=True)
        swf = models.FileField(verbose_name=u'SWF баннер', upload_to=get_file_path, null=True, blank=True)
        swf_width = models.IntegerField(verbose_name=u'Ширина SWF баннера', default=0)
        swf_height = models.IntegerField(verbose_name=u'Высота SWF баннера', default=0)
        code = models.TextField(max_length=255, verbose_name=u'Код баннера', null=True, blank=True)
        publish = models.BooleanField(u'Публиковать', default=True)

        def __unicode__(self):
            return "%s" % self.title

        def get_view(self):
            if self.image:
                return '<a href="%s" target="%s"><img src="%s" alt="%s"></a>' % (self.url,self.target, self.image.url, self.title)
            elif self.swf:
                return '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="%s" height="%s" id="banner" title="%s" align="middle"><param name="wmode" value="opaque" /><param name="allowScriptAccess" value="sameDomain" /><param name="allowFullScreen" value="false" /><param name="movie" value="%s" /><param name="quality" value="high" /><param name="bgcolor" value="#ffffff" /><embed wmode="opaque" src="%s" quality="high" bgcolor="#ffffff" width="%s" height="%s" name="elka" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" /></object>' % (self.swf_width, self.swf_height, self.title, self.swf.url, self.swf.url, self.swf_width, self.swf_height)
            elif self.code:
                return self.code
            else:
                return ''

class Settings(models.Model):
    class Meta(object):
        verbose_name = u'Настройки'
        verbose_name_plural = u'Настройки'
        ordering = ['id']

    title = models.CharField(max_length=255, verbose_name=u'Название')
    value = models.CharField(max_length=255, verbose_name=u'Значение', blank=True, null=True)
    value2 = models.IntegerField(verbose_name=u'Значение',default=0)
    active = models.BooleanField(max_length=255, verbose_name=u'Активность', default=False)

    def __unicode__(self):
        return "%s" % self.title


class DeliveryCompetition(models.Model):
    STATUS = (
        (0, u'Не доставлено'),
        (1, u'Доставлено'),
    )
    CARGO = (
        (1, u'Письмо'),
        (2, u'Бандероль'),
    )
    WEIGHT = (
        (1, u'до 1кг'),
        (2, u'до 2кг'),
        (3, u'до 3кг'),
        (4, u'до 4кг'),
        (5, u'до 5кг'),
        (6, u'до 6кг'),
        (7, u'до 7кг'),
        (8, u'до 8кг'),
        (9, u'до 9кг'),
        (10, u'до 10кг'),
        (11, u'Больше 10кг'),
    )
    uuid = UUIDField(auto=True)
    company = models.ForeignKey(Company, verbose_name=u'Компания')
    user = models.ForeignKey(User, verbose_name=u'Пользователь', blank=True, null=True)
    cargo = models.IntegerField(u'Что перевозим', choices=CARGO)
    weight = models.IntegerField(u'Вес', choices=WEIGHT)
    width = models.CharField(u'Ширина', max_length=255, null=True, blank=True)
    height = models.CharField(u'Высота', max_length=255, null=True, blank=True)
    length = models.CharField(u'Длина', max_length=255, null=True, blank=True)
    delivery_date = models.DateField(u'Дата доставки')
    delivery_time = models.CharField(u'Желаемое время доставки', max_length=20, blank=True, null=True)
    from_address = models.CharField(u'Адрес', max_length=100)
    from_fio = models.CharField(u'ФИО отправителя / Организация', max_length=100)
    from_phone = models.CharField(u'Телефон', max_length=50)
    from_email = models.EmailField(u'Ваш E-mail')
    to_address = models.CharField(u'Адрес', max_length=100)
    to_fio = models.CharField(u'ФИО получателя / Организация', max_length=100)
    to_phone = models.CharField(u'Телефон', max_length=50)
    cargo_description = models.TextField(u'Текст отзыва', max_length=2500, blank=True)
    status = models.PositiveIntegerField(u'Статус', choices=STATUS, default=0)
    price = models.PositiveIntegerField(u'Стоимость заказа')
    payment_type = models.ForeignKey('PaymentType', verbose_name=u'Способ оплаты')
    urgency = models.BooleanField(u'Срочность', default=False)
    created = models.DateTimeField(u'Дата', default=datetime.now, editable=False)

    class Meta:
        verbose_name = u'Конкурс'
        verbose_name_plural = u'Конкурс на доставку'

    def get_payment(self):
        return CompanyPaymentField.objects.filter(payment__company=self.company,
                                                  payment__payment_type=self.payment_type)

    def get_absolute_url(self):
        return reverse('competition_detail', args=[self.uuid])

    def __unicode__(self):
        return str(self.cargo)


class DeliveryZonePrice(models.Model):
    zone_fields = OrderedDict([('1kg', u'До 1кг'), ('2kg', u'До 2кг'),
                               ('3kg', u'До 3кг'), ('4kg', u'До 4кг'),
                               ('5kg', u'До 5кг'), ('6kg', u'До 6кг'),
                               ('7kg', u'До 7кг'), ('8kg', u'До 8кг'),
                               ('9kg', u'До 9кг'), ('10kg', u'До 10кг'),
                               ('ot10kg', u'От 10кг')])
    zone = OrderedDict([(1, u'Москва'),
                        (2, u'МКАД ( 3км до и после МКАДа)'),
                        (3, u'За МКАД. до 5 км'),
                        (4, u'За МКАД. до 10 км'),
                        (5, u'За МКАД. до 25 км'),
                        (6, u'За МКАД. до 50 км'),
                        (7, u'За МКАД. до 100 км')])

    company = models.ForeignKey(Company, verbose_name=u'Компания', unique=True)
    '''
    Прейскурант по 7 зонам. Москва, МКАД, МКАД (до 5км), МКАД (до 10км) и т.д.
    '''
    for x in zone.iterkeys():
        for key, value in zone_fields.items():
            exec('zone%s_%s = models.PositiveIntegerField(u"%s", blank=True, null=True)' % (x, key, value))
    urgency = models.PositiveIntegerField(u'Срочность (надбавка за срочность)', blank=True, null=True)

    class Meta:
        verbose_name = u'Расценку зоны'
        verbose_name_plural = u'Расценки на зоны'

    def __unicode__(self):
        return self.company.name


class PaymentType(models.Model):
    '''
    Администратор указывает название платёжки
    '''
    name = models.CharField(u'Название', max_length=100)
    emoney = models.BooleanField(u'Электронные деньги', default=False)
    show_fields = models.BooleanField(u'Показывать да/нет вместо полей', default=False)
    order = models.PositiveIntegerField(u'Порядок', default=0)

    class Meta:
        verbose_name = u'Вариант'
        verbose_name_plural = u'Варианты оплаты'
        ordering = ('order',)

    def __unicode__(self):
        return self.name


class PaymentTypeField(models.Model):
    '''
    Администратор указывает название полей платёжки
    '''
    payment = models.ForeignKey(PaymentType, verbose_name=u'Вариант оплаты')
    name = models.CharField(u'Название поля', max_length=100, unique=True)
    order = models.PositiveIntegerField(u'Порядок', default=0)

    class Meta:
        verbose_name = u'Поле'
        verbose_name_plural = u'Поля'
        ordering = ('order',)

    def __unicode__(self):
        return self.name


class CompanyPayment(models.Model):
    '''
    Платёжки компаний
    '''
    company = models.ForeignKey(Company, verbose_name=u'Компания')
    show_fields = models.BooleanField(u'Показывать да/нет вместо полей', default=False)
    payment_type = models.ForeignKey(PaymentType, verbose_name=u'Вариант оплаты')

    class Meta:
        verbose_name = u'Вариант'
        verbose_name_plural = u'Варианты оплаты указанные компаниями'
        ordering = ('payment_type__order',)

    def __unicode__(self):
        return str(self.pk)


class CompanyPaymentField(models.Model):
    '''
    Значения платёжек компаний
    '''
    payment = models.ForeignKey(CompanyPayment, verbose_name=u'Вариант оплаты')
    value = models.CharField(u'Значение', max_length=100)

    class Meta:
        verbose_name = u'Поле'
        verbose_name_plural = u'Поля'

    def __unicode__(self):
        return self.value
