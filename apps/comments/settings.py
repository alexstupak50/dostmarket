# -*- coding: utf-8 -*-
from django.conf import settings


def get(key, default):
    return getattr(settings, key, default)


COMMENTS_ANONYMOUS = get('COMMENTS_ANONYMOUS', True)
