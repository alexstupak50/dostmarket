# -*- coding:utf-8 -*-
from django.contrib import admin

from .models import Comments
from fein_admin_menu.admin import tree_editor


class CommentsAdmin(admin.ModelAdmin):
    list_display = ('user', 'anonymous', 'text', 'created', 'publish')
    readonly_fields = ('user', 'anonymous', 'created', 'ip',
                       'content_type', 'object_pk', 'content_object')
    list_filter = ('publish',)
    ordering = ['-created']

    def has_add_permission(self, *args, **kwargs):
        return

admin.site.register(Comments, CommentsAdmin)
