from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'comments/new/$', 'comments.views.comments_add', name='comments_add'),
)
