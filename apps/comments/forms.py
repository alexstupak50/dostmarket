# -*- coding: utf-8 -*-
from django import forms
from models import Comments

from comments import settings as comments_settings
# from captcha.fields import CaptchaField


class CommentsForm(forms.ModelForm):

    class Meta:
        model = Comments
        widgets = {'likes':  forms.HiddenInput,
                   'content_type': forms.HiddenInput,
                   'object_pk': forms.HiddenInput
        }
        exclude = ('user', 'parent', 'created', 'publish', 'ip')

    def __init__(self, target_object, user, data=None, initial=None):
        super(CommentsForm, self).__init__(data=data, initial=initial)
        self.user = user

        if user.is_authenticated() or not comments_settings.COMMENTS_ANONYMOUS:
            del self.fields['anonymous']
            del self.fields['email']

        '''
        if not self.user.is_authenticated() and comments_settings.COMMENTS_ANONYMOUS:
            self.fields['captcha'] = CaptchaField(label=u'Каптча')
        '''

    def clean_anonymous(self):
        if not self.user.is_authenticated() and comments_settings.COMMENTS_ANONYMOUS:
            if not self.cleaned_data['anonymous']:
                raise forms.ValidationError(u'Это обязательное поле')
        return self.cleaned_data['anonymous']

    def clean_email(self):
        if not self.user.is_authenticated() and comments_settings.COMMENTS_ANONYMOUS:
            if not self.cleaned_data['email']:
                raise forms.ValidationError(u'Это обязательное поле')
        return self.cleaned_data['email']
