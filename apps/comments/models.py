# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db import models
from django.contrib.auth.models import User

from mptt.models import MPTTModel, TreeForeignKey
from order.models import OrderBet


class Comments(MPTTModel):

    class Meta:
        verbose_name = u'Комментарий'
        verbose_name_plural = u'Комментарии'
        get_latest_by = 'created'

    class MPTTMeta:
        order_insertion_by = ['ip']

    user = models.ForeignKey(User, verbose_name=u'Пользователь', blank=True, null=True)
    anonymous = models.CharField(u'Имя', max_length=50, blank=True)  # anonymous name
    parent = TreeForeignKey('self', verbose_name=u'Родитель', blank=True, null=True, related_name='children')
    email = models.EmailField('E-mail', blank=True)
    text = models.TextField(u'Добавить сообщение')
    created = models.DateTimeField(u'Создан', auto_now_add=True)
    publish = models.BooleanField(u'Публиковать', default=True)
    ip = models.IPAddressField(blank=True, null=True)
    content_type = models.ForeignKey(ContentType)
    object_pk = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_pk')
    likes = models.ManyToManyField(
        User, verbose_name=u'Лайки', related_name='likes', null=True, blank=True)

    def get_name(self):
        if self.user:
            return self.user.username
        elif self.anonymous:
            return self.anonymous

    def get_user_bet(self):
        bets = OrderBet.objects.filter(user=self.user, order=self.content_object)
        if bets:
            bet = bets[0]
            return '%d %s' % (bet.price, bet.get_currency_display())
        return

    def __unicode__(self):
        return u'%s' % self.text
