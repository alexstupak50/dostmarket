# -*- coding: utf-8 -*-
from django import template
from django.contrib.contenttypes.models import ContentType

from comments import settings as comments_settings
from comments.forms import CommentsForm
from comments.models import Comments

register = template.Library()


@register.inclusion_tag('comments/comments_add.html', takes_context=True)
def render_form(context, comment_obj):
    """
    Returns html with a form for a given object

    Usage:

        {% render_text_comments_form entry %}

    Where entry is an object instance we want to comments on
    """

    ctype = ContentType.objects.get_for_model(comment_obj)

    initial = {
        'content_type': ctype,
        'object_pk': comment_obj.pk,
        }

    request_user = context['request'].user
    return {
        'form': CommentsForm(comment_obj, request_user, initial=initial),
        'redirect_to': context['request'].path,
        'STATIC_URL': context['STATIC_URL'],
        'anonym_can_comment': comments_settings.COMMENTS_ANONYMOUS,
        'user': request_user
    }


@register.inclusion_tag('comments/comments_list.html', takes_context=True)
def render_comments_list(context, comment_obj):
    """
    Returns html with form for a given object

    Usage:

        {% render_comments_list entry %}

    Where entry is an object instance we want to see comments of
    """
    
    ctype = ContentType.objects.get_for_model(comment_obj)

    comments_list = Comments.objects.filter(content_type=ctype.pk,
                                            object_pk=comment_obj.pk,
                                            publish=True)

    return {
        'comments_list': comments_list,
        'request': context['request'],
        }

@register.inclusion_tag('comments/comments_list_news.html', takes_context=True)
def render_comments_list_news(context, comment_obj):
    """
    Returns html with form for a given object

    Usage:

        {% render_comments_list entry %}

    Where entry is an object instance we want to see comments of
    """

    ctype = ContentType.objects.get_for_model(comment_obj)

    comments_list = Comments.objects.filter(content_type=ctype.pk,
                                            object_pk=comment_obj.pk,
                                            publish=True)

    return {
        'comments_list': comments_list,
        'request': context['request'],
        }

@register.inclusion_tag('comments/comments_add_news.html', takes_context=True)
def render_form_news(context, comment_obj):
    """
    Returns html with a form for a given object

    Usage:

        {% render_text_comments_form entry %}

    Where entry is an object instance we want to comments on
    """

    ctype = ContentType.objects.get_for_model(comment_obj)

    initial = {
        'content_type': ctype,
        'object_pk': comment_obj.pk,
        }

    request_user = context['request'].user
    return {
        'form': CommentsForm(comment_obj, request_user, initial=initial),
        'redirect_to': context['request'].path,
        'STATIC_URL': context['STATIC_URL'],
        'anonym_can_comment': comments_settings.COMMENTS_ANONYMOUS,
        'user': request_user
    }
