# -*- coding: utf-8 -*-
import json
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.views.decorators.http import require_POST
from django.middleware.csrf import _get_new_csrf_key
from django.utils.dateformat import format as dformat
from django.views.generic import TemplateView
from .forms import CommentsForm
from .models import Comments
from news.models import News
from articles.models import Article
from order.utils import JsonResponse
from order.views import EmailMixin
from order.utils import mail_email
from order.models import Order
# from comments import settings as comments_settings
# from captcha.models import CaptchaStore
# from captcha.helpers import captcha_image_url


class AddLike(TemplateView):
    model = Comments

    def get(self, request, *args, **kwargs):
        item = self.model.objects.get(id=kwargs.get('pk'))
        item.likes.add(request.user)
        count = item.likes.count()
        return JsonResponse({'valid': True, 'count': count})


@require_POST
def comments_add(request):

    # Get an object user content_type and object_pk from POST
    ctype = ContentType.objects.get(
        id=request.POST['content_type']
    )

    comment_object = ctype.get_object_for_this_type(
        id=request.POST['object_pk']
    )

    form = CommentsForm(
        comment_object,
        request.user,
        request.POST or None
    )

    if form.is_valid():
        obj = None
        comment = form.save(commit=False)
        try:
            mail_email(
                u'Новый комментарий на сайте(admin)',
                '<a href="http://www.dostmarket.ru%s">%s</a>' % (
                    comment_object.get_absolute_url(), comment_object),
                {},
                settings.DEFAULT_FROM_EMAIL,
                # ['stupak.alexn@gmail.com']
                [settings.A_EMAIL, ]
            )
        except:
            #TODO прописать ошибки
            pass
        try:
            if isinstance(comment_object, News) or isinstance(comment_object, Article):
                comment_object.comments_count += 1
                comment_object.save()
            if request.user.is_authenticated():
                comment.user = request.user
                # emailsend = EmailMixin()
                obj = comment.content_object
                if obj.user != request.user:
                    mail_email(
                        u'Новый комментарий на сайте',
                        'order/inclusion/email_add_comment.html',
                        {'host': 'dostmarket.ru', 'order': obj},
                        settings.DEFAULT_FROM_EMAIL,
                        # ['stupak.alexn@gmail.com']
                        [obj.user.email, ]
                    )
                    # emailsend.send_letter(
                    #     u'Новый комментарий к вашему заказу', obj, [obj.user.email,],
                    #     'order/inclusion/email_add_comment.html'
                    #     )
        except AttributeError:
            pass
        comment.ip = request.META['REMOTE_ADDR']
        comment.parent = None

        target = request.POST.get('parent', None)
        if target == '':
            target = None
        if not target is None:
            target = Comments.objects.get(pk=target)

        comment = Comments.objects.insert_node(comment, target,
                                               position='last-child', save=True)

        if isinstance(comment_object, Order) and comment.parent:
            if comment.parent.user != request.user and obj.user.email != comment.parent.user.email:
                mail_email(
                    u'Вам ответили на ваш комментарий к заказу - %s' % comment_object,
                    'order/inclusion/email_add_comment_parent.html',
                    {'host': 'dostmarket.ru', 'order': comment_object, 'user': comment.parent.user},
                    settings.DEFAULT_FROM_EMAIL,
                    # ['stupak.alexn@gmail.com']
                    [comment.parent.user.email, ]
                )
        if request.is_ajax():
            # to_json_responce = dict()
            # to_json_responce['success'] = True
            # to_json_responce['user'] = comment.user.username
            # to_json_responce['created'] = dformat(comment.created, 'd E Y г. H:m:s')
            # to_json_responce['text'] = comment.text
            # to_json_responce['parent'] = None
            # if not comment.parent is None:
            #     to_json_responce['parent'] = comment.parent.pk
            # to_json_responce['pk'] = comment.pk
            # to_json_responce['level'] = comment.level
            # to_json_responce['csrf'] = _get_new_csrf_key()
            if isinstance(comment_object, News) or isinstance(comment_object, Article):
                template = 'comments/comment_news.html'
            else:
                template = 'comments/comment.html'
            parent_pk = comment.parent.pk if comment.parent else None
            return HttpResponse(json.dumps({
                'html': render_to_string(template, {'node': comment, 'valid': True}),
                'valid': True, 'parent': parent_pk
            }), content_type='application/json')
            '''
            if not request.user.is_authenticated() and comments_settings.COMMENTS_ANONYMOUS:
                to_json_responce['new_cptch_key'] = CaptchaStore.generate_key()
                to_json_responce['new_cptch_image'] = captcha_image_url(to_json_responce['new_cptch_key'])
            '''

            # return HttpResponse(json.dumps(to_json_responce), content_type='application/json')

    else:
        if request.is_ajax():
            to_json_responce = dict()
            to_json_responce['success'] = False
            to_json_responce['form_errors'] = form.errors
            to_json_responce['request'] = request
            '''
            if not request.user.is_authenticated() and comments_settings.COMMENTS_ANONYMOUS:
                to_json_responce['new_cptch_key'] = CaptchaStore.generate_key()
                to_json_responce['new_cptch_image'] = captcha_image_url(to_json_responce['new_cptch_key'])
            '''

            return HttpResponse(json.dumps(to_json_responce), content_type='application/json')

    #FIXME: Redirects to redirect_to (value of the hidden field)
    return redirect(request.POST['redirect_to'])
