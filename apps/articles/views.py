# vim:fileencoding=utf-8
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from articles.models import Article
from apps.news.models import News


class ArticleList(ListView):
    template_name = "news/news_list.html"
    paginate_by = 10
    model = Article
    context_object_name = 'list'

    def get_context_data(self, **kwargs):
        ctx = super(ArticleList, self).get_context_data(**kwargs)
        # articles = news
        ctx['articles'] = News.objects.all()[:7]
        return ctx


    def get_queryset(self):
        tag = self.request.GET.get('tag', False)
        if tag:
            queryset = self.model.objects.filter(category__name=tag)
        else:
            queryset = self.model.objects.all()
        return queryset


class ArticleView(DetailView):
    model = Article
    template_name = 'news/news.html'
    # template_name = 'articles/news.html'
    context_object_name = 'news'


