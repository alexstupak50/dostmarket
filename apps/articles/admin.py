# vim:fileencoding=utf-8
from articles.models import Category, Article
from django.contrib import admin
from sorl.thumbnail import ImageField
from sorl.thumbnail.admin.current import AdminImageWidget

class ArticleAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'date']
    list_filter = ['category']
    list_display_links = ['title']
    filter_horizontal = ['category']
    formfield_overrides = {
        ImageField: {'widget': AdminImageWidget},
        }

admin.site.register(Article, ArticleAdmin)
admin.site.register(Category)