# vim:fileencoding=utf-8
from django.db.models import permalink
import os
import uuid
import settings
from sorl.thumbnail.fields import ImageField
from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(getattr(settings, 'PRODUCT_IMAGE_UPLOAD_TO', 'article/'), filename)

class Article(models.Model):
    class Meta(object):
        verbose_name = u'Статья'
        verbose_name_plural = u'Статьи'
        ordering = ['-date']

    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    date = models.DateTimeField(max_length=255, verbose_name=u'Дата публикации')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path, null=True, blank=True)
    teaser = models.TextField(verbose_name=u'Анонс',blank=True, null=True)
    content = RichTextField(verbose_name=u'Текст')
    category = models.ManyToManyField('Category', verbose_name=u'Категории новости',blank=True, null=True)
    keywords = models.TextField(verbose_name=u'Мета Keywords',blank=True, null=True)
    description = models.TextField(verbose_name=u'Meta Description',blank=True, null=True)
    seo_title = models.CharField(max_length=255, verbose_name=u'Seo тайтл',blank=True, null=True,)
    comments_count = models.PositiveIntegerField(u'Количество комментариев', default=0)

    def __unicode__(self):
        return self.title

    @permalink
    def get_absolute_url(self):
        return 'article', (self.id, ), {}

class Category(models.Model):
    class Meta(object):
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'
        ordering = ['name']

    name = models.CharField(max_length=255, verbose_name=u'Название')

    def __unicode__(self):
        return self.name