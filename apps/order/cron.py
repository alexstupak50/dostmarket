# -*- coding: utf-8 -*-
from django.utils import timezone
import logging
import datetime
from order.models import Order


logging.basicConfig(
    format=u'%(levelname)-8s [%(asctime)s] %(message)s',
    level=logging.DEBUG,
    filename=u'djagno_cron.log')


def order_reset_day_view():
    Order.objects.all().update(views_today=0)
    logging.info('order_reset_day_view')


def order_cancel():
    """
    Завершаем заказы в которых истек срок актуальности
    :return:
    """
    # date
    for order in Order.objects.filter(topicality__lt=(timezone.now()-timezone.timedelta(days=1)), status=0):
        order.status = 6
        order.save()
    logging.info('order_cancel')


def order_cancel_performer():
    """
    Меняем статус заказа и ставки в случае если в течение 2 часов исполнитель не ответил
    :return:
    """
    delta = datetime.timedelta(hours=2)
    orders = Order.objects.filter(confirmation_date__lte=(timezone.now()-delta), status=5)
    for order in orders:
        logging.info(order.id)
        bet = order.get_win_bet()
        bet.status = 2
        bet.reject_kind = 0
        bet.status_time = timezone.now()
        bet.save()
        # торги продолжаються
        order.confirmation_date = None
        order.status = 0
        order.save()
    logging.info('order_cancel_performer')