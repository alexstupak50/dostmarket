#coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from django.utils import timezone
from dateutil import tz
from decimal import Decimal
from django.contrib.auth.models import User
import json

from smart_selects.db_fields import ChainedForeignKey


LOADERS_CHOICES = (
    (1, _(u'Погрузка разгрузка не требуется')),
    (2, _(u'Достаточно помощи водителя при погрузке')),
    (3, _(u'Требуется 1 грузчик для погрузки')),
    (4, _(u'Требуется 2 грузчика для погрузки')),
    (5, _(u'Требуется 3 грузчика для погрузки')),
    (6, _(u'Требуется 4 грузчика для погрузки')),
)


ROUTE_KIND = (
    (1, _(u'Регион')),
    (2, _(u'Маршрут')),
)


SERVICE = (
    (1, _(u'Перевозка/доставка')),
    (2, _(u'Перевозка + грузчик')),
    (3, _(u'Первозка + 2 грузчика')),
    (4, _(u'Первозка + 3 грузчика')),
    (5, _(u'Первозка + 4 грузчика')),
)

CURRENCY = (
    (1, u'руб.'),
    (2, u'грн.'),
    (3, u'блр.'),
    (4, u'кзт.'),
)

TRANSPORTATION_TYPE = (
    (1, _(u'Купить и привезти')),
    (2, _(u'Нужна только перевозка'))
)

TRANSPORTATION_TYPE_BET = (
    (1, _(u'Отдельная машина')),
    (2, _(u'Сборный груз (догруз)'))
)

ADDRESS_TYPE = (
    (1, _(u'Начальный пункт')),
    (2, _(u'Конечный пункт'))
)

ORDER_BET_STATUS = (
    (0, _(u'Ждет подтверждения')),
    (1, _(u'Ставка принята')),
    (2, _(u'Ставка отклонена')),
)

ORDER_BET_REJECT_KIND_CUSTOMER = (
    (1, _(u'Не устраивает сумма перевозки')),
    (2, _(u'Не устраивает дата/время перевозки')),
)

ORDER_BET_REJECT_KIND = (
    (0, _(u'Отклонена заказчиком')),) +\
    ORDER_BET_REJECT_KIND_CUSTOMER + (
    (11, _(u'Обновлена')),
    (12, _(u'Отклонена перевозчиком')),
)


ORDER_STATUS = (
    (0, _(u'Идут торги')),
    # (5, _(u'Подтверждение перевозчиком')),
    (5, _(u'Ожидает подтверждения')),
    (1, _(u'Выполняется')),
    (2, _(u'Завершен')),
    (3, _(u'Не выполнен')),
    (4, _(u'Отменен')),
    (6, _(u'Не актуален')),
)

MODALITIES = (
    (1, _(u'Фургон')),
    (2, _(u'Эвакуатор')),
    (3, _(u'Ж/д транспорт')),
    (4, _(u'Автоваз')),
    (5, _(u'Своим ходом')),
    (6, _(u'Буксировка')),
)

REVIEW_KIND = (
    (1, _(u'Положительный')),
    (2, _(u'Отрицательный')),
    (3, _(u'Нейтральный')),
)


class Profile(models.Model):
    user = models.OneToOneField(User)
    phone = models.CharField(max_length=255, blank=True, null=True, verbose_name='phone')

    class Meta:
        ordering = ['user']
        verbose_name = 'user'
        verbose_name_plural = 'users'


class OrderCategory(models.Model):
    name = models.CharField(_(u'Название'), max_length=120)
    icon = models.ImageField(
        _(u'Иконка'), upload_to='order_category/icon', null=True, blank=True)
    icon_white = models.ImageField(
        _(u'Белая иконка'), upload_to='order_category/icon_white', null=True, blank=True)
    ordering = models.IntegerField(_(u'Порядок вывода'), default=1)
    block_width = models.IntegerField(
        _(u'Ширина блока'), default=1,
        help_text=_(u'От 1 до 4. Имееться введу ширина блока в плитке'
                    u' на главной странице'))
    name_description = models.CharField(
        _(u'Описание загаловка'), blank=True, null=True,
        max_length=255, help_text=_(u'Выводиться при оформлении заказа в поле'
                                    u' заголовка в качестве placeholder'))
    name_example = RichTextField(
        _(u'Пример заговловка'), blank=True, null=True,
        max_length=255, help_text=_(u'Выводиться при оформлении заказа в поле '
                                    u'заголовка при наведении на поле'))
    name_options = models.CharField(
        _(u'Варианты заголовка'), blank=True, null=True,
        max_length=255, help_text=_(u'Через запятую. Выводиться в при '
                                    u'оформлении заказа ниже поля заголовка'))
    name_options_erase = models.BooleanField(
        _(u'Очищать поле заголовка при выборе нового варианта'),
        default=False
    )
    body_description = models.TextField(
        _(u'Описание доп. информации'), blank=True, null=True,
        help_text=_(u'Выводиться при оформлении заказа в поле '
                    u'доп. информации в качестве placeholder'))
    body_example = RichTextField(
        _(u'Пример доп. информации'), blank=True, null=True,
        help_text=_(u'Выводиться при оформлении заказа в поле '
                    u'доп. информации при наведении на поле'))
    body_options = models.CharField(
        _(u'Варианты доп. информации'), blank=True, null=True, max_length=255,
        help_text=_(u'Через запятую. Выводиться в при оформлении заказа '
                    u'ниже поля доп. информации'))
    cargo_weight = models.BooleanField(
        _(u'Вес груза'), default=False,
        help_text=_(u'Показывать поле "вес груза" при оформлении заказа'))
    cargo_volume = models.BooleanField(
        _(u'Объем груза'), default=False,
        help_text=_(u'Показывать поле "объем груза" при оформлении заказа'))
    loaders = models.BooleanField(
        _(u'Грузчики'), default=False,
        help_text=_(u'Показывать поле "грузчики" при оформлении заказа'))
    can_be_distilled = models.BooleanField(
        _(u'Можно перегнать'), default=False,
        help_text=_(u'Показывать поле "можно перегнать" при оформлении заказа'))
    not_on_move = models.BooleanField(
        _(u'Не на ходу'), default=False,
        help_text=_(u'Показывать поле "не на ходу" при оформлении заказа'))
    transportation_type = models.BooleanField(
        _(u'Только перевезти или перевезти и купить'), default=False,
        help_text=_(u'Показывать поле "Только перевезти или перевезти и купить" '
                    u'при оформлении заказа'))
    services = models.ManyToManyField(
        'OrderService', verbose_name=_(u'Услуги перевозчика'),
        null=True, blank=True)
    count_items = models.BooleanField(
        _(u'Множественное количество обьектов перевозки'), default=False,
        help_text=_(u'Возможность добавить дополнительное поле для обьекта перевозки.'))
    carriage_passengers = models.BooleanField(
        _(u'Перевозка пасажиров'), default=False,
        help_text=_(u'Показывать поле перевозки пасажиров'))
    modalities = models.BooleanField(
        _(u'Доступные споособы перевозок'), default=False,
        help_text=_(u'Показывать поле "Доступные споособы перевозок" '
                    u'при оформлении заказа'))
    transportation_type_in_bet = models.BooleanField(
        _(u'Тип перевозки при оформлении ставки'), default=False,
        help_text=_(u'Показывать выбор типа перевозки(Отдельная машина/Сборный '
                    u'груз(догруз)) при оформлении ставки'))
    modalities_bet = models.BooleanField(
        _(u'Тип перевозки'), default=False,
        help_text=_(u'Показывать поле "Тип перевозки" '
                    u'при отклике на заказ'))

    def get_fields(self):

        fields_list = ['cargo_weight', 'cargo_volume', 'loaders',
                       'can_be_distilled', 'not_on_move', 'body_description',
                       'buy_and_transportation', 'only_transportation']
        fields = dict()
        for field in fields_list:
            fields[field] = getattr(self, field, False)
        return fields

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _(u'Категория заказа')
        verbose_name_plural = _(u'Категории заказа')
        ordering = ('ordering',)


class OrderService(models.Model):
    name = models.CharField(_(u'Название'), max_length=120)
    icon = models.ImageField(
        _(u'Иконка'), upload_to='order_service/icon', null=True, blank=True)
    description = models.TextField(_(u'Описание'), null=True, blank=True)
    ordering = models.IntegerField(_(u'Порядок вывода'), default=1)

    class Meta:
        verbose_name = _(u'Услуга перевозчика')
        verbose_name_plural = _(u'Услуги перевозчика')
        ordering = ('ordering',)

    def __unicode__(self):
        return unicode(self.name)


class OrderAddress(models.Model):
    country = models.ForeignKey(
        'geo.Country', verbose_name=_(u'Страна'), null=True, blank=True)
    region = ChainedForeignKey(
        'geo.Region', verbose_name=_(u'Область'), null=True, blank=True,
        chained_field='country', chained_model_field='country', auto_choose=True)
    city = ChainedForeignKey(
        'geo.City', verbose_name=_(u'Город'), null=True, blank=True,
        chained_field='region', chained_model_field='region', auto_choose=True)
    order = models.ForeignKey(
        'order.Order', related_name='addresses', verbose_name=_(u'Заказ'))
    long_position = models.DecimalField(max_digits=16, decimal_places=8,
                                        default=Decimal('0.00'))
    lat_position = models.DecimalField(max_digits=16, decimal_places=8,
                                       default=Decimal('0.00'))
    type = models.SmallIntegerField(
        _(u'Тип пункта'), choices=ADDRESS_TYPE, default=1)
    address = models.CharField(
        _(u'Улица, дом'), max_length=255, blank=True)

    # def __unicode__(self):
    #     return '%s-%s' % (self.country.name, self.region.name)


class OrderName(models.Model):
    name = models.CharField(_(u'Заголовок'), max_length=255)
    count = models.PositiveIntegerField(_(u'Количество'), default=1)
    order = models.ForeignKey('Order', related_name='order_name')


class OrderManager(models.Manager):
    def get_count_new_bet(self, user):
        """ Возращаем количество новых ставок к заказу """
        return self.filter(bets__is_read=False, user__id=user.id).count()

    def get_auto_selected(self):
        return self.filter(auto_selected=True)


class Order(models.Model):
    user = models.ForeignKey(
        User, related_name='orders',
        verbose_name=_(u'Заказчик'))
    name = models.CharField(_(u'Заголовок'), max_length=255)
    body = models.TextField(
        _(u'Дополнительная информация'), blank=True, null=True)
    category = models.ForeignKey(
        'order.OrderCategory', related_name='orders',
        verbose_name=_(u'Категория'))
    photo = models.ImageField(
        _(u'Фото'), upload_to='order/photo', blank=True, null=True)
    cargo_weight = models.FloatField(
        _(u'Вес груза'), blank=True, null=True,
        help_text=_(u'в килограммах(кг)'))
    cargo_volume = models.FloatField(
        _(u'Обьем груза'), blank=True, null=True,
        help_text=_(u'в кубических метрах(м3)'))
    loaders = models.ForeignKey(
        'OrderService', verbose_name=_(u'Грузчики'), related_name='orders',
        null=True, blank=True)
    modalities = models.SmallIntegerField(
        _(u'Способы перевозок'), choices=MODALITIES, null=True, blank=True)
    can_be_distilled = models.BooleanField(_(u'Можно перегнать'), default=False)
    not_on_move = models.BooleanField(_(u'Не на ходу'), default=False)
    buy_and_transportation = models.SmallIntegerField(
        _(u'Тип перевозки'), choices=TRANSPORTATION_TYPE, blank=True, null=True)
    date = models.DateField(_(u'Дата перевозки'), blank=True, null=True)
    time = models.CharField(_(u'Время'), max_length=10, blank=True, null=True)
    created = models.DateTimeField(_(u'Дата создания'), auto_now_add=True)
    start_address = models.ForeignKey(
        'OrderAddress', related_name='order_start', null=True, blank=True)
    end_address = models.ForeignKey(
        'OrderAddress', related_name='order_end', null=True, blank=True)
    price_max = models.DecimalField(
        _(u'Максимальная цена'), max_digits=8, decimal_places=2,
        blank=True, null=True)
    is_hide = models.BooleanField(_(u'Скрыть'), default=False)
    topicality = models.DateTimeField(_(u'Время на торги'), blank=True, null=True)
    distance = models.FloatField(default=0.0, verbose_name=_(u'Расстояние'),
                                 help_text=_(u'км'))
    views = models.IntegerField(_(u'Просмотров'), default=0)
    views_today = models.IntegerField(_(u'Просмотров за сегодня'), default=0)
    status = models.SmallIntegerField(
        _(u'Статус'), choices=ORDER_STATUS, default=0)
    currency = models.SmallIntegerField(
        _(u'Валюта'), choices=CURRENCY, default=1)
    performer = models.ForeignKey(
        User, verbose_name=_(u'Исполнитель'),
        related_name='orders_performer', null=True, blank=True)
    confirmation_date = models.DateTimeField(
        _(u'Дата окончания подтверждения'), null=True, blank=True)
    is_main = models.BooleanField(u'Отображать на главной', default=False)
    timezone = models.CharField(
        _(u'Часовой поис заказа'), max_length=50, default='UTC')
    auto_selected = models.BooleanField(u'Автоматически выбран исполнитель', default=False)
    payment = models.ForeignKey('company.PaymentType', related_name='orders', null=True, blank=True)
    objects = OrderManager()

    BETS_ERRORS = {
        'auth': _(u'Для того, чтобы сделать ставку, вам необходимо авторизоваться в качестве компании или зарегистрироваться'),
        'closed': _(u'Для данного заказа торги закрыты'),
        'create_order': _(u'Вы не можете делать ставку для своего заказа'),
        'carrier': _(u'Чтобы участвовать в торгах вам необходимо быть авторизованным в качестве курьерской или транспортной компании.'),
        'is_actual': _(u'Заказ не актуален')
    }

    def __unicode__(self):
        # name = ','.join([item.name for item in self.order_name.all()])
        return u'№%d - %s' % (self.id,  self.name)

    class Meta:
        verbose_name = _(u'Заказ')
        verbose_name_plural = _(u'Заказы')

    def get_absolute_url(self):
        return reverse_lazy('order:order_detail', kwargs={'pk': self.id})

    @property
    def get_id(self):
        return self.pk + 1000

    def get_fields(self):
        fields_list = ['cargo_weight', 'cargo_volume', 'loaders',
                       'can_be_distilled','not_on_move',
                       'buy_and_transportation']
        fields = []
        for field in fields_list:
            fields.append({
                'val': getattr(self, field, False),
                'name': unicode(self._meta.get_field(field).verbose_name)
            })
        return fields

    def get_position(self):
        l = [
            self.start_address.long_position, self.start_address.lat_position,
            self.end_address.long_position, self.end_address.lat_position,
        ]
        return json.dumps([float(p) for p in l])


    def get_bets_count(self):
        return self.bets.count()

    def get_datetime(self):
        if not self.time or not self.date:
            return None
        hour = int(self.time[0:2])
        minute = int(self.time[3:5])
        return timezone.datetime(
            year=self.date.year, month=self.date.month, day=self.date.day,
            hour=hour, minute=minute, tzinfo=tz.gettz('UTC'))

    def get_name(self):
        try:
            return self.order_name.all()[0].name
        except IndexError:

            return self.name or u'Имя не определено'

    def get_name_list(self):
        try:
            return ", ".join([order.name for order in self.order_name.all()])
        except IndexError:
            return self.name or u'Имя не определено'

    def get_related_name(self):
        try:
            return self.order_name.all()
        except IndexError:
            return 0

    def get_win_bet(self):
        """Возвращает ставку, которая выиграла"""
        try:
            return self.bets.filter(status=1)[0]
        except (ObjectDoesNotExist, IndexError):
            return None

    def is_can_add_bet(self):
        """Можно делать ставки?"""
        if self.status == 0:
            return True
        return False

    def get_carrier(self):
        """Возвращает перевозчика"""
        return self.performer

    def is_user_can_bet(self, user):
        """Может ли юзер делать ставки для данного заказа.
           Возвращает возможность(bool), текст ошибки(char or None),
           тип ошибки(char or None)
        """
        validations = ['auth', 'closed', 'carrier', 'create_order', 'is_actual']
        for func_name in validations:
            if not getattr(self, '_bet_validate_%s' % func_name)(user):
                return False, self.BETS_ERRORS[func_name], func_name
        return True, None, None

    def _bet_validate_closed(self, user):
        return self.is_can_add_bet()

    def _bet_validate_auth(self, user):
        return user.is_authenticated()

    def _bet_validate_carrier(self, user):
        return user.is_company() or user.is_courier()

    def _bet_validate_create_order(self, user):
        if user.is_company() or user.is_courier():
            return self.user != user
        return True

    def _bet_validate_is_actual(self, user):
        return self.is_actual()

    def is_fire(self):
        """Горячий заказ?"""
        if (self.topicality - timezone.now()).days <= 2 \
                and timezone.now() <= self.topicality:
            return True
        return False

    def is_actual(self):
        """Заказ актуален?"""
        if timezone.now() < self.topicality:
            return True
        return False

    def get_remain_day(self):
        """Сколько осталось дней актуальности заказа"""
        days = (self.topicality - timezone.now()).days
        return 0 if days < 0 else days

    def get_new_bets(self):
        return self.bets.filter(is_read=False)

    def get_status_active(self):
        """
        True если стасут еще : обрабатуеться(в торгах, ждет подтверждения, выполняеться)
        """
        return True if self.status in [1, 5, 0] else False

    # def is_new_bet(self):
    #     return timezone.now() > self.last_action

    # def is_add_review(self):
    #     """Можно оставить отзыв о перевозчике?"""
    #     if (self.status == 2 or self.status == 3) and not self.is_have_review():
    #         return True
    #     return False

    def is_can_close(self):
        """Можно закрыть заказ?"""
        if self.status == 1:
            return True
        return False

    def get_order_id(self):
        return self.id

    def is_can_cancel(self):
        """Можно отменить заказ?"""
        if self.status == 0:
            return True
        return False

    def is_can_edit(self):
        if self.status in [4, 3, 2, 1, 6]:
            return False
        return True

    # def is_can_edit(self):
    #     """Можно изменить заказ?"""
    #     if self.status == 0:
    #         return True
    #     return False
    #
    # def is_can_add_bet(self):
    #     """Можно делать ставки?"""
    #     if self.status == 0:
    #         return True
    #     return False
    #
    # def is_show_phone(self):
    #     """Показывать телефон заказщика?"""
    #     if self.status == 1:
    #         return True
    #     return False

    #
    # def get_start_price(self):
    #     """Стартовая цена"""
    #     if self.bets.exists():
    #         return self.bets.order_by('created').first().price
    #     return None
    #
    # def get_active_bet_by_user(self, user):
    #     """Возвращает текущую ставку юзера"""
    #     try:
    #         return self.bets.get(status=0, user=user)
    #     except OrderBet.DoesNotExist:
    #         return None
    #
    # def get_best_bet(self):
    #     """Возвращает лучшую цену в заказе"""
    #     if self.bets.exists():
    #         return self.bets.order_by('price').first().price
    #     return None

    # def get_active_bets(self):
    #     """Возвращает активные ставки"""
    #     return self.bets.filter(status=0)

    # def set_timezone(self):
    #     """Устанавливаем часовой поис в зависимости от стартового адреса"""
    #     self.timezone = geocoder.timezone([
    #         self.start_address.lat_position,
    #         self.start_address.long_position]).timezone_id or 'UTC'
    #
    # def refresh_dates_by_timezone(self):
    #     """Переводим дату и время введенное юзером в UTC поис"""
    #     self.set_timezone()
    #
    #     if self.get_datetime():
    #         dt = self.get_datetime().replace(
    #             tzinfo=tz.gettz(self.timezone)).astimezone(tz.gettz('UTC'))
    #         self.date = dt.date()
    #         self.time = '%2d:%2d' % (dt.hour, dt.minute)
    #         self.end_trading = self.end_trading.replace(
    #             tzinfo=tz.gettz(self.timezone)).astimezone(tz.gettz('UTC'))


class OrderBet(models.Model):
    order = models.ForeignKey(
        'Order', verbose_name=_(u'Заказ'), related_name='bets')
    user = models.ForeignKey(
        User, verbose_name=_(u'Перевозчик'), related_name='bets')
    status = models.SmallIntegerField(
        _(u'Статус'), choices=ORDER_BET_STATUS, default=0)
    reject_kind = models.SmallIntegerField(
        _(u'Причина отказа'), null=True, blank=True,
        choices=ORDER_BET_REJECT_KIND,
        help_text=_(u'Указываеться если ставка отклонена'))
    status_time = models.DateTimeField(_(u'Время смены статуса'),
                                       null=True, blank=True)
    price = models.DecimalField(
        verbose_name=_(u'Цена'), max_digits=12, decimal_places=2)
    date = models.DateField(verbose_name=_(u'Дата перевозки'), blank=True, null=True)
    time = models.CharField(
        verbose_name=_(u'Время перевозки'), max_length=10, blank=True, null=True)
    # date_topical = models.DateField(verbose_name=_(u'Актуально до(дата)'), blank=True, null=True)
    # time_topical = models.CharField(
    #     verbose_name=_(u'Актуально до(время)'), max_length=10, blank=True, null=True)
    service = models.ForeignKey(
        'OrderService', verbose_name=_(u'Услуга'), related_name='bets',
        null=True, blank=True)
    transportation_type = models.SmallIntegerField(
        _(u'Тип перевозки'), choices=MODALITIES, null=True, blank=True)
    currency = models.SmallIntegerField(
        _(u'Валюта'), choices=CURRENCY, default=1)
    created = models.DateTimeField(_(u'Создано'), auto_now_add=True)
    is_read = models.BooleanField(_(u'Просмотренно заказчком'), default=False)

    class Meta:
        verbose_name = _(u'Ставка')
        verbose_name_plural = _(u'Ставки')
        ordering = ('created', )

    def __unicode__(self):
        return u'%s - %s [%s %s]' % (self.order, self.price, self.date,
                                     self.time)

    def is_change_status(self):
        """Можно ли изменять статус(принять/отклогить) ставки"""
        if self.status == 0 and not self.order.status == 2:
            return True
        return False

    def get_datetime(self):
        """Возвращает обьект datetime на основе полей self.date и self.time
           Необходимо для проверки соотвествия условий ставки
        """
        if self.date and self.time:
            hour = int(self.time[0:2])
            minute = int(self.time[3:5])
            return timezone.datetime(
                year=self.date.year, month=self.date.month, day=self.date.day,
                hour=hour, minute=minute, tzinfo=tz.gettz('UTC'))

    def is_corresponds(self):
        """Ставка соотвествует условиям?"""
        if self.price > self.order.price_max or \
                (not self.order.get_datetime() and
                    self.get_datetime() > self.order.get_datetime()):
            return False
        return True

    def get_order_id(self):
        return self.order.id


class Route(models.Model):
    user = models.ForeignKey(User, related_name='routes', verbose_name=_(u'Пользователь'))
    kind = models.SmallIntegerField(_(u'Тип'), choices=ROUTE_KIND, default=1)
    country = models.ForeignKey(
        'geo.Country', verbose_name=_(u'Страна(откуда)'))
    region = ChainedForeignKey(
        'geo.Region', verbose_name=_(u'Регион(откуда)'),
        chained_field='country', chained_model_field='country',
        auto_choose=True, null=True, blank=True)
    country_to = models.ForeignKey(
        'geo.Country', related_name='route_set_to',
        verbose_name=_(u'Страна(куда)'), null=True, blank=True)
    region_to = ChainedForeignKey(
        'geo.Region', related_name='route_set_to',
        verbose_name=_(u'Регион(куда)'), null=True, blank=True,
        chained_field='country_to', chained_model_field='country',
        auto_choose=True)
    only_region = models.BooleanField(
        _(u'Только заказы внутри региона'), default=False)
    is_return = models.BooleanField(_(u'И обратно'), default=False)
    is_weight = models.BooleanField(
        _(u'Уведомлять только о грузах указанного веса'), default=False)
    weight_from = models.IntegerField(_(u'Вес от'), null=True, blank=True,
                                      help_text=_(u'кг'))
    weight_to = models.IntegerField(_(u'Вес до'), null=True, blank=True,
                                    help_text=_(u'кг'))
    empty_weight = models.BooleanField(
        _(u'Уведомлять если вес не указан'), default=False)
    only_category = models.BooleanField(
        _(u'Получать уведомления о заказах только следующих категорий'),
        default=False)
    categories = models.ManyToManyField(
        'order.OrderCategory', verbose_name=_(u'Категории'),
        null=True, blank=True)
    is_sms = models.BooleanField(
        _(u'Маршрут для уведомлений по sms'), default=False)
    default = models.BooleanField(
        _(u'Маршрут по умолчанию'), default=False)

    class Meta:
        verbose_name = _(u'Путь')
        verbose_name_plural = _(u'Пути')

    def __unicode__(self):
        if self.kind == 1:
            return u'%s' % self.region
        else:
            fields = ['country', 'region', 'country_to', 'region_to']
            return u'%s -- %s' % (self.region, self.region_to)

    def is_need_notification(self, order):
        """Необходимо ли оповещать об этом заказе?"""
        filters = ['region', 'route', 'weight', 'category']
        for filter in filters:
            if not getattr(self, '_is_need_notification_%s' % filter)(order):
                return False
        return True

    def _is_need_notification_region(self, order):
        """Проходит по региону?"""
        if self.kind != 1:
            return True
        if self.only_region:
            # только внутри региона
            if self.region == order.start_address.region and \
                    self.region == order.end_address.region:
                return True
        else:
            if self.default:
                if self.region == order.start_address.region or \
                        self.region == order.end_address.region:
                    return True
                if not self.region and self.country:
                    if self.country == order.start_address.country or \
                            self.country == order.end_address.country:
                        return True
            elif self.region == order.start_address.region:
                return True

        return False

    def _is_need_notification_route(self, order):
        """Проходит по маршруту?"""
        if self.kind != 2:
            return True
        if not self.region or not self.region_to:
            if not self.region and not self.region_to and self._check_country(order):
                return True
            elif self.region and not self.region_to:
                if self.region == order.start_address and self._check_country(order):
                    return True
            elif not self.region and self.region_to:
                if self.region_to == order.end_address and self._check_country(order):
                    return True
        else:
            if self.is_return:
                # и обратно
                if (self.region == order.start_address.region and
                        self.region_to == order.end_address.region) or\
                    (self.region == order.end_address.region and
                        self.region_to == order.start_address.region):
                    return True
            else:
                if self.region == order.start_address.region and\
                        self.region_to == order.end_address.region:
                    return True
        return False

    def _check_country(self, order):
        if self.country and self.country_to:
            if self.country == order.start_address.country and\
                    self.country_to == order.end_address.country:
                return True
        return False


    def _is_need_notification_weight(self, order):
        """Подходит по весу?"""
        if not self.is_weight:
            # Не фильтруем по весу
            return True
        if not order.cargo_weight:
            # Не задан вес в заказе
            return self.empty_weight

        if self.weight_from and self.weight_to:
            if self.weight_from >= order.cargo_weight <= self.weight_to:
                return True
            return False

        if self.weight_from:
            if self.weight_from >= order.cargo_weight:
                return True
            return False

        if self.weight_to:
            if order.cargo_weight <= self.weight_to:
                return True
            return False

        return False

    def _is_need_notification_category(self, order):
        """Подходит по категории?"""
        if not self.only_category:
            return True
        if self.categories.filter(id=order.category_id).exists():
            return True

        return False


class TmpUsers(models.Model):
    """Временная модель. В ней храним юзера, который ещё ни разу не логинился"""
    user = models.OneToOneField(User)

