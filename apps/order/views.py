#coding: utf-8
import json
import urllib
import settings
import datetime
from django.core.mail.message import EmailMessage
from django.contrib.auth.decorators import login_required
from django.template import loader
from django.template.context import Context
from django.contrib import messages
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404, render_to_response, RequestContext
from django.views.generic import FormView, TemplateView, ListView, View, CreateView, DeleteView, UpdateView
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Q
from django.template.loader import render_to_string
from django.template.defaultfilters import striptags
from django.forms.models import inlineformset_factory
from django.core.paginator import Paginator, EmptyPage
from dateutil.relativedelta import relativedelta
from django.contrib import auth
from django.contrib.auth.models import User

from company.models import Company
from company.views import LoginRequiredMixin
from order.models import Order, OrderCategory, OrderAddress, OrderName, OrderBet, Profile, Route, TmpUsers
from order.forms import OrderForm, OrderListFilterForm, OrderNameForm, OrderBetForm, \
    ChangePasswordForm, AuthenticationForm2, RouteForm
from .utils import notification_new_order, JsonResponse, CarrierMixin, mail_email, SetCookieMixin, PostTwitter


class FormErrorMixin(object):
    def form_invalid(self, form):
        return JsonResponse({'valid': False, 'errors': form.errors})


class EmailMixin(object):
    email_template = ''

    def send_letter(self, theme, order, mail, email_template=None):
        email_template = self.email_template if not email_template else email_template

        t = loader.get_template(email_template)
        context = {'order': order, 'host': 'dostmarket.ru'}
        context = Context(context)
        html = (t.render(context)).encode('utf-8')
        msg = EmailMessage(theme, html, settings.EMAIL_HOST_USER, mail, headers={})
        msg.content_subtype = "html"
        msg.send(fail_silently=True)


class OrderCategoryList(ListView):
    template_name = 'order/order_category_list.html'
    model = OrderCategory
    context_object_name = 'categories'


class OrderFormView(PostTwitter, CarrierMixin, FormErrorMixin, EmailMixin, FormView):
    model = Order
    template_name = 'order/order_form.html'
    form_class = OrderForm
    success_url = reverse_lazy('order:order_category_list')
    email_template = 'order/inclusion/email_order_add.html'

    def get_initial(self):
        init = super(OrderFormView, self).get_initial()
        user = self.request.user

        if user.is_authenticated():
            phone = ''
            try:
                phone = user.profile.phone
            except Profile.DoesNotExist:
                pass
            init.update({
                'username': user.first_name or user.username,
                'phone': phone,
                'email': user.email
                })
        return init

    def get_context_data(self, **kwargs):
        ctx = super(OrderFormView, self).get_context_data(**kwargs)
        ctx['order_category'] = self.category
        ctx['form_url'] = reverse_lazy('order:order_form', kwargs={'category_pk': self.kwargs.get('category_pk')})
        return ctx

    def get_form_kwargs(self):
        kwargs = super(OrderFormView, self).get_form_kwargs()
        kwargs['category'] = self.category = get_object_or_404(
            OrderCategory, pk=self.kwargs.get('category_pk'))
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        data = {}
        is_edit = 'edit' in self.request.get_full_path()
        cd = form.cleaned_data
        obj = form.save(commit=False)
        category = get_object_or_404(
            OrderCategory, pk=self.kwargs.get('category_pk'))
        obj.category = category
        obj.save()

        start_address, created = OrderAddress.objects.get_or_create(
            country=cd['country_from'], city=cd['city_from'], order=obj, type=1, address=cd['address_from'],
            long_position=cd['long_position_from'], lat_position=cd['lat_position_from'],
            region=cd['region_from'])
        end_address, created = OrderAddress.objects.get_or_create(
            country=cd['country_to'], city=cd['city_to'], order=obj, type=2, address=cd['address_to'],
            long_position=cd['long_position_to'], lat_position=cd['lat_position_to'],
            region=cd['region_to'])
        obj.start_address = start_address
        obj.end_address = end_address
        obj.save()
        if not is_edit:
            mail_email(
                u'Новый заказ на %s на DostMarket.ru' % obj.id,
                'order/inclusion/email_new_order_admin.html',
                {'order': obj, 'host': settings.HOST},
                settings.DEFAULT_FROM_EMAIL,
                [settings.A_EMAIL, ]
            )
            company = Company.objects.filter(send_mail=True)
            notification_new_order(obj, company)
        if obj.user.email and not is_edit:
            mail_email(
                u'Ваш заказ номер %s на DostMarket.ru' % obj.id,
                self.email_template,
                {'order': obj, 'host': settings.HOST},
                settings.DEFAULT_FROM_EMAIL,
                [obj.user.email,]
            )
        # post_twitter status
        self.post_twitter(obj)

        data['valid'] = True
        data['redirect_url'] = reverse('order:order_detail', kwargs={'pk': obj.pk})
        if not is_edit:
            data['redirect_url'] = reverse(
                'order:add_success', kwargs={'category_pk': self.kwargs.get('category_pk')}
            ) + '?next=%s' % data['redirect_url']
        return JsonResponse(data)


class SuccessOrder(TemplateView):
    template_name = 'order/add_success.html'


class OrderFormUpdateView(LoginRequiredMixin, OrderFormView):
    order = None
    success_url = reverse_lazy('order:order_list')

    def dispatch(self, request, *args, **kwargs):
        self.order = get_object_or_404(Order, pk=kwargs['pk'])
        kwargs['category_pk'] = self.order.category.pk
        if self.order.user != request.user:
            messages.error(request, _(u'Вы не можете редактировать этот заказ'))
            return HttpResponseRedirect(
                reverse_lazy('order:order_detail',
                             kwargs={'pk': self.order.pk}))
        return super(OrderFormUpdateView, self).dispatch(
            request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(OrderFormUpdateView, self).get_context_data(**kwargs)
        ctx['order'] = self.order
        return ctx

    def get_success_url(self):
        self.success_url = reverse_lazy('order:order_detail', kwargs={'pk': self.order.pk})
        return self.success_url
    #
    # def get_context_data(self, **kwargs):
    #     ctx = super(OrderFormUpdateView, self).get_context_data(**kwargs)
    #     ctx['form_url'] = reverse_lazy('order:order_update_form', kwargs={'pk': self.order.pk})
    #     return ctx

    def get_form_kwargs(self):
        kwargs = super(OrderFormUpdateView, self).get_form_kwargs()
        kwargs['instance'] = self.order
        kwargs['edit'] = True
        #TODO добавить в модель поле "гибкое время"
        return kwargs

    def send_add_order_mail(self, obj, form):
        pass

    def send_reg_email(self, obj, form):
        pass

    def success_message(self, obj):
        messages.success(
            self.request,
            _(u'Ваша заявка №%(num)s-"%(name)s" изменена.') % dict(
                num=obj.pk,
                name=obj.name)
        )


class OrderListView(SetCookieMixin, ListView):
    model = Order
    template_name = 'order/order_list.html'
    context_object_name = "orders_list"
    form_class = OrderListFilterForm

    def get_queryset(self):
        filters = self.request.GET.copy()
        user = self.request.user
        # if not filters and user.is_authenticated() and user.is_carrier():
        #     # стартовое фильтрование заказов в зависимости от указаных региональных данных
        #     fields = ['country', 'region']
        #     if user.is_courier():
        #         for field in fields:
        #             attr = getattr(user.company, field)
        #             filters['%s_from' % field] = attr.id if attr else ''
        pagination = filters.get('pangination', 20)
        if not filters:
            self.queryset = self.model.objects.filter(is_hide=False).order_by('-created')
        else:
            self.queryset = self.filter(filters)
        # Только заказы, в которых идут торги
        # self.queryset = self.queryset.filter(is_hide=False, status=0)
        self.queryset = self.queryset.filter(is_hide=False)
        try:
            page = int(self.request.GET.get('page', 1))
            self.queryset = Paginator(self.queryset, pagination).page(page)
        except ValueError:
            self.queryset = Paginator(self.queryset, pagination).page(1)
        except EmptyPage:
            pass

        return self.queryset

    def get_context_data(self, **kwargs):
        ctx = super(OrderListView, self).get_context_data(**kwargs)
        ctx['order_count'] = len(self.queryset)
        # initial = {}
        # if self.request.user.is_authenticated():
        #     try:
        #         profile = self.request.user.company
        #         categories = profile.categories
        #         initial = {
        #             'country_from': profile.country,
        #             'region_from': profile.region,
        #             'category': categories.all().values_list('id', flat=True),
        #             }
        #     except Company.DoesNotExist:
        #         pass
        ctx['form'] = self.form_class(self.request.GET)
        adjacent_pages = 6
        num_pages = self.queryset.paginator.num_pages
        page = int(self.request.GET.get('page', 1))
        startPage = max(page - adjacent_pages, 1)
        if startPage <= 3:
            startPage = 1
        endPage = page + adjacent_pages + 1
        if endPage >= num_pages - 1:
            endPage = num_pages + 1
        ctx['num_pages'] = [n for n in range(startPage, endPage) \
                            if n > 0 and n <= num_pages]
        return ctx

    def filter(self, filters):
        q = Q()
        func_filters = ['addresses', 'distance', 'weight', 'date', 'category']
        for name in func_filters:
            try:
                q = getattr(self, '_filter_%s' % name)(filters, q)
            except ValueError:
                pass
        queryset = self.model.objects
        return queryset.filter(q).order_by('-created')


    def _filter_addresses(self, filters, q):
        _q = Q()
        qs_kwargs = lambda country, region: {
            'start_address__city__region__id': int(filters.get(region)),
            'start_address__country__id': int(filters.get(country)),
        }
        qe_kwargs = lambda country, region: {
            'end_address__city__region__id': int(filters.get(region)),
            'end_address__country__id': int(filters.get(country)),
        }
        if filters.get('country_to'):
            _q = _q & Q(end_address__country__id=int(filters.get('country_to')))

        if filters.get('country_from'):
            _q = _q & Q(start_address__country__id=int(filters.get('country_from')))

        if filters.get('region_from'):
            _q = _q & Q(**qs_kwargs('country_from', 'region_from'))

        if filters.get('region_to'):
            _q = _q & Q(**qe_kwargs('country_to', 'region_to'))
        #
        # if filters.get('back') and filters.get('region_to'):
        #     _q = _q | Q(**qe_kwargs('country_to', 'region_to'))
        return _q & q

    def _filter_distance(self, filters, q):
        if filters.get('distance_from'):
            q = q & Q(distance__gte=filters.get('distance_from'))
        if filters.get('distance_to'):
            q = q & Q(distance__lte=filters.get('distance_to'))
        return q

    def _filter_weight(self, filters, q):
        _q = Q()
        if filters.get('weight_from'):
            _q = _q & Q(cargo_weight__gte=filters.get('weight_from'))
        if filters.get('weight_to'):
            _q = _q & Q(cargo_weight__lte=filters.get('weight_to'))
        if filters.get('weight_unknown'):
            _q = _q | Q(cargo_weight__isnull=True)
        return _q & q

    def _filter_date(self, filters, q):
        if filters.get('date_from'):
            date = timezone.datetime.strptime(
                filters.get('date_from'), '%d.%m.%Y')
            q = q & Q(date__gte=date)
        if filters.get('date_to'):
            date = timezone.datetime.strptime(filters.get('date_to'), '%d.%m.%Y')
            q = q & Q(date__lte=date)
        return q

    def _filter_category(self, filters, q):
        if filters.getlist('category') and filters.getlist('category')[0] != '':
            q = q & Q(category__in=filters.getlist('category'))
        return q


class OrderDetailView(SetCookieMixin, TemplateView):
    template_name = 'order/order_detail.html'

    def get(self, request, *args, **kwargs):
        order = self.get_order()
        if not order:
            return render_to_response('order/not_found.html', dict(),
                                      context_instance=RequestContext(request))

        view_orders = request.session.get('view_orders') or list()
        if order.pk not in view_orders:
            view_orders.append(order.pk)
            request.session['view_orders'] = view_orders
            order.views += 1
            order.views_today += 1
            order.save()
        if self.request.user == order.user:
            order.bets.all().update(is_read=True)
        # if order.status == 0 and order.end_trading <= timezone.now():
        #     order.status = 4
        #     order.save()
        #     notification.order_expiried(order)
        return super(OrderDetailView, self).get(request, *args, **kwargs)

    def get_order(self):
        try:
            pk = self.kwargs.get('pk')
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        ctx = super(OrderDetailView, self).get_context_data(**kwargs)
        ctx['order'] = self.get_order()
        # ctx['now'] = timezone.now()
        ctx['is_can_bet'], ctx['bet_error_message'], ctx['bet_error_type'] =\
            ctx['order'].is_user_can_bet(self.request.user)
        bets = ctx['order'].bets.all()
        ctx['bets'] = bets.order_by('price')
        return ctx


class OrderBetAddView(EmailMixin, FormView):
    form_class = OrderBetForm
    template_name = 'order/inclusion/order_bet_form.html'
    email_template = 'order/inclusion/email_new_bet.html'

    def get_order(self):
        return get_object_or_404(Order, pk=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        kwargs['order'] = self.get_order()
        kwargs['now'] = timezone.now()
        return kwargs

    def get_form_kwargs(self):
        kwargs = super(OrderBetAddView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['order'] = self.get_order()
        return kwargs

    def form_valid(self, form):
        bet = form.save(commit=False)
        bet.order.bets.filter(user=bet.user, status=0).update(
            status=2, reject_kind=11, status_time=timezone.now())
        bet.save()
        self.send_letter(
            u'На ваш заказ номер %s поступило новое предложение по цене.' % bet.order.id,
            bet.order, [bet.order.user.email, ])
        data = dict()
        data['valid'] = True
        data['reload'] = True
        data['text'] = render_to_string(
            'order/alert_text/succes_add_bet_message.html', {'order': bet.order})
        data['title'] = u'Ваша ставка успешно размещена'
        return HttpResponse(json.dumps(data), content_type="application/json")


class OrderCloseView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs.get('pk'))
        self.data = dict()
        self.data['title'] = u'Статус заказа'
        status = kwargs['status']
        close_choices = {
            '2': self.completed,
            '3': self.not_fulfilled,
        }
        if not order.is_can_close():
            self.data['text'] = u'Этот заказ нельзя закрыть'
        else:
            if not close_choices[status](order):
                self.data['text'] = u'У вас нет прав для закрытия заказа'
        if request.is_ajax():
            return JsonResponse(self.data)
        else:
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/'))

    def completed(self, order):
        """Заказ выполнен"""
        if self.request.user != order.user and\
                self.request.user != order.get_carrier():
            return False
        order.status = 2
        order.save()
        perfomer_url = order.get_win_bet().user.company.get_absolute_url()
        self.data['status'] = u'Завершен'
        self.data['text'] = u"""
            Заказ выполнен. <br><br>
            Теперь вы можете оставить комментарий на странице
            <a class='linn' href='%s'>исполнителя</a>""" % perfomer_url
        return True

    def not_fulfilled(self, order):
        """Заказ не выполнен"""
        if self.request.user != order.user:
            return False
        order.status = 3
        order.save()
        self.data['status'] = u'Отменен'
        self.data['text'] = u'Заказ не выполнен'
        # messages.success(self.request, _(u'Заказ не выполнен'))
        return True


class OrderBetSetStatusView(EmailMixin, View):
    email_template = 'order/inclusion/email_rejection_bet_client.html'
    email_template_carrier = 'order/inclusion/email_rejection_bet_carrier.html'

    def get(self, request, *args, **kwargs):
        bet = get_object_or_404(OrderBet, pk=kwargs['pk'])
        set_choices = {
            '1': self.take_bet,
            '2': self.reject_bet
        }
        data = dict()
        if not bet.is_change_status():
            data['text'] = u'Над этой ставкой больше нельзя предпринимать действия'
        else:
            if not set_choices[kwargs['status']](bet):
                data['text'] = u'У вас нет прав на это действие'
        data = dict()
        data['title'] = u'Ошибка'
        data['valid'] = False if data.get('text') else True
        status = kwargs['status']
        if data['valid'] and status == '1':
            data['title'] = u'Ставка принята'
            data['text'] = render_to_string(
                'order/alert_text/accept_bet.html', {'bet': bet})
        if data['valid'] and status == '2':
            if self.request.user == bet.user:
                data['title'] = u'Ваша ставка отменена'
                data['text'] = render_to_string(
                    'order/alert_text/reject_rate_bet_ownet.html', {'bet': bet})
            else:
                data['title'] = u'Ставка отклонена'
                data['text'] = render_to_string(
                    'order/alert_text/reject_rate_bet_user.html', {'bet': bet})
        data['status'] = data['title']
        return JsonResponse(data)

    def take_bet(self, bet):
        if self.request.user != bet.order.user:
            return False
        bet.status = 1
        bet.status_time = timezone.now()
        bet.save()
        # Статус заказа = Подтверждение перевозщиком
        mail_email(
            u'Вы выбрали исполнителя на ваш заказ',
            'order/inclusion/email_select_currier.html',
            {'order': bet.order, 'host': 'dostmarket.ru'},
            settings.DEFAULT_FROM_EMAIL,
            [self.request.user.email,]
        )
        url = reverse('order:order_detail', kwargs={'pk': bet.order.pk})
        mail_email(
            u'ПРЕДЛОЖЕНИЕ ПО ЗАКАЗУ номер %s принято пользователем' % bet.order.pk,
            '<a href="http://www.dostmarket.ru%s">%s</a>' % (url, bet.order.pk),
            {},
            settings.DEFAULT_FROM_EMAIL,
            [settings.A_EMAIL,]
        )
        bet_user_email = bet.user.email if bet.user.email else bet.user.username
        mail_email(
            u'Ваше предложение по цене выбрал заказчик на DostMarket.ru',
            'order/inclusion/email_win_bet.html',
            {'order': bet.order, 'host': 'dostmarket.ru'},
            settings.DEFAULT_FROM_EMAIL,
            [bet_user_email, ]
        )
        bet.order.status = 5
        bet.order.confirmation_date = timezone.now() + relativedelta(minute=5)
        bet.order.save()
        return True

    def reject_bet(self, bet):
        if self.request.user != bet.user and self.request.user != bet.order.user:
            return False
        # Если юзер вец заказа
        if self.request.user == bet.order.user:
            reject_id = self.request.GET.get('reject')
            if reject_id and reject_id in \
                    [k for k, v in bet.order.get_bet_reject_customer_choices()]:
                bet.reject_kind = reject_id
            else:
                # Без указания причины
                bet.reject_kind = 0
        # Если юзер владелец ставки
        elif self.request.user == bet.user:
            bet.reject_kind = 12
        if bet.reject_kind == 12:
            pass
            # self.send_letter(
            #     u'Отмена ставки к Вашему заказу №%s' % bet.order.id,
            #     bet.order, [bet.order.user.email, ])
        else:
            self.send_letter(
                u' Заказчик отклонил Ваше предложение к заказу №%s' % bet.order.id,
                bet.order, [bet.user.email, ], self.email_template_carrier)
        bet.status = 2
        bet.status_time = timezone.now()
        bet.save()
        # messages.success(self.request, _(u'Ставка отклонена, торги '
        #                                  u'продолжаються'))
        return True



        # if not bet.is_change_status():
        #     data['text'] = u'Над этой ставкой больше нельзя предпринимать действия'
        # else:
        #     if not set_choices[kwargs['status']](bet):
        #         data['text'] = u'У вас нет прав на это действие'
        # data['title'] = u'Ошибка'
        # data['valid'] = False if data.get('error') else True
        # if data['valid'] and kwargs['status'] == '1':
        #     data['performer_name'] = bet.user.username
        #     data['text'] = render_to_string(
        #         'order/inclusion/succes_add_bet_message.html', {'order': bet.order})
        #     data['title'] = u'Ваша ставка успешно размещена'


class OrderBetConfirmationView(LoginRequiredMixin, EmailMixin, View):
    order = None
    email_template = 'order/inclusion/email_rejection_bet_client.html'
    email_template_carrier = 'order/inclusion/email_rejection_bet_carrier.html'

    def dispatch(self, request, *args, **kwargs):
        self.order = get_object_or_404(Order, pk=kwargs['pk'])
        return super(OrderBetConfirmationView, self).dispatch(
            request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # Когда статус заказа Подтверждение перевозщиком
        result = dict()
        if self.order.status == 5:
            result = getattr(self, '_%s' % kwargs['action'])()
        if 'text' in result:
            result['valid'] = True

        result.setdefault('text', u'Ошибка доступа')
        return JsonResponse(result)

    def _yes(self):
        data = dict()
        data['title'] = u'Подтверждение заказа перевозчиком'
        """Подтверждение заказа перевозчиком"""
        if self.request.user != self.order.get_win_bet().user:
            return
        data['text'] = """
            Вы подтвердили, что беретесь за выполнение данного заказа. <br>
            Теперь вам доступны контактные данные заказчика на странице <br> заказа,
            а так же продублированы в e-mail.
            """
        data['status'] = u'Ставка принята'
        self.order.performer = self.request.user
        self.order.status = 1
        self.order.is_paid = True
        self.order.confirmation_date = None
        self.order.save()
        data['status'] = u'Ставка принята'
        return data

    def _cancel(self):
        """Отклонение ставки при подтверждении перевозчиком или заказчиком,
        пока перевозщик не дал ответ"""
        data = dict()
        data['title'] = u'Отклонение ставки'
        if self.request.user != self.order.user and \
                self.request.user != self.order.get_win_bet().user:
            return
        # заявка отклонена
        data['text'] = u'Заявка отклонена'
        bet = self.order.get_win_bet()
        bet.status = 2
        bet.reject_kind = 0 if self.request.user == self.order.user else 12
        bet.status_time = timezone.now()
        bet.save()
        # торги продолжаються
        if bet.reject_kind == 12:
            self.send_letter(
                u'Отказ от исполнения заказа №%s' % bet.order.id,
                bet.order, [bet.order.user.email, ])
        else:
            self.send_letter(
                u' Заказчик отклонил Ваше предложение к заказу №%s' % bet.order.id,
                bet.order, [bet.user.email, ], self.email_template_carrier)
        self.order.confirmation_date = None
        self.order.status = 0
        self.order.auto_selected = False
        self.order.save()
        data['status'] = u'Отклонена перевозчиком '
        # messages.success(self.request, _(u'Ставка отклонена'))
        return data


class OrderCancelView(LoginRequiredMixin, EmailMixin,  View):
    """
        Отмена заказа или ставки
    """
    email_template = 'order/inclusion/email_rejection_bet_client.html'

    def get(self, request, *args, **kwargs):
        objects = kwargs.get('match').split('/')
        data = dict()
        data['title'] = 'Отмена'
        redirect = False
        if not request.user.is_carrier():
            for order_id in objects:
                order = get_object_or_404(Order, pk=int(order_id))
                if not order.is_can_cancel():
                    data['text'] = u'Заказ №%s нельзя отменить' % order.id
                elif order.user == request.user:
                    # Статус = отменен
                    order.status = 4
                    for bet in order.bets.all():
                        if bet.reject_kind not in [12, 0]:
                            self.send_letter(
                                u' Заказчик отклонил Ваше предложение к заказу №%s' % order.id,
                                order, [bet.user.email, ], 'order/inclusion/email_rejection_bet_carrier.html')
                    order.save()
                    data['text'] = u'Заказ №%s отменен' % order.id
                    redirect = True
                else:
                    data['text'] = u'У вас нет прав для отмены заказа №%s' % order.id
        else:
            for bet_id in objects:
                bet = get_object_or_404(OrderBet, pk=int(bet_id))
                if not bet.order.status != 1:
                    data['text'] = u'Ставку №%s нельзя отменить' % bet.id
                bet.reject_kind = 12
                # self.send_letter(
                #     u'Отмена ставки к Вашему заказу №%s' % bet.order.id,
                #     bet.order, [bet.order.user.email, ])
                bet.status = 2
                bet.order.confirmation_date = None
                bet.status_time = timezone.now()
                if bet == bet.order.get_win_bet():
                    bet.order.status = 0
                    bet.order.save()
                bet.save()
                data['text'] = u'Ставка №%s отменена' % bet.id
                redirect = True
        if redirect:
            data['redirect_url'] = '/'
        return JsonResponse(data)
        # return HttpResponseRedirect(reverse_lazy('competition_list'))


class CityAutocompleteView(View):
    #TODO передалать на  js
    def get(self, request, *args, **kwargs):
        country = int(request.GET.get('country', 1))
        if country:
            if country == 1:
                country = 'ru'
            elif country == 2:
                country = 'ua'
            elif country == 3:
                country = 'by'
            elif country == 4:
                country = 'kz'
        params = {
            "input": unicode(request.GET.get('input')).encode('utf-8'),
            "key": request.GET.get('key'),
            'components': 'country:%s' % country,
            "sensor": 'true',
            "types": '(cities)'
        }
        url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?'+urllib.urlencode(params)+'&language=ru'
        data = json.dumps(json.load(urllib.urlopen(url)))
        return HttpResponse(data, content_type="application/json")


class ListNotificationRoute(LoginRequiredMixin, ListView):
    model = Route
    template_name = "order/inclusion/route_list.html"
    context_object_name = "routes"

    def get_queryset(self):
        return self.request.user.routes.all()


class AddNotificationRoute(LoginRequiredMixin, CreateView):
    model = Route
    form_class = RouteForm
    template_name = "order/inclusion/route_change_form.html"
    success_url = reverse_lazy('order:route_list')

    def get_context_data(self, **kwargs):
        ctx = super(AddNotificationRoute, self).get_context_data(**kwargs)
        ctx['route_form_url'] = reverse_lazy('order:add_route')
        ctx['route_form'] = ctx.get('form')
        return ctx

    def get_form_kwargs(self):
        kwargs = super(AddNotificationRoute, self).get_form_kwargs()
        kwargs.update(user=self.request.user)
        return kwargs

    def form_valid(self, form):
        form.save()
        company = self.request.user.company
        company.send_mail = True
        company.save()
        data = dict()
        data['valid'] = True
        data['success_url'] = str(self.success_url)
        return JsonResponse(data)


class EditNotificationRoute(LoginRequiredMixin, UpdateView):
    model = Route
    form_class = RouteForm
    template_name = "order/inclusion/route_change_form.html"
    success_url = reverse_lazy('order:route_list')


    def get_context_data(self, **kwargs):
        ctx = super(EditNotificationRoute, self).get_context_data(**kwargs)
        ctx['route_form_url'] = reverse_lazy('order:edit_route', kwargs={'pk': self.object.pk})
        ctx['route_form'] = ctx.get('form')
        return ctx

    def form_valid(self, form):
        form.save()
        data = dict()
        data['valid'] = True
        data['success_url'] = str(self.success_url)
        return HttpResponse(json.dumps(data), content_type="application/json")


class DeleteNotificationRoute(LoginRequiredMixin, DeleteView):
    model = Route
    success_url = reverse_lazy('order:route_list')

    def get(self, *args, **kwargs):  # удаляем без "подтверждения"
        return self.post(*args, **kwargs)


class SignIn(FormView):
    form_class = AuthenticationForm2

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        ctx = dict()
        ctx['errors'] = form.errors
        ctx['valid'] = False
        return self.render_to_json_response(ctx)

    def form_valid(self, form):
        auth.login(self.request, form.get_user())
        ctx = {}
        ctx['valid'] = True
        return self.render_to_json_response(ctx)


class Unsubscribe(TemplateView):
    template_name = 'order/unsubscribe.html'

    def get(self, request, *args, **kwargs):
        uuid = kwargs.get('uuid')
        company = get_object_or_404(Company, uuid=uuid)
        company.send_mail = False
        company.save()
        company.user.routes.all().delete()
        return super(Unsubscribe, self).get(request, *args, **kwargs)


