# -*- encoding: utf-8 -*-
from django.contrib import admin
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _
from django.contrib import auth, messages
from django.contrib.auth.models import User
from .forms import SendMessages
from django.db.models import Q
from django.core import mail
from .models import OrderCategory, OrderService, Order, OrderAddress, OrderName, OrderBet
from apps.company.models import Company
from django.template.defaultfilters import striptags
import mailer
import settings
from email_mailer.models import Email, NoEmail
from apps.geo.models import Country, Region, City

class OrderNameInline(admin.TabularInline):
    model = OrderName
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'category', 'created', 'status', 'is_main', 'is_hide')
    search_fields = ('name', 'user', 'body')
    list_filter = ('category',)
    list_editable = ('is_main',)
    # inlines = [OrderNameInline, ]
#

class OrderCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    # list_editable = ('ordering', 'block_width')
    search_fields = ('name',)
    filter_horizontal = ('services',)


def SEND_MESSAGES(request, *args, **kwargs):
    template_name = 'admin/custom/send_messages.html'
    ctx = dict()
    if settings.DEBUG:
        TEST_EMAIL = ['stupak.alexn@gmail.com']
    else:
        TEST_EMAIL = [settings.A_EMAIL]

    ctx['title'] = u'Отправка сообщений'
    ctx['form'] = form = SendMessages(request.POST or None)
    if form.is_valid():
        cd = form.cleaned_data
        users_email = get_email_list(cd['user_type'], cd)
        if not cd['is_test']:
            for email in users_email:
                if not NoEmail.objects.filter(email__icontains=email).exists():
                    mailer.send_html_mail(
                        cd['subject'], striptags(cd['body']), cd['body'], settings.DEFAULT_FROM_EMAIL, [email])
            messages.success(request, _(u'Сообщения добавлены в очередь'))
            return redirect('/admin/')
        else:
            mailer.send_html_mail(
                cd['subject'], striptags(cd['body']), cd['body'], settings.DEFAULT_FROM_EMAIL, TEST_EMAIL)
            messages.success(
                request, _(u'Тестовое сообщение отправлено'))
            post = request.POST.copy()
            post['is_test'] = False
            ctx['form'] = SendMessages(post or None)
            return render(request, template_name, ctx)
    if form.errors:
        post = request.POST.copy()
        for attr in ['region', 'city']:
            if post.get(attr):
                del post[attr]
        ctx['form'] = SendMessages(post or None)
        messages.error(request, _(u'Исправьте ошибки ниже'))
    return render(request, template_name, ctx)


def get_email_list(user_type, filters):
    """
    USER_TYPE = (
        (1, u'Пользователи'),
        (2, u'Компании и курьеры'),
        (3, u'Все'),

        (4, u'Транспортная компания email'),
        (5, u'Курьерская служба email'),
        (6, u'Пользователь email')
    )
    """
    email_list = []
    if '3' in user_type:
        for user in User.objects.all():
            if user.email and user.email not in email_list:
                email_list.extend(user.email)
        email_list.extend(get_email_company(filters))
        not_register = Email.objects.filter(_filter_addresses(filters)).\
            values_list('email', flat=True)
        email_list.extend(not_register)
        return set(email_list)
    if '1' in user_type:
        for user in User.objects.filter(company__isnull=True):
            if user.email:
                email_list.extend(user.email)
    if '2' in user_type:
        email_list.extend(get_email_company(filters))
    email = Email.objects.filter(
        type__in=user_type).filter(_filter_addresses(filters)).values_list('email', flat=True)
    email_list.extend(email)
    return set(email_list)


def _filter_addresses(filters):
    _q = Q()
    if filters.get('country'):
        _q = _q & Q(country=filters.get('country'))

    if filters.get('region'):
        _q = _q & Q(region=filters.get('region'))

    if filters.get('city'):
        _q = _q & Q(city=filters.get('city'))
    return _q


def get_email_company(filters):
    email_list = []
    for company in Company.objects.filter(active=True).filter(_filter_addresses(filters)):
        if company.email:
            email_list.append(company.email)
        try:
            if company.user and company.user.email:
                email_list.append(company.user.email)
        except User.DoesNotExist:
            pass
    return email_list

admin.site.register_view('send/email', view=SEND_MESSAGES)

admin.site.register(OrderCategory, OrderCategoryAdmin)
admin.site.register(OrderService)
admin.site.register(Order, OrderAdmin)
# admin.site.register(OrderAddress)
admin.site.register(OrderBet)