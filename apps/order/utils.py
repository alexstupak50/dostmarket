# -*- encoding: utf-8 -*-
import json
from django.utils import timezone
import datetime
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string
import random
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.defaultfilters import striptags
from django import forms
from django.utils.translation import ugettext_lazy as _
from math import sin, cos, radians, degrees, acos
from django.template.context import Context
from django.core.mail.message import EmailMessage
from django.template import loader, RequestContext
import settings
from django.contrib.auth.models import User
from geo.models import City
from apps.email_mailer.models import NoEmail


class RemoveEmptyLabelMixin(object):
    class Meta:
        remove_empty_label = None

    def __init__(self, *args, **kwargs):
        super(RemoveEmptyLabelMixin, self).__init__(*args, **kwargs)
        remove_empty_label = getattr(self.Meta, 'remove_empty_label', None)
        if remove_empty_label:
            for name, field in self.fields.iteritems():
                if name in remove_empty_label:
                    # иногда choices обьект итерации
                    field.choices = list(field.choices)
                    if field.choices[0][1] == u'---------':
                        field.choices = field.choices[1:]
                        field.widget.choices = field.choices


def notification_new_order(order, company):
    for item in company:
        try:
            user = item.user
        except User.DoesNotExist:
            continue
        if item.user:
            orders_send = []
            for route in user.routes.all():
                if route.is_need_notification(order):
                    orders_send.append(order)
            if orders_send:
                email = user.email if user.email else user.company.email
                if email:
                    mail_email(
                        _(u'На сайте DostMarket.ru опубликован новый заказ на доставку.'),
                        'order/inclusion/email_order_for_carrier.html',
                        {'order': order, 'host': settings.HOST, 'company': item},
                        settings.DEFAULT_FROM_EMAIL,
                        [email, ]
                    )


def mail_email(subject, template, context, from_email, recipient_list,
                   priority="medium", fail_silently=False, auth_user=None,
                   auth_password=None, headers={}):
    import mailer
    if template.endswith(('.txt', '.html')):
        html = render_to_string(template, context)
    else:
        html = template
    try:
        if not NoEmail.objects.filter(email__icontains=recipient_list[0]).exists():
            mailer.send_html_mail(subject, striptags(html), html, from_email, recipient_list)
    except:
        print u'трабл в емейле'
    return


def password_generate(num=8, only_digits=False):
    if only_digits:
        data = '1234567890'
    else:
        data = 'qwertyuiopasdfghjklzxcvbnm1234567890'
    password = ''
    for n in xrange(num):
        x = random.choice(data)
        if not x.isdigit() and random.randint(0, 1):
            x = x.title()
        password = '%s%s' % (password, x)
    return password


def get_object_or_none(klass, *args, **kwargs):
    try:
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        return None


def check_address(form, list_field):
    cd = form.cleaned_data
    try:
        for country_id, city in list_field:
            City.objects.get(name=cd[city], country=cd[country_id])
    except City.DoesNotExist:
        raise forms.ValidationError(
            _(u'Произошла ошибка, возможно вы указали не существующий адрес.404'))


def send_email(subject, e_mail, context, email_template, send=True):
        t = loader.get_template(email_template)
        context.update({'host': 'dostmarket.ru'})
        context = Context(context)
        html = (t.render(context)).encode('utf-8')
        msg = EmailMessage(subject, html, settings.EMAIL_HOST_USER, e_mail, headers={})
        msg.content_subtype = "html"
        if send:
            msg.send(fail_silently=True)
        else:
            return msg

def calc_dist(lat_a, long_a, lat_b, long_b):
    """
    Возвращает результат в км.
    """
    lat_a = radians(lat_a)
    lat_b = radians(lat_b)
    long_diff = radians(long_a - long_b)
    distance = (sin(lat_a) * sin(lat_b) +
                cos(lat_a) * cos(lat_b) * cos(long_diff))
    return round(degrees(acos(distance)) * 100, 2)


class JsonResponse(HttpResponse):
    """
        JSON response
    """

    def __init__(self, content, mimetype='application/json', status=None, content_type=None):
        super(JsonResponse, self).__init__(
            content=json.dumps(content),
            mimetype=mimetype,
            status=status,
            content_type=content_type,
        )


class CarrierMixin(object):

    def get(self, *args, **kwargs):
        if self.is_carrier():
            return redirect('/')
        return super(CarrierMixin, self).get(*args, **kwargs)

    def post(self, *args, **kwargs):
        if self.is_carrier():
            return redirect('/')
        return super(CarrierMixin, self).post(*args, **kwargs)

    def is_carrier(self):
        if self.request.user.is_authenticated() and self.request.user.is_carrier():
            return True


class SetCookieMixin(object):
    cookie_key = 'twitter_window'
    cookie_val = 'is_show'
    days_expire = 30

    def render_to_response(self, context, **response_kwargs):
        response = super(SetCookieMixin, self).render_to_response(context, **response_kwargs)
        # if self.days_expire is None:
        #     max_age = 365 * 24 * 60 * 60  #one year
        # else:
        #     max_age = self.days_expire * 24 * 60 * 60
        delta = datetime.timedelta(days=self.days_expire)
        if not self.request.COOKIES.get(self.cookie_key):
            response.set_cookie(self.cookie_key, self.cookie_val,
                                domain=".dostmarket.ru", expires=timezone.now()+delta)
        return response

    def get_context_data(self, **kwargs):
        ctx = super(SetCookieMixin, self).get_context_data(**kwargs)
        # ctx[self.cookie_key] = False if 'is_show' == self.request.COOKIES.get(self.cookie_key, True) else True
        ctx[self.cookie_key] = False
        return ctx


class PostTwitter(object):

    def post_twitter(self, obj):
        try:
            import twitter
            api = twitter.Api(consumer_key=settings.CONSUMER_KEY,
                              consumer_secret=settings.CONSUMER_SECRET,
                              access_token_key=settings.ACCESS_TOKEN,
                              access_token_secret=settings.ACCESS_TOKEN_SECRET)
            source = u'Новый заказ. {text}. Маршрут: {city_from} - {city_to}. {host}{link}'.format(
                text=obj.name.title(),
                host=settings.HOST,
                city_from=obj.start_address.city,
                city_to=obj.end_address.city,
                link=obj.get_absolute_url()
            )
            status = api.PostUpdate(source)
        except:
            #TODO прописать ошибки
            print u'twitter error'