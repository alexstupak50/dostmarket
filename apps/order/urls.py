# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from order.views import OrderCategoryList, OrderFormView, OrderListView, \
    OrderDetailView, OrderBetAddView, OrderBetSetStatusView,     OrderBetConfirmationView, OrderCancelView, \
    OrderCloseView, CityAutocompleteView, SignIn, ListNotificationRoute, AddNotificationRoute, EditNotificationRoute, \
    DeleteNotificationRoute, OrderFormUpdateView, SuccessOrder, Unsubscribe

urlpatterns = patterns('',
    url(r'^detail/(?P<pk>\d+)/$', OrderDetailView.as_view(), name='order_detail'),
    url(r'^list/category/$', OrderCategoryList.as_view(), name='order_category_list'),
    url(r'^form/(?P<category_pk>\d+)/$', OrderFormView.as_view(),
        name='order_form'),
    url(r'^form/(?P<pk>\d+)/edit/$', OrderFormUpdateView.as_view(),
        name='order_update_form'),
    url(r'^list/$', OrderListView.as_view(), name='order_list'),
    url(r'^(?P<pk>\d+)/bet/add/$', OrderBetAddView.as_view(),
        name='order_bet_add'),
    url(r'^bet/(?P<pk>\d+)/(?P<status>[1|2])/$', OrderBetSetStatusView.as_view(),
        name='order_bet_set_status'),
    url(r'^(?P<pk>\d+)/confirmation/(?P<action>yes|cancel)/$',
        OrderBetConfirmationView.as_view(), name='order_confirmation'),
    url(r'^cancel/(?P<match>.+)/$', OrderCancelView.as_view(),
        name='order_cancel'),
    url(r'^order/(?P<pk>\d+)/close/(?P<status>[2|3])/$', OrderCloseView.as_view(),
        name='order_close'),
    url(r'^city/autocomplete/$', CityAutocompleteView.as_view(), name='city_autocomplete'),
    url(r'^login/$', SignIn.as_view(), name='login'),
    url(r'^route/list/$', ListNotificationRoute.as_view(), name='route_list'),
    url(r'^route/add/$', AddNotificationRoute.as_view(), name='add_route'),
    url(r'^route/edit/(?P<pk>\d+)/$', EditNotificationRoute.as_view(), name='edit_route'),
    url(r'^route/delete/(?P<pk>\d+)/$', DeleteNotificationRoute.as_view(), name='delete_route'),

    url(r'^form/(?P<category_pk>\d+)/success/$', SuccessOrder.as_view(), name='add_success'),
    url(r'^unsubscribe/(?P<uuid>[0-9A-Za-z-_.//]+)/$', Unsubscribe.as_view(), name='unsubscribe'),
)