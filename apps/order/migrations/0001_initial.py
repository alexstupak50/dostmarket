# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Profile'
        db.create_table('order_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('order', ['Profile'])

        # Adding model 'OrderCategory'
        db.create_table('order_ordercategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('icon', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('icon_white', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('block_width', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('name_description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_example', self.gf('ckeditor.fields.RichTextField')(max_length=255, null=True, blank=True)),
            ('name_options', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_options_erase', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('body_description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('body_example', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('body_options', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('cargo_weight', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cargo_volume', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('loaders', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('can_be_distilled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('not_on_move', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('transportation_type', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('count_items', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('carriage_passengers', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('modalities', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('transportation_type_in_bet', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('order', ['OrderCategory'])

        # Adding unique constraint on 'OrderCategory', fields ['name']
        db.create_unique('order_ordercategory', ['name'])

        # Adding M2M table for field services on 'OrderCategory'
        m2m_table_name = db.shorten_name('order_ordercategory_services')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ordercategory', models.ForeignKey(orm['order.ordercategory'], null=False)),
            ('orderservice', models.ForeignKey(orm['order.orderservice'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ordercategory_id', 'orderservice_id'])

        # Adding model 'OrderService'
        db.create_table('order_orderservice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('icon', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal('order', ['OrderService'])

        # Adding model 'OrderAddress'
        db.create_table('order_orderaddress', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['order.Country'], null=True, blank=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['order.Region'], null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['order.City'], null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(related_name='addresses', to=orm['order.Order'])),
            ('long_position', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=16, decimal_places=8)),
            ('lat_position', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=16, decimal_places=8)),
        ))
        db.send_create_signal('order', ['OrderAddress'])

        # Adding model 'Order'
        db.create_table('order_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='orders', to=orm['auth.User'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='orders', to=orm['order.OrderCategory'])),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('cargo_weight', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('cargo_volume', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('loaders', self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True)),
            ('modalities', self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True)),
            ('can_be_distilled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('not_on_move', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('buy_and_transportation', self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('time', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('count_items', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('start_address', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='order_start', null=True, to=orm['order.OrderAddress'])),
            ('end_address', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='order_end', null=True, to=orm['order.OrderAddress'])),
            ('price_max', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('price_blitz', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('topicality', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('distance', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('views', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('views_today', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('status', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('performer', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='orders_performer', null=True, to=orm['auth.User'])),
            ('confirmation_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('city_from', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('city_to', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('country_from', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('country_to', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('timezone', self.gf('django.db.models.fields.CharField')(default='UTC', max_length=50)),
        ))
        db.send_create_signal('order', ['Order'])

        # Adding model 'Country'
        db.create_table('order_country', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('order', ['Country'])

        # Adding model 'Region'
        db.create_table('order_region', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='regions', to=orm['order.Country'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('order', ['Region'])

        # Adding unique constraint on 'Region', fields ['country', 'name']
        db.create_unique('order_region', ['country_id', 'name'])

        # Adding model 'City'
        db.create_table('order_city', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cities', null=True, to=orm['order.Region'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=9, decimal_places=6, blank=True)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=9, decimal_places=6, blank=True)),
        ))
        db.send_create_signal('order', ['City'])


    def backwards(self, orm):
        # Removing unique constraint on 'Region', fields ['country', 'name']
        db.delete_unique('order_region', ['country_id', 'name'])

        # Removing unique constraint on 'OrderCategory', fields ['name']
        db.delete_unique('order_ordercategory', ['name'])

        # Deleting model 'Profile'
        db.delete_table('order_profile')

        # Deleting model 'OrderCategory'
        db.delete_table('order_ordercategory')

        # Removing M2M table for field services on 'OrderCategory'
        db.delete_table(db.shorten_name('order_ordercategory_services'))

        # Deleting model 'OrderService'
        db.delete_table('order_orderservice')

        # Deleting model 'OrderAddress'
        db.delete_table('order_orderaddress')

        # Deleting model 'Order'
        db.delete_table('order_order')

        # Deleting model 'Country'
        db.delete_table('order_country')

        # Deleting model 'Region'
        db.delete_table('order_region')

        # Deleting model 'City'
        db.delete_table('order_city')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'order.city': {
            'Meta': {'object_name': 'City'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cities'", 'null': 'True', 'to': "orm['order.Region']"})
        },
        'order.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'order.order': {
            'Meta': {'object_name': 'Order'},
            'body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'buy_and_transportation': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'can_be_distilled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cargo_volume': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'cargo_weight': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': "orm['order.OrderCategory']"}),
            'city_from': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'city_to': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'confirmation_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'count_items': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'country_from': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'country_to': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'distance': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'end_address': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'order_end'", 'null': 'True', 'to': "orm['order.OrderAddress']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loaders': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'modalities': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'not_on_move': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'performer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'orders_performer'", 'null': 'True', 'to': "orm['auth.User']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'price_blitz': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'price_max': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'start_address': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'order_start'", 'null': 'True', 'to': "orm['order.OrderAddress']"}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'timezone': ('django.db.models.fields.CharField', [], {'default': "'UTC'", 'max_length': '50'}),
            'topicality': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': "orm['auth.User']"}),
            'views': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'views_today': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'order.orderaddress': {
            'Meta': {'object_name': 'OrderAddress'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['order.City']", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['order.Country']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat_position': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '16', 'decimal_places': '8'}),
            'long_position': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '16', 'decimal_places': '8'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'addresses'", 'to': "orm['order.Order']"}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['order.Region']", 'null': 'True', 'blank': 'True'})
        },
        'order.ordercategory': {
            'Meta': {'ordering': "('ordering',)", 'unique_together': "(('name',),)", 'object_name': 'OrderCategory'},
            'block_width': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'body_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'body_example': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'body_options': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'can_be_distilled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cargo_volume': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cargo_weight': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'carriage_passengers': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'count_items': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'icon_white': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loaders': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modalities': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'name_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_example': ('ckeditor.fields.RichTextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_options': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_options_erase': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'not_on_move': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'services': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['order.OrderService']", 'null': 'True', 'blank': 'True'}),
            'transportation_type': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'transportation_type_in_bet': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'order.orderservice': {
            'Meta': {'ordering': "('ordering',)", 'object_name': 'OrderService'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'order.profile': {
            'Meta': {'ordering': "['user']", 'object_name': 'Profile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'order.region': {
            'Meta': {'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regions'", 'to': "orm['order.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['order']