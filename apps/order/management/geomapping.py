# -*- coding: utf-8 -*-
import io
import tempfile
import logging
import zipfile
import requests
import errno
import shutil

# from decimal import Decimal

from django.conf import settings

from django_geoip.vendor.progressbar import ProgressBar, Percentage, Bar
from django_geoip import compat
from django_geoip.models import IpRange, City, Region, Country

from .iso3166_1 import ISO_CODES

# from core.models import LocCity, LocRegion, LocCountry
# from core.utils import get_coordinates


class GeoMapping(object):
    """Backend to download and update geography and ip addresses mapping.
    """

    def __init__(self, logger=None):
        self.files = {}
        self.logger = logger or logging.getLogger(name='geoip_update')

    def clear_database(self):
        """ Removes all geodata stored in database.
            Useful for development, never use on production.
        """
        self.logger.info('Removing obsolete geoip from database...')
        # LocCity.objects.all().delete()
        # LocRegion.objects.all().delete()
        # LocCountry.objects.all().delete()

    def download_files(self):
        self.files = self._download_extract_archive(settings.GEO_MAPPING_SOURCE_URL)
        return self.files

    def sync_database(self):
        self._process_country_file(io.open(self.files['countries'], encoding=settings.GEO_MAPPING_FILE_ENCODING))
        self._process_region_file(io.open(self.files['regions'], encoding=settings.GEO_MAPPING_FILE_ENCODING ))
        self._process_cities_file(io.open(self.files['cities'], encoding=settings.GEO_MAPPING_FILE_ENCODING ))
        self._bulk_create()
        self.clear_temp_dir()
        self.logger.info('Done')

    def _download_extract_archive(self, url):
        """ Returns dict with 2 extracted filenames """
        self.logger.info('Downloading zipfile from %s...' % settings.GEO_MAPPING_SOURCE_URL)
        self.temp_dir = tempfile.mkdtemp()
        archive = zipfile.ZipFile(self._download_url_to_string(url))
        self.logger.info('Extracting files...')
        file_cities = archive.extract(settings.GEO_MAPPING_CITIES_FILENAME, path=self.temp_dir)
        file_regions = archive.extract(settings.GEO_MAPPING_REGIONS_FILENAME, path=self.temp_dir)
        file_countries = archive.extract(settings.GEO_MAPPING_COUNTRIES_FILENAME, path=self.temp_dir)
        return {'cities': file_cities, 'regions': file_regions, 'countries': file_countries}

    def _download_url_to_string(self, url):
        if settings.DEBUG and hasattr(settings, 'GM_LOCAL_FILE_LOCATION'):
            return settings.GM_LOCAL_FILE_LOCATION
        r = requests.get(url)
        return compat.BytesIO(r.content)

    def _line_to_dict(self, file, field_names):
        """ Converts file line into dictonary """
        for line in file:
            delimiter = settings.GEO_MAPPING_FIELDS_DELIMITER
            yield self._extract_data_from_line(line, field_names, delimiter)

    def _extract_data_from_line(self, line, field_names=None, delimiter="\t"):
        return dict(zip(field_names, line.rstrip('\n').replace('\"', '').split(delimiter)))

    def _process_country_file(self, file):
        """ Iterate over countries info and extract useful data """
        self.countries = dict()
        allowed_countries = settings.GEO_MAPPING_ALLOWED_COUNTRIES
        for country_info in self._line_to_dict(file, field_names=settings.GEO_MAPPING_COUNTRY_FIELDS):
            if allowed_countries and country_info['title_en'].lower() not in allowed_countries:
                continue
            self.countries.update({int(country_info['country_id']): {'title_en': country_info['title_en'], 'title_ru': country_info['title_ru'], 'regions': dict()}})
        self.countries_set = self.countries.keys()

    def _process_region_file(self, file):
        """ Iterate over regions info and extract useful data """
        for region_info in self._line_to_dict(file, field_names=settings.GEO_MAPPING_REGION_FIELDS):
            null = [''] + settings.GEO_MAPPING_REGION_FIELDS
            # skip null
            if region_info['country_id'] in null:
                continue
            # skip
            if region_info['country_id'] and int(region_info['country_id']) not in self.countries_set:
                continue
            self.countries[int(region_info['country_id'])]['regions'].update({int(region_info['region_id']): {'title_en': region_info['title_en'], 'title_ru': region_info['title_ru'], 'cities': list()}})

    def _process_cities_file(self, file):
        """ Iterate over cities info and extract useful data """
        for city_info in self._line_to_dict(file, field_names=settings.GEO_MAPPING_CITIES_FIELDS):
            null = [''] + settings.GEO_MAPPING_CITIES_FIELDS
            # skip null
            if city_info['country_id'] in null or city_info['region_id'] in null:
                continue
            # skip
            if city_info['country_id'] and int(city_info['country_id']) not in self.countries_set:
                continue
            coord = get_coordinates(self.logger, self.countries[int(city_info['country_id'])]['title_ru'], city_info['title_ru'])
            self.countries[int(city_info['country_id'])]['regions'][int(city_info['region_id'])]['cities'].append(dict({'title_en': city_info['title_en'], 'title_ru': city_info['title_ru'], 'important': True if city_info['important'] == 't' else False}, **coord))

    def _bulk_create(self):
        """ build geo map tables with fresh data """
        for country_id, data in self.countries.iteritems():
            regions = data.pop('regions')
            country = LocCountry.objects.create(**data)
            self.logger.info('Created country entry %s. Creating regions:' % data['title_en'])
            for region_id, region_data in regions.iteritems():
                cities = region_data.pop('cities')
                region = LocRegion.objects.create(**dict({'country_id': country.id}, **region_data))
                self.logger.info('-- created region entry %s.\n--- Creating cities' % region_data['title_en'])
                additional = {'country_id': country.id, 'region_id': region.id}
                for index, city_data in enumerate(cities):
                    cities[index] = LocCity(**dict(city_data, **additional))
                LocCity.objects.bulk_create(cities)
                self.logger.info('--- done\n')

    def clear_temp_dir(self):
        try:
            shutil.rmtree(self.temp_dir)
        except OSError as exc:
            if exc.errno != errno.ENOENT:
                raise

    def set_default(self):
        try:
            LocCity.objects.get(default=True)
        except Exception, e:
            LocCity.objects.filter(title_ru__iexact=getattr(settings, 'GEO_MAPPING_DEFAULT_CITY')).update(default=True)
