#coding: utf-8
from django.template.context import Context
from django.core.mail.message import EmailMessage
from django.template import loader, RequestContext
import settings

from django.core.management.base import BaseCommand, CommandError
from apps.order.models import Order, OrderAddress, OrderName, OrderBet

class Command(BaseCommand):
    args = '<type>'

    def handle(self, *args, **options):
        try:
            id = int(args[0])
        except IndexError:
            id = 1000
        for order in Order.objects.all():
            kwargs = self.get_fields(Order, order)
            kwargs['id'] = id
            new_order = Order.objects.create(**kwargs)
            order.start_address = order.end_address = None
            order.save()
            address = OrderAddress.objects.filter(order=order)
            bets = OrderBet.objects.filter(order=order)
            name = OrderName.objects.filter(order=order)
            if name:
                new_order.name = name[0].name
                new_order.save()
            if address:
                for addres in address:
                    addres.order = new_order
                    addres.save()
            if bets:
                for bet in bets:
                    bet.order = new_order
                    bet.save()
            id += 1
            order.delete()
            print id


    def get_fields(self, model, instance):
        field_names = [f.name for f in model._meta.fields]
        for field_name in field_names:
            return {field_name: getattr(instance, field_name) for field_name in field_names}

