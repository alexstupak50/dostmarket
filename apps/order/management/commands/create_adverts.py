# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.utils import timezone
import sys
import traceback
import os
import logging
from optparse import make_option

from ..db_fakeload import FakeFill

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Filles the db with fake adverts'
    option_list = BaseCommand.option_list + (
        make_option(
            '-c', '--clear',
            action='store_true',
            default=False,
            help="Clear tables prior import"
        ),
        make_option(
            '-a', '--amount',
            action='store',
            type='int',
            dest='amount',
            default=100,
            help='Sets the number adverts created. Default: 100'),
    )

    def get_logger(self, verbosity):
        logger = logging.getLogger('import')
        logger.addHandler(logging.StreamHandler())
        VERBOSITY_MAPPING = {
            0: logging.CRITICAL, # no
            1: logging.INFO, # means normal output (default)
            2: logging.DEBUG, # means verbose output
            3: logging.DEBUG, # means very verbose output
        }
        logger.setLevel(VERBOSITY_MAPPING[int(verbosity)])
        return logger

    def handle(self, *args, **options):
        logger = self.get_logger(options['verbosity'])
        backend = FakeFill(logger=logger, amount=options['amount'])

        if options.get('clear'):
            backend.clear_database()

        backend.sync_database()
