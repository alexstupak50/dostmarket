# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from geo.models import Country, City, Region
from company.models import Company


class Command(BaseCommand):
    args = '<type>'

    def handle(self, *args, **options):
        """
        Находим регионы и страны для старых компаний

        """
        company = Company.objects.filter(country__isnull=True).exclude(city__isnull=True)
        for x in company:
            city = City.objects.filter(name=x.get_city())
            if city:
                city = city[0]
                if city.region:
                    x.region = city.region
                    x.country = city.country
                    x.save()
