#coding: utf-8
from django.template.context import Context
from django.core.mail.message import EmailMessage
from django.template import loader, RequestContext
import settings

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from apps.company.models import Company
from apps.order.views import EmailMixin

class Command(BaseCommand):
    args = '<type>'

    def handle(self, *args, **options):
        email_list = []
        if args[0] == 'company':
            for company in Company.objects.all():
                if company.email:
                    self.add_and_send_email(email_list, company.email, 'company')
                if company.user and company.user.email:
                    self.add_and_send_email(email_list, company.user.email, 'company')
        if args[0] == 'users':
            send = False
            for user in User.objects.filter(company__isnull=True):
                if user.email:
                    if send:
                        self.add_and_send_email(email_list, user.email, 'users')
                        print user.email, ' --- ', 'send'
                    if user.email == 'ciamangar@yandex.com':
                        send = True
                    else:
                        print user.email, ' --- ', 'not send'

    def add_and_send_email(self, emails, obj, kind):
        if kind == 'company':
            template = 'order/inclusion/email_send_company.html'
            subject = u'Обновление сайта DostMarket.ru'
        else:
            template = 'order/inclusion/email_send_user.html'
            subject = u'Получайте доставку с выгодой до 70%'
        if obj not in emails:
            self.send_email(subject, [obj, ], template)
            emails.append(obj)

    def send_email(self, subject, e_meil, email_template):
        t = loader.get_template(email_template)
        context = {'host': 'dostmarket.ru'}
        context = Context(context)
        html = (t.render(context)).encode('utf-8')
        msg = EmailMessage(subject, html, settings.EMAIL_HOST_USER, e_meil, headers={})
        msg.content_subtype = "html"
        msg.send(fail_silently=True)