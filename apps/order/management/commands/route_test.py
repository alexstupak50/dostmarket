# -*- coding: utf-8 -*-
from apps.company.models import Company
import uuid
from order.models import OrderCategory, Route, Order
from django.core.management.base import BaseCommand
from django.utils.html import mark_safe, strip_tags
from geo.models import Country
from order.utils import notification_new_order
from mailer.models import Message, MessageLog


class Command(BaseCommand):
    def handle(self, *args, **options):
        company = Company.objects.filter(email='com.alex@bigmir.net')
        for order in Order.objects.all():
            notification_new_order(order, company)

        # Message.objects.all()[1:].delete()
        # print company[0].user.routes.all()
        # company[0].user.routes.all()[1:].delete()

