# -*- coding: utf-8 -*-
from django import template, middleware
from django.utils import timezone
from order.forms import OrderBetForm
from order.models import OrderBet, OrderCategory
from django.contrib.auth.forms import AuthenticationForm
register = template.Library()


@register.inclusion_tag('order/inclusion/login_form.html',
                        takes_context=True)
def login_form(context):
    request = context['request']
    return {'form': AuthenticationForm(request)}


@register.inclusion_tag('inclusion/order_category.html')
def order_category_modal():
    return {
        'category_list': OrderCategory.objects.all()
    }

@register.filter
def split(value, arg):
    return value.split(arg)


@register.simple_tag(takes_context=True)
def get_params(context, new=False, **kwargs):
    """
    Для добавления гет параметров до уже существующих
    """
    data = dict()
    if not new:
        data = {k: v for k, v in context['request'].GET.iteritems() if v}

    for key, value in kwargs.iteritems():
        data[key] = value

    if not data:
        return

    params = '?'
    for key, value in data.iteritems():
        params = '%s%s=%s&' % (params, key, value)
    return params[:-1]


@register.inclusion_tag('order/inclusion/order_bet_form.html',
                        takes_context=True)
def order_bet_form_tag(context):
    user = context['request'].user

    return {
        'form': OrderBetForm(
            user=user, order=context['order']),
        'order': context['order'],
        'request': context['request'],
        # 'is_can_bet': is_can_bet,
        # 'bet_error_message': bet_error_message,
        # 'bet_error_type': bet_error_type,
        # 'next_time_bet': context['order'].get_next_time_bet_by_user(
        #     context['request'].user),
        # 'now': timezone.now()
    }

@register.filter
def add_comma(obj):
    if obj:
        return ', %s' % obj
    else:
        return ''

@register.filter
def switch_class_color_order(value):
    mark = {
        4: 'fail',
        2: 'success',
    }
    return mark[value] if value in mark else 'waiting'

@register.filter
def switch_class_color_bet(value):
    mark = {
        2: 'fail',
        1: 'success',
    }
    return mark[value] if value in mark else 'waiting'

@register.filter
def color(value):
    mark = {
        4: 'canceled',
        6: 'canceled',
    }
    return mark[value] if value in mark else ''

@register.filter
def br(value):
    if not value:
        return '-'
    return value.replace('/', '/<br>').replace('+', '+<br>')
