#coding: utf-8
import string
import requests
import re
import random
import urllib
import urllib2
import json
from django.db.models import Q
from ckeditor.widgets import CKEditorWidget
from django.template.loader import render_to_string
from itertools import islice, chain
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from smart_selects.widgets import ChainedSelect
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from dateutil import tz
from smart_selects.widgets import ChainedSelect
from django.core.mail import send_mail, EmailMultiAlternatives
from django.conf import settings
from company.models import Company
from order.models import OrderAddress, OrderName, OrderBet, TmpUsers, OrderService, Route
import datetime
from django.contrib.auth.forms import AuthenticationForm
from django.utils import timezone
from django.contrib.auth.models import User
from registration.models import RegistrationProfile
from order.utils import get_object_or_none, calc_dist, RemoveEmptyLabelMixin
from order.models import Order, Profile, OrderAddress, MODALITIES, OrderCategory, OrderService
from geo.models import Country, City, Region
from apps.email_mailer.models import COMPANY_TYPE

TOPICALITY_ORDER = (
    (1, _(u'Запрос актуален 1 день')),
    (2, _(u'Запрос актуален 2 дня')),
    (3, _(u'Запрос актуален 3 дня')),
    (4, _(u'Запрос актуален 4 дня')),
    (5, _(u'Запрос актуален 5 дней')),
    (6, _(u'Запрос актуален 6 дней')),
    (7, _(u'Запрос актуален 7 дней')),
)


class OrderCategoryMixin(object):
    """
    Устанавливает необходимые поля у формы заказа в завимисти от категории
    """
    def __init__(self, *args, **kwargs):
        try:
            self.category = kwargs.pop('category')
        except KeyError:
            if not hasattr(self, 'catagery'):
                self.category = None
        super(OrderCategoryMixin, self).__init__(*args, **kwargs)
        if self.category:
            for field, value in self.category.get_fields().items():
                if field in self.fields and not value:
                    # del self.fields[field]
                    pass


class OrderAddressForm(forms.ModelForm):
    class Meta:
        model = OrderAddress


class OrderNameForm(forms.ModelForm):
    class Meta:
        model = OrderName

    def __init__(self, *args, **kwargs):
        super(OrderNameForm, self).__init__(*args, **kwargs)
        self.fields['count'].required = False
        self.fields['name'].required = True

    def clean_count(self):
        count = self.cleaned_data['count']
        return count if count else 1

    def save(self, commit=True):
        obj = super(OrderNameForm, self).save(commit)

        if commit:
            obj.save()

        return obj


class OrderForm(OrderCategoryMixin, forms.ModelForm):
    username = forms.CharField(label=_(u'Имя'), max_length=30, required=True)
    email = forms.EmailField(label=_(u'E-Mail адрес'), required=True)
    # password = forms.CharField(label=_(u'Пароль'), max_length=30, required=False)
    phone = forms.CharField(
        label=_(u'Мобильный телефон'), max_length=15, required=True)
    modalities = forms.ChoiceField(
        widget=forms.RadioSelect(attrs={'id': 'value'}), choices=MODALITIES, required=False)

    address_from = forms.CharField(label=_(u'Улица, дом'), max_length=255, required=False)
    address_to = forms.CharField(label=_(u'Улица, дом'), max_length=255, required=False)
    country_from = forms.CharField(label=_(u'Страна(Откуда)'), required=False)
    country_to = forms.CharField(label=_(u'Страна(Куда)'), required=False)
    city_from = forms.CharField(label=_(u'Город'), max_length=255, required=False)
    city_to = forms.CharField(label=_(u'Город'), max_length=255, required=False)

    class Meta:
        model = Order
        fields = ('name', 'body', 'photo', 'cargo_weight', 'cargo_volume',
                  'loaders', 'can_be_distilled', 'not_on_move',
                  'buy_and_transportation', 'date', 'time', 'price_max',
                  'topicality', 'modalities', 'currency', 'payment')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        edit = kwargs.pop('edit', None)
        super(OrderForm, self).__init__(*args, **kwargs)
        # self.fields['payment'].empty_label = u'Форма оплаты'
        # self.fields["payment"].choices = [("", u'Форма оплаты'),] + list(self.fields["payment"].choices)[1:]
        # self.fields['payment'].widget.choices = self.fields['payment'].choices
        self.fields['topicality'].input_formats = ('%d.%m.%Y',)
        self.fields['topicality'].required = True

        if edit:
            if self.instance and self.instance.price_max:
                self.initial['price_max'] = '%d' % self.instance.price_max
            for prefix in [('from', 'start'), ('to', 'end')]:
                for field in ['country', 'city', 'address']:
                    obj = getattr(self.instance, '%s_address' % prefix[1])
                    if obj:
                        self.fields['%s_%s' % (field, prefix[0])].initial = getattr(obj, field)
            if self.initial.get('date'):
                self.initial['date'] = self.initial['date'].strftime('%d.%m.%Y')
            if self.initial.get('topicality'):
                self.initial['topicality'] = self.initial['topicality'].strftime('%d.%m.%Y')
        category = kwargs.pop('category', None)
        if category:
            self.fields['loaders'].queryset = category.services.all()

        if self.user.is_authenticated():
            # если есть юзер нет необходимости вводить данные для него
            del self.fields['email']
            del self.fields['phone']
            del self.fields['username']

    def clean_modalities(self):
        value = self.cleaned_data['modalities']
        if not value:
            return 0
        else:
            return value

    def clean_email(self):
        email = self.cleaned_data['email']
        if not self.user.is_authenticated():
            users = User.objects.filter(Q(email=email) | Q(username=email))
            if users.count():
                raise forms.ValidationError(
                    _(u'Пользаватель с таким e-mail уже зарегистрирован на нашем сайте. Для завершения заявки, вам нужно авторизоваться.'))
        if self.user.is_authenticated() and self.user.is_carrier():
            raise forms.ValidationError(
                _(u'На данный е-mail зарегистрирован перевозчик.'))
        return email
    # def clean_phone(self):
    #     value = self.cleaned_data['phone']
    #     r = re.compile('\+\d{7,14}')
    #     if not r.search(value):
    #         raise forms.ValidationError(
    #             _(u'Номер должен быть в международнем формате, '
    #               u'и содержать знак + и 7-14 цыфр'))
    #     return value

    def clean_date(self):
        value = self.cleaned_data['date']
        if value and value < timezone.now().date():
            raise forms.ValidationError(_(u'Дата перевозки не может быть '
                                          u'в прошедшем времени'))
        if not value:
            return value
        return datetime.datetime.combine(value, datetime.time(0, 0))

    def clean_topicality(self):
        value = self.cleaned_data['topicality']
        if value and value.date() < timezone.now().date():
            raise forms.ValidationError(_(u'Дата перевозки не может быть '
                                          u'в прошедшем времени'))
        return value

    def clean_time(self):
        time = self.cleaned_data['time']
        if time:

            try:
                hour, minute = int(time[:2]), int(time[3:])
                if hour > 23 or minute > 59:
                    raise forms.ValidationError(
                        _(u'Не верно указанно время.'))
            except ValueError:
                raise forms.ValidationError(
                    _(u'Не верно указанно время.'))
        return time

    def clean(self):
        # country_to, country_from, city_from, address_to
        suffixs = ('_to', '_from')
        cd = self.cleaned_data
        for suffix in suffixs:
            url = 'http://geocode-maps.yandex.ru/1.x/'
            country = cd['country' + suffix]
            city = cd['city' + suffix]
            address = cd['address' + suffix]
            if len(city.split(',')) > 1 and u'город' in city.split(',')[1]:
                city = city.split(',')[0]
            data = ' '.join([country, city, address])
            params = {
                'format': 'json',
                'geocode': data,
                'lang': 'ru_RU'
            }
            r = requests.get(url, params=params)
            result = r.json()['response'].get('GeoObjectCollection')
            try:
                metaDataProperty = result.get('featureMember')[0]['GeoObject']['metaDataProperty']
                if int(result['metaDataProperty']['GeocoderResponseMetaData']['found']) != 0:
                    metaDataProperty = metaDataProperty['GeocoderMetaData']['AddressDetails']['Country']
                    cd['country'+suffix] = metaDataProperty['CountryName']
                    if 'SubAdministrativeArea' in metaDataProperty['AdministrativeArea']:
                        AdministrativeAreaName = metaDataProperty['AdministrativeArea']['SubAdministrativeArea'].get('AdministrativeAreaName')
                        AdministrativeAreaName_Region = metaDataProperty['AdministrativeArea'].get('AdministrativeAreaName')
                    else:
                        AdministrativeAreaName_Region = metaDataProperty['AdministrativeArea']['AdministrativeAreaName']
                    if AdministrativeAreaName_Region:
                        cd['region'+suffix] = AdministrativeAreaName_Region
                    elif AdministrativeAreaName:
                        cd['region'+suffix] = AdministrativeAreaName
                    else:
                        cd['region'+suffix] = metaDataProperty['AdministrativeArea']['SubAdministrativeArea']['SubAdministrativeAreaName']
                    if 'SubAdministrativeArea' not in metaDataProperty['AdministrativeArea']:
                        cd['city'+suffix] = metaDataProperty['AdministrativeArea']['Locality']['LocalityName']
                    else:
                        cd['city'+suffix] = metaDataProperty['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName']
                    pos = result.get('featureMember')[0]['GeoObject']['Point']['pos']
                    cd['lat_position'+suffix] = float(pos.split(' ')[1])
                    cd['long_position'+suffix] = float(pos.split(' ')[0])
                else:
                    raise forms.ValidationError(
                        _(u'Произошла ошибка, возможно вы указали несуществующий адрес.'))
            except (KeyError, IndexError):
                raise forms.ValidationError(
                        _(u'Произошла ошибка, возможно вы указали несуществующий адрес.'))
            try:
                country = get_object_or_none(Country, code=cd['country'+suffix]) or \
                          get_object_or_none(Country, name=cd['country'+suffix])
                if not country:
                    raise forms.ValidationError(
                        _(u'Не верно указана страна. Только Россия, Украина, Беларусь и Казахстан.'))

                cd['country'+suffix] = country
                region = get_object_or_none(Region, country=country, name=cd.get('region'+suffix, None))
                city = City.objects.filter(name=cd['city'+suffix], region=region)
                if city:
                    cd['city'+suffix] = city[0]
                    cd['region'+suffix] = region
                else:
                    # гугл может не правильно отдавать регион для городов, поему даем шанс
                    # найти регион через город
                    city = City.objects.filter(name=cd['city'+suffix])
                    if city:
                        cd['city'+suffix] = City.objects.filter(name=cd['city'+suffix])[0]
                        if cd['city'+suffix].region:
                            cd['region'+suffix] = cd['city'+suffix].region
                        else:
                            cd['region'+suffix] = None
                if not city:
                    raise forms.ValidationError(
                        _(u'Произошла ошибка, возможно вы указали несуществующий '
                          u'адрес.'))
            except ObjectDoesNotExist:
                raise forms.ValidationError(
                    _(u'Произошла ошибка, возможно вы указали несуществующий '
                      u'адрес.'))
        cd['distance'] = calc_dist(
            cd['long_position_to'], cd['lat_position_to'],
            cd['long_position_from'], cd['lat_position_from']
        )
        return super(OrderForm, self).clean()

    def save(self, commit=True):
        data = self.cleaned_data
        obj = super(OrderForm, self).save(commit=False)
        obj.distance = data.get('distance', 0)
        user = self.user
        if not user or not user.is_authenticated():
            passwd = self.generator()
            user = User.objects.create_user(
                data['email'], data['email'], passwd)
            user.first_name = data['username']
            # изза того что в базе даных хранаться пользователи с однаковым емейл, будем делать проверу
            user_profile, create = Profile.objects.get_or_create(
                user=user, phone=data['phone'])
            p = TmpUsers()
            p.user = user
            ctx_dict = {'user': user, 'password': passwd}
            subject = render_to_string('registration/subject_registration.txt', ctx_dict)
            subject = ''.join(subject.splitlines())
            message = render_to_string('registration/registration_email.txt', ctx_dict)
            user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)
            p.save()
            user.save()
        obj.user = user
        if commit:
            obj.save()
        return obj

    def generator(self, size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))


class OrderListFilterForm(forms.Form):

    category = forms.ModelChoiceField(
        queryset=OrderCategory.objects.all())
    distance_from = forms.IntegerField(label=_(u'От:'), help_text=_(u'км'))
    distance_to = forms.IntegerField(label=_(u'До:'), help_text=_(u'км'))
    weight_from = forms.IntegerField(label=_(u'От:'), help_text=_(u'кг'))
    weight_to = forms.IntegerField(label=_(u'До:'), help_text=_(u'кг'))
    weight_unknown = forms.BooleanField(label=_(u'Вес не указан'), initial=True)
    date_from = forms.DateField(label=_(u'С:'))
    date_to = forms.DateField(label=_(u'По:'))
    country_to = country_from = forms.ModelChoiceField(
        label=_(u'Страна'), queryset=Country.objects.all(), empty_label=u"Страна")
    # city_from = city_to = forms.CharField(label=_(u'Город'))
    region_from = forms.ModelChoiceField(queryset=Region.objects.all(), empty_label=u"Регион",
                                         widget=ChainedSelect(
                                             app_name='geo',
                                             model_name='Region',
                                             chain_field='country_from',
                                             model_field='country',
                                             auto_choose=False,
                                             show_all=False,)
    )
    region_to = forms.ModelChoiceField(queryset=Region.objects.all(), empty_label=u"Регион",
                                       widget=ChainedSelect(
                                           app_name='geo',
                                           model_name='Region',
                                           chain_field='country_to',
                                           model_field='country',
                                           auto_choose=False,
                                           show_all=False,)
    )
    back = forms.BooleanField(label=_(u'Обратный маршрут'), required=False)

    def __init__(self, *args, **kwargs):
        super(OrderListFilterForm, self).__init__(*args, **kwargs)

        self.fields['region_from'].widget.queryset = \
            self.fields['region_to'].widget.queryset = Region.objects.all()


class OrderBetForm(forms.ModelForm):
    is_correspond = forms.BooleanField(label=u'Время соответствует заказу', required=False)
    is_price_order = forms.BooleanField(label=u'Цена соответсвует заказу', required=False)

    class Meta:
        model = OrderBet
        exclude = ('order', 'user', 'status')
        widgets = {
            'transportation_type': forms.RadioSelect,
            }
        remove_empty_label = ('transportation_type', 'currency')

    def __init__(self, *args, **kwargs):
        self.order = kwargs.pop('order', None)
        self.user = kwargs.pop('user', None)
        if not self.user or not self.order:
            raise TypeError(
                '%s need order and user in arguments' % self.__class__.__name__)
        super(OrderBetForm, self).__init__(*args, **kwargs)
        self.fields['date'].input_formats = ('%d.%m.%Y', )
        self.fields['date'].required = True
        # if self.order.date:
        #     if self.order.get_datetime():
        #         self.fields['date'].initial = self.order.get_datetime() \
        #             .astimezone(tz.gettz(self.order.timezone)).date() \
        #             .strftime('%d.%m.%Y')
        #     else:
        #         self.fields['date'].initial = self.order.date.strftime(
        #             '%d.%m.%Y')

        # if self.order.get_datetime():
        #     dt = self.order.get_datetime().astimezone(
        #         tz.gettz(self.order.timezone))
        #     self.fields['time'].initial = '%2d:%2d' % (dt.hour, dt.minute)
        # else:
        #     self.fields['time'].initial = self.order.time

        self.fields['service'].queryset = self.order.category.services.all()
        self.fields['service'].empty_label = _(u'Укажите услугу')

        # if self.order.loaders:
        #     self.fields['service'].required = True
        #     self.fields['service'].initial = self.order.loaders - 1

        if not self.order.category.services.exists():
            del self.fields['service']
        if self.data.get('is_price_order') and 'on' in self.data.get('is_price_order'):
            self.fields['price'].required = False
        else:
            self.fields['price'].required = True

    def clean_time(self):
        time = self.cleaned_data['time']
        if time:
            try:
                hour, minute = int(time[:2]), int(time[3:])
                if hour > 23 or minute > 59:
                    raise forms.ValidationError(
                        _(u'Не верно указанно время.'))
            except ValueError:
                raise forms.ValidationError(
                    _(u'Не верно указанно время.'))
        return time

    def clean_price(self):
        price = self.cleaned_data['price']
        if price and price < 1:
            raise forms.ValidationError(_(u'Цена не может быть меньше 1'))
        return float(price) if price else None

    def clean(self):
        cd = self.cleaned_data
        if cd['is_price_order']:
            cd['price'] = self.order.price_max or 0
            cd['currency'] = self.order.currency
        return cd

    def save(self, commit=True):
        if not self.user.is_authenticated():
            raise ValueError('user must be auhenticated')
        try:
            if not self.user.company:
                raise ValueError('user must be carrier')
        except Company.DoesNotExist:
            pass
        obj = super(OrderBetForm, self).save(commit=False)
        obj.order = self.order
        if self.cleaned_data.get('is_correspond'):
            obj.data = self.order.date
            obj.time = self.order.time
        obj.user = self.user
        # obj.refresh_dates_by_timezone()
        if commit:
            obj.save()
        return obj


class ChangePasswordForm(forms.Form):
    password_cur = forms.CharField(
        label=_(u'Введите текущий пароль'), widget=forms.PasswordInput, max_length=32)
    password1 = forms.CharField(widget=forms.PasswordInput(), label=_(u'Введите новый пароль'), max_length=32)
    password2 = forms.CharField(widget=forms.PasswordInput(), label=_(u'Подтвердите новый пароль'), max_length=32)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_password_cur(self):
        password_cur = self.cleaned_data['password_cur']
        if self.user:
            if not self.user.check_password(password_cur):
                raise forms.ValidationError(_(u'Неверный пароль'))
        return password_cur

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 != password2:
            raise forms.ValidationError(_(u'Указанные пароли не совпадают'))
        return password2


class AuthenticationForm2(AuthenticationForm):
    def clean(self):
        data = super(AuthenticationForm2, self).clean()
        if self.user_cache.is_carrier():
            raise forms.ValidationError(u'На данный е-mail зарегистрирован перевозчик.')
        return data


class RouteForm(forms.ModelForm):
    #FIXME Профиксить этот баг
    """
        python2.7/site-packages/smart_selects/widgets.py
        70 строка
        empty_label = iter(self.choices).next()[1]
        заменить на
        try:
            empty_label = iter(self.choices).next()[1]  # Hacky way to getting the correct empty_label from the field instead of a hardcoded '--------'
        except:
            empty_label = '---------'
    """
    class Meta:
        model = Route
        exclude = ('user')
        widgets = {
            'kind': forms.RadioSelect(attrs={'class': 'styled'}),
            }

    def __init__(self, user=None, *args, **kwargs):
        super(RouteForm, self).__init__(*args, **kwargs)
        self.user = user
        self.fields['categories'].widget = forms.CheckboxSelectMultiple()
        # self.fields['region_to'].choices = [u'Регион', ]
        # self.fields['region_to'].empty_label = '--------'
        # self.fields['region'].choices = [u'Регион', ]
        # self.fields['region'].empty_label = '--------'
        self.fields['categories'].choices = [
            (e.pk, e.icon, e.name) for e in OrderCategory.objects.all().only(
                'pk', 'icon', 'name')]
        if self.instance.pk:
            self.fields['categories'].initial = [
                c.pk for c in self.instance.categories.all().only('pk')]
        if self.data:
            kind = self.data.get('kind')
            if kind and int(kind) == 2:
                self.fields['country_to'].required = True
                self.fields['region'].required = False
                self.fields['region_to'].required = False
            if self.data.get('is_weight'):
                self.fields['weight_from'].required = True
                self.fields['weight_to'].required = True
        if self.data.get('categories'):
            self.fields['categories'].initial = [int(pk) for pk in self.data.getlist('categories')]

    def save(self, commit=True):
        obj = super(RouteForm, self).save(commit=False)
        if self.user:
            obj.user = self.user
        obj.save()
        if self.instance.pk:
            obj.categories.clear()
        obj.categories.add(*self.cleaned_data['categories'])
        return obj


USER_TYPE = (
    (1, u'Пользователи'),
    (2, u'Компании и курьеры'),
)

TYPE = USER_TYPE + tuple((item[0], item[1] + u'(не зарегистрированые)') for item in COMPANY_TYPE) + ((3, u'Все'),)


class SendMessages(forms.Form):
    user_type = forms.MultipleChoiceField(
        label=u'Кому',
        choices=TYPE,
        widget=forms.CheckboxSelectMultiple())
    country = forms.ModelChoiceField(queryset=Country.objects.all(), required=False)
    region = forms.ModelChoiceField(queryset=Region.objects.all(),
                                    widget=ChainedSelect(
                                        app_name='geo',
                                        model_name='Region',
                                        chain_field='country',
                                        model_field='country',
                                        auto_choose=False,
                                        show_all=False,),
                                    required=False,
                                    )
    city = forms.ModelChoiceField(queryset=City.objects.all(),
                                  widget=ChainedSelect(
                                      app_name='geo',
                                      model_name='City',
                                      chain_field='region',
                                      model_field='region',
                                      auto_choose=False,
                                      show_all=False,),
                                  required=False
    )
    subject = forms.CharField(label=u'Тема письма', widget=forms.Textarea)
    body = forms.CharField(widget=CKEditorWidget(), help_text=u'Текст сообщения')

    is_test = forms.BooleanField(initial=False, widget=forms.HiddenInput, required=False)

    # class Meta:
    #     fields = ('user_type', 'country', 'region', 'city', 'subject', 'body', 'is_test')

