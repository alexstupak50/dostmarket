# vim:fileencoding=utf-8
from models import AllowIP,DenyIP
from django.contrib import admin


admin.site.register(AllowIP)
admin.site.register(DenyIP)
