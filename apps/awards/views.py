# vim:fileencoding=utf-8
from django.views.generic import TemplateView, View, FormView, CreateView
from models import AwardCompany, VotesAwards, BlackList
from company.models import Company
from django.db.models import Avg, Max, Q
import json
from django import http
from django import forms
from datetime import date, timedelta, datetime, time
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

class AddVoteForm(forms.Form):
    email = forms.EmailField(max_length=255)
    name = forms.CharField(max_length=255)
    id_company = forms.ModelChoiceField(queryset=AwardCompany.objects.all())

class AwardCompanyForm(forms.ModelForm):
    class Meta(object):
        model = AwardCompany
        exclude = ['created','hack','votes','votes_all','active']

class FrontAwards(TemplateView):
    template_name = "awards/front.html"

class SuccessRequest(TemplateView):
    template_name = "awards/success.html"

class VoteAwards(TemplateView):
    template_name = "awards/vote.html"

    def get_context_data(self, **kwargs):
        ctx = super(VoteAwards,self).get_context_data(**kwargs)
        ctx['companies'] = AwardCompany.objects.filter(type=kwargs.get('type','courier'), active=True).order_by('company__name')
        ctx['max'] = ctx['companies'].aggregate(max=Max('votes_all'))['max']
        return ctx


class JSONResponseMixin(object):
    def render_to_response(self, context):
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        return http.HttpResponse(content, content_type='application/json; charset=UTF-8', **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)

class AddVote(JSONResponseMixin, View):

    def post(self, request, *args, **kwargs):
        data = {'valid':False, 'search':False}
        form = AddVoteForm(request.POST)
        black = BlackList.objects.filter(ip=get_client_ip(request)).count()
        if not request.POST.get('captcha', False) and form.is_valid() and black == 0:
            form = form.clean()
            begin = date.today()-timedelta(days=7)
            end = datetime.combine(datetime.now(), time(hour=23,minute=59,second=59))
            search = VotesAwards.objects.filter(company__type=form['id_company'].type).filter(Q(email=form['email']) | Q(ip=get_client_ip(request)))#.filter(created__lte=end, created__gte=begin)
            if search.count() > 0:
                data['search'] = True
            else:
                vote = VotesAwards.objects.create(name=form['name'], email=form['email'], company=form['id_company'], ip=get_client_ip(request))
            data['qty'] = form['id_company'].update_vote()
            data['valid'] = True
        return self.render_to_response(data)


class RequestForm(CreateView):
    template_name = "awards/request.html"
    form_class = AwardCompanyForm
    success_url = "/awards/request/success/"

    def get_form(self, form_class):
        form = form_class(**self.get_form_kwargs())
        form.fields["company"].queryset = Company.objects.all().order_by('name')
        return form
