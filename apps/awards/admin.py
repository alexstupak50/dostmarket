# vim:fileencoding=utf-8
from awards.models import AwardCompany, VotesAwards, BlackList
from django.contrib import admin

class VotesInline(admin.TabularInline):
    model = VotesAwards
    extra = 1

class AwardCompanyAdmin(admin.ModelAdmin):
    list_display = ('company', 'active')
    readonly_fields = ['votes', 'votes_all']
    inlines = [VotesInline]

    def log_change(self, request, object, message):
        super(AwardCompanyAdmin, self).log_change(request, object, message)
        object.update_vote()


# admin.site.register(AwardCompany, AwardCompanyAdmin)
# admin.site.register(BlackList)
