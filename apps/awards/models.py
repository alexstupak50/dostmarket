# vim:fileencoding=utf-8
from django.db import models
from company.models import Company

COMPANY_TYPE = (
    ('transport', 'Транспортная компания'),
    ('courier', 'Курьерская служба'),
)


class AwardCompany(models.Model):
    class Meta(object):
        verbose_name = u'Компания'
        verbose_name_plural = u'Компании'
        ordering = ['company']

    company = models.ForeignKey(Company, verbose_name=u'именем', unique=True)
    site = models.CharField(max_length=255, verbose_name=u'Официальный сайт компании')
    person = models.CharField(max_length=255, verbose_name=u'Контактное лицо')
    phone = models.CharField(max_length=255, verbose_name=u'Контактный телефон')
    email = models.EmailField(max_length=255, verbose_name=u'Контактный e-mail')
    type = models.CharField(max_length=255, verbose_name=u'Номинация для участия', choices=COMPANY_TYPE, default='courier')
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата', null=True, blank=True)
    hack = models.IntegerField(default=0, verbose_name=u'Накрутка голосов')
    votes = models.IntegerField(default=0, verbose_name=u'Реальные голоса')
    votes_all = models.IntegerField(default=0, verbose_name=u'Общее количество голосов')
    active = models.BooleanField(verbose_name=u'Заявка принята', default=False)

    def __unicode__(self):
        return self.company.name

    def update_vote(self):
        self.votes = self.votes_people.all().count()
        self.votes_all = self.hack + self.votes
        self.save()
        return self.votes_all

class VotesAwards(models.Model):
    class Meta(object):
        verbose_name = u'Голос'
        verbose_name_plural = u'Голос'
        ordering = ['company']

    company = models.ForeignKey(AwardCompany, verbose_name=u'Название компании', related_name='votes_people')
    name = models.CharField(max_length=255, verbose_name=u'Имя')
    email = models.EmailField(max_length=255, verbose_name=u'E-mail')
    ip = models.IPAddressField(verbose_name=u'IP-адрес', blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата', null=True, blank=True)

    def __unicode__(self):
        return self.name

class BlackList(models.Model):
    class Meta(object):
        verbose_name = u'IP-адрес'
        verbose_name_plural = u'IP-адреса'

    ip = models.IPAddressField(verbose_name=u'IP-адрес')
