#coding: utf-8
import json
import urllib
import urllib2

from django.core.management.base import BaseCommand, CommandError
from vkcity.models import Cities, Countries
from geo.models import Country, City, Region

"""
vk

country_id = models.IntegerField(primary_key=True)
name = models.CharField(max_length=765)
currency_code = models.CharField(max_length=15)
currency = models.CharField(max_length=765, blank=True)

geo

code = models.CharField(_('country code'), max_length=2, blank=True, null=True)
name = models.CharField(_('country name'), max_length=255, unique=True)
currency_code = models.CharField(max_length=15)
currency = models.CharField(max_length=765, blank=True)

"""


class Command(BaseCommand):
    args = '<type>'
    def handle(self, *args, **options):
        type = args[0]

        if type == 'country':
            for country in Countries.objects.using('vk_city').filter(country_id__in=[1, 2, 3, 4]):
                Country.objects.create(name=country.name, id=country.country_id)

        if type == 'region':
            for country in Country.objects.all():
                url = 'http://api.vk.com/method/database.getRegions'
                params = {
                    'country': country.id,
                    'lang': 'RU',
                }

                encoded_params = urllib.urlencode(params)
                url = url + '?' + encoded_params
                try:
                    response = urllib2.urlopen(url)
                    result = json.loads(response.read())
                    for region in result['response']:
                        Region.objects.get_or_create(
                            name=region.get('title'), country=country, id=region.get('region_id'))
                except urllib2.URLError as e:
                    print e

        if type == 'city':
            """
            geo

            country = models.ForeignKey(Country)
            region = models.ForeignKey(Region, related_name='cities', blank=True, null=True)
            name = models.CharField(_('city name'), max_length=255)
            latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
            longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)

            vk

            country_id = models.IntegerField()
            city = models.CharField(max_length=765)
            state = models.CharField(max_length=765, blank=True)
            region = models.CharField(max_length=765)
            biggest_city = models.IntegerField(null=True, blank=True)
            """

            for city in Cities.objects.using('vk_city').filter(country_id__in=[1, 2, 3, 4]):
                q = dict()
                country = Country.objects.filter(id=city.country_id)
                if country:
                    q['country'] = country[0]
                if city.region:
                    region = Region.objects.filter(name__icontains=city.region, country__id=city.country_id)
                    if region:
                        q['region'] = region[0]
                q['name'] = city.city
                City.objects.create(**q)