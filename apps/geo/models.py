#coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    code = models.CharField(_('country code'), max_length=2, blank=True, null=True)
    name = models.CharField(_('country name'), max_length=255, unique=True)
    currency_code = models.CharField(max_length=15, blank=True, null=True)
    currency = models.CharField(max_length=765, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _(u'Страна')
        verbose_name_plural = _(u'Страны')


class Region(models.Model):

    country = models.ForeignKey(Country, related_name='regions')
    name = models.CharField(_('region name'), max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('region')
        verbose_name_plural = _('regions')
        unique_together = (('country', 'name'), )


class City(models.Model):
    country = models.ForeignKey(Country)
    region = models.ForeignKey(Region, related_name='cities', blank=True, null=True)
    name = models.CharField(_('city name'), max_length=255)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')
