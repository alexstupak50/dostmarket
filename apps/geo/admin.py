# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Country, Region, City

class CityAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_filter = ('region',)

admin.site.register(Country)
admin.site.register(Region)
admin.site.register(City, CityAdmin)