# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Email.country'
        db.add_column('email_mailer_email', 'country',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geo.Country'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Email.region'
        db.add_column('email_mailer_email', 'region',
                      self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['geo.Region'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Email.city2'
        db.add_column('email_mailer_email', 'city2',
                      self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['geo.City'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Email.country'
        db.delete_column('email_mailer_email', 'country_id')

        # Deleting field 'Email.region'
        db.delete_column('email_mailer_email', 'region_id')

        # Deleting field 'Email.city2'
        db.delete_column('email_mailer_email', 'city2_id')


    models = {
        'email_mailer.email': {
            'Meta': {'object_name': 'Email'},
            'city2': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': "orm['geo.City']", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geo.Country']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'region': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': "orm['geo.Region']", 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        'geo.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geo.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cities'", 'null': 'True', 'to': "orm['geo.Region']"})
        },
        'geo.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '765', 'blank': 'True'}),
            'currency_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'geo.region': {
            'Meta': {'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regions'", 'to': "orm['geo.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['email_mailer']