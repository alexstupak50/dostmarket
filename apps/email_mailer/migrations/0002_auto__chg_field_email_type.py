# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Email.type'
        db.alter_column('email_mailer_email', 'type', self.gf('django.db.models.fields.SmallIntegerField')(default=1))

    def backwards(self, orm):

        # Changing field 'Email.type'
        db.alter_column('email_mailer_email', 'type', self.gf('django.db.models.fields.SmallIntegerField')(null=True))

    models = {
        'email_mailer.email': {
            'Meta': {'object_name': 'Email'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {})
        }
    }

    complete_apps = ['email_mailer']