#coding: utf-8
from django.core.management.base import BaseCommand
from mailer.models import Message, MessageLog


class Command(BaseCommand):
    args = '<type>'

    def handle(self, *args, **options):
        interavel = 2000
        max_val = MessageLog.objects.latest('id').id
        print 'max_val = %s, interavel = %s' % (max_val, interavel)
        t = [x for x in range(0, max_val, interavel)]
        l = zip(t, t[1:])
        l.append((l[-1][1], max_val))
        for ran in l:
            mes = MessageLog.objects.filter(id__range=ran)
            count = ' count = %s' % mes.count()
            mes.delete()
            print 'delete -- ' + str(ran) + count