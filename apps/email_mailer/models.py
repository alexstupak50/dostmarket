# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from smart_selects.db_fields import ChainedForeignKey
from import_export import resources, widgets, fields
from apps.geo.models import City, Country, Region

COMPANY_TYPE = (
    (4, u'Транспортная компания'),
    (5, u'Курьерская служба'),
    (6, u'Пользователи')
)


class Email(models.Model):
    name = models.CharField(_(u'Название комании'), max_length=200, blank=True)
    email = models.EmailField(_(u'E-mail адресс'))
    type = models.SmallIntegerField(
        _(u'Тип компании'), choices=COMPANY_TYPE)
    country = models.ForeignKey(
        'geo.Country', verbose_name=_(u'Страна'),
        null=True, blank=True)
    region = ChainedForeignKey(
        'geo.Region', verbose_name=_(u'Область'), null=True, blank=True,
        chained_field='country', chained_model_field='country', auto_choose=True)
    city = ChainedForeignKey(
        'geo.City', verbose_name=_(u'Город'), null=True, blank=True,
        chained_field='region', chained_model_field='region', auto_choose=True)

    class Meta:
        verbose_name_plural = verbose_name = u'Email'

    def __unicode__(self):
        return self.email


class NoEmail(models.Model):
    email = models.CharField(u'Емейл', max_length=250)

    class Meta:
        verbose_name = u'Не существующие email'
        verbose_name_plural = u'Не существующие email'

    def __unicode__(self):
        return self.email


class MyWidget(widgets.ForeignKeyWidget):
    def clean(self, value):
        # name = super(MyWidget, self).clean(value)
        try:
            return City.objects.filter(country=1, name=value)[0]
        except:
            return None


class MyWidgetCountry(widgets.ForeignKeyWidget):
    def clean(self, value):
        # name = super(MyWidget, self).clean(value)
        try:
            return Country.objects.get(id=int(value))
        except:
            return None


class MyWidgetRegion(widgets.ForeignKeyWidget):
    def clean(self, value):
        # name = super(MyWidget, self).clean(value)
        try:
            return City.objects.filter(country=1, name=value)[0].region
        except:
            return None


class EmailResource(resources.ModelResource):
    city = fields.Field(column_name='city', attribute='city', widget=MyWidget(City, 'name'))
    country = fields.Field(column_name='country', attribute='country', widget=MyWidgetCountry(Country))
    region = fields.Field(column_name='region', attribute='region', widget=MyWidgetRegion(Region))

    class Meta:
        model = Email
        fields = ['id', 'city', 'type', 'name', 'email', 'country', 'region']
