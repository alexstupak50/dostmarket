from django.contrib import admin
from .models import Email, EmailResource, NoEmail
from import_export.admin import ImportExportModelAdmin


# class EmailAdmin(admin.ModelAdmin):
#     list_display = ('email', 'name', 'type')
#     list_filter = ('type',)
#     search_fields = ('email', 'name')


class BookAdmin(ImportExportModelAdmin):
    resource_class = EmailResource
    list_display = ('email', 'name', 'type')
    list_filter = ('type',)
    search_fields = ('email', 'name')

admin.site.register(Email, BookAdmin)
admin.site.register(NoEmail)