VERSION = (1, 9, 2)
__version__ = '.'.join(map(str, VERSION))


class LazySettings(object):
    def _load_settings(self):
        from fein_admin_menu import default_settings
        from django.conf import settings as django_settings

        for key in dir(default_settings):
            if not key.startswith('FEINCMS_'):
                continue

            value = getattr(default_settings, key)
            value = getattr(django_settings, key, value)
            setattr(self, key, value)

    def __getattr__(self, attr):
        self._load_settings()
        del self.__class__.__getattr__
        return self.__dict__[attr]

settings = LazySettings()
