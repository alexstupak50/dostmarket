jQuery.fn.extend({
    alert: function(dict) {
        dict = typeof dict !== 'undefined' ? dict : {};
        dict.title = typeof dict.title !== 'undefined' ? dict.title : "Ошибка";
        dict.text = typeof dict.text !== 'undefined' ? dict.text : "Исправьте ошибки";
        dict.id = typeof dict.id !== 'undefined' ? dict.id : "error_form";
        dict.reload = typeof dict.reload !== 'undefined' ? dict.reload : false;
        dict.redirect_url = typeof dict.redirect_url !== 'undefined' ? dict.redirect_url : false;
        var parametrs = {
            padding:[0, 0, 0, 0],
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            helpers: {
                overlay: {
                    css: {'background': 'rgba(35,72,88, 0.5)'},
                }
            }
        };
        if (dict.reload) {
            parametrs.afterClose = function () {
                window.location.reload();
            }
        }
        if (dict.redirect_url) {
            parametrs.afterClose = function () {
                document.location = dict.redirect_url;
            }
        }
        var compiled = _.template($("#alert").html());
        $('#alert_container').html(compiled(dict));
        $.fancybox($('#'+dict.id), parametrs);
        return this;
    },
    getParameterByName: function getParameterByName(name){
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    removeParam: function removeParam(key, url) {
        var sourceURL = typeof url !== 'undefined' ? url : location.search;
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    },
    parentForm: function() {
        return $(this).closest('form');
    },
    refresh: function () {
        $('select').styler();
        $('select').trigger('refresh');
        $(".your-time").mask("99:99");
        Custom.refresh();
    },
    alax_post: function (calback) {
        var form = $(this).parentForm();
        var button_submit = $(this);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            beforeSend: function () {
                button_submit.addClass('progress_button');
            },
            success: calback,
            error: function () {
                $.fn.alert({
                    'text': 'Произошла ошибка, попробуйте пожалуйста позже'
                })
            }
        });
    },
    process_start: function () {
        $(this).addClass('progress_button');
    },
    process_end: function () {
        $(this).removeClass('progress_button');
    }
});

function login_window_show() {
    if ($.fn.getParameterByName('login')) {
        $.fancybox.open('.log');
        window.history.replaceState(null, null, location.protocol + '//' + location.host + location.pathname)
    }
}
function equalHeight(group) {
    var tallest = 0;
    group.each(function() {
        var thisHeight = $(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.height(tallest);
}

function edit_toggle() {
    $('.blue-table td.edit').toggleClass('hide');
    $('.blue-table td.edit').next().toggleClass('hide');
    $('.edit_profile').toggleClass('hide');
}

function _toggle(obj) {
    if ($(obj).hasClass('opened')) {
        $(obj).removeClass('opened');
        $('.cp-info .change_password, .my-orders .container-sm, .company-info .change_password').slideToggle();
    } else{
        $(obj).addClass('opened');
        $('.cp-info .change_password, .my-orders .container-sm, .company-info .change_password').slideToggle();
    }
}

function _error(selector, template, params) {
    params = typeof params !== 'undefined' ? params : {};
    params.check_user = typeof params.check_user!== 'undefined' ? params.check_user: false;
    $(selector).click(function (e) {
        e.preventDefault();
        var show = true;
        //Если юзера не авторизироване то ошибка
        if (params.check_user && $(template).attr('data-user') != 'AnonymousUser')
            show = false;
        if (show) {
            $.fn.alert({
                text: $(template).html()
            });
        }
    });
}
function submit_login_form(button_submit, $form) {
    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        beforeSend: function () {
            button_submit.addClass('progress_button');
        },
        success: function (data) {
            if ($(data).find('#check_error').html()) {
                $('#login').html(data);
            }
            else {
                window.location.reload();
            }

        }
    });
}
$(document).ready(function(){

    $('#twitter a').click(function () {
        $.fancybox.close()
    });

    $('.not_custumer').click(function (e) {
        e.preventDefault();
        $.fn.alert({
            'text': $('#template_not_custumer').html()
        });
    });
    $('#comments_add .blue-btn.comment-btn').click(function (e) {
        e.preventDefault();
        var button = $(this);
        $(this).alax_post(function (data) {
            if (data.valid) {
                button.removeClass('progress_button');
                if (data.parent) {
                    $('.comments a[data-parent='+ data.parent +'], .news_comments a[data-parent='+ data.parent +']')
                        .closest('.comment-info').after(data.html);
                } else
                    $('.comments .container-sm, .news_comments').append(data.html);
                $('aside.left').height(function (index, height) {
                    return (height + 75);
                });
                var $form = $('.add-comment-form');
                $form.fadeOut('fast');
                $form.find('textarea').val(' ');
                $form.find('input[name=parent]').val('');
                $.fn.alert({
                    title: 'Добавление комментария',
                    'text': 'Ваш комментарий добавлен.'
                });
            } else {
                $.fn.alert({
                    'text': 'Произошла ошибка'
                })
            }
        });
    });

    $('body').on('click', ".order-table tbody tr", function () {
        var id, $td = $(this).find('td:first-child');
        if ($(this).closest('.order-table').prev().hasClass('count-company')){
            return true;
        }
        if ($td.find('p:first-child').length)
            id = $td.find('p:first-child').html().trim();
        else
            id = $td.html().trim();
        if (id)
            document.location = '/order/detail/' + id + '/'
    });
    $('input').focus(function () {
        $(this).css('text-align', 'left');
    }).blur(function () {
        $(this).css('text-align', 'center');
    });
    $.each($('input, textarea'), function(index, value) {
        $(this).data('holder', $(this).attr('placeholder'));
    });

    $('input, textarea').focusin(function(){
        $(this).attr('placeholder','');
    });

    $('input, textarea').focusout(function(){
        $(this).attr('placeholder', $(this).data('holder'));
    });
    // error function
    _error('#not_carrier', '#template_not_carrier');
    _error('.add-comment', '#template_not_commenting', {check_user:true});
    _error('a.reply', '#template_not_commenting', {check_user:true});
    //

    $(".your-time, .time").mask("99:99");
    $('#items_on_page').change(function (e) {
        e.preventDefault();
        document.location = $(this).data('url') +'?items_on_page=' + $(this).val()+'#sort';
    });
    $('.like').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $.get($this.attr('href'), function (data) {
            $this.find('em').html(data.count);
        });
    });
    $('#search_order').click(function (e) {
        e.preventDefault();
        var val = $('#specify_order').val();
        if (parseInt(val, 10) && val != '')
            document.location = '/order/detail/'+val+'/';
        else
            $.fn.alert({
                text: 'Не верные данные'
            });
    });

    $('.respond-company-form .cancel').click(function (e) {
        e.preventDefault();
        var $form = $('#respond_main_form');
        $form.find('textarea').val(' ');
        $form.slideUp();
        $(this).toggle();
        $('#respond_main_open').toggle();
    });

    $('#respond_main_open').click(function (e) {
        e.preventDefault();
        var $form = $('#respond_main_form');
        $form.find('#id_company').val($(this).data('company'));
        $form.slideDown();
        $(this).toggle();
        $('.respond-company-form .cancel').toggle();
    });

    $('body').on('click', '.add_respond', function (e) {
        e.preventDefault();
        var $wrap_form = $('.add-comment-form');
        $wrap_form.find('#id_parent').val($(this).attr('rel'));
        $wrap_form.find('#id_company').val($(this).data('company'));
        $(this).closest('.comm-info').before($wrap_form.slideDown());
    });
    $('body').on('click', '#submit_respond', function (e) {
        var button = $(this);
        e.preventDefault();
        $(this).alax_post(function (data) {
            button.removeClass('progress_button');
            $('#submit_respond').parentForm().parent().slideUp();
            $('#submit_respond').parentForm().find('textarea').val(' ');
            $.fn.alert({
                'title': 'Отправка отзыва',
                'text': 'Отзыв добавлен, для подтверждения пожалуйста пройдите по ссылке, отправленной вам в письме на ваш e-mail, указанный при регистрации.'
            });
        });
    });
    $('#load_image').click(function(e) {
        e.preventDefault();
        $('.browse').click();
    });
    $('.browse').change(function() {
        $(this).closest('form').submit();
    });

    $('body').on('keydown', '#id_password' ,function(event) {
        if (event.keyCode == 13) {
            submit_login_form($('#login_submit'), $(this).parentForm());
        }
    });

    $('body').on('click', '#login_submit', function (e) {
        e.preventDefault();
        var button_submit = $(this);
        var $form = button_submit.closest('form');
        submit_login_form(button_submit, $form);
        return false;
    });


    $('.click').click(function (e) {
        e.preventDefault();
        edit_toggle();
    });

    $('.change_password_b').click(function (e) {
        e.preventDefault();
        $('.edit_password').toggleClass('hide');
    });
    $('#change_pass_submit').click(function (e) {
        e.preventDefault();
        var $form = $('.change_password_form');
        var button = $(this);
        button.process_start();
        $.post($form.attr('action'), $form.serialize(), function (data) {
            button.process_end();
            if (data.valid) {
                $.fn.alert({
                    title: 'Смена пароля',
                    text: 'Пароль успешно изменен'
                });
                _toggle(this);
                $form.find('input:visible').val(' ');
                $($form).find('.error').html(' ').removeClass('error');
                $('.edit_password').toggleClass('hide');
            } else {
                $form.replaceWith($(data));
                $.fn.alert();
            }

            //$('.change_password').toggleClass('opened').slideToggle();
        });
    });
    $('#respond_company').click(function (e) {
        e.preventDefault();
        var uri = $.fn.removeParam('id');
        uri = $.fn.removeParam('page', uri);
        url = window.location.pathname + '?' + uri.replace('?', '') + '&id=' + $('.reviews-search #company').attr('data-id') + '#respondes';
        document.location = url;
    });

    $('#tabs1').addClass($.fn.getParameterByName('mark'));
    var dateToday = new Date();
    $(".datepicker" ).datepicker({
        numberOfMonths: 1,
        buttonImageOnly: true,
        dateFormat: 'dd.mm.yy',
        minDate: dateToday
    });
    $('.reg_form').submit(function () {
        if (!$('#id_agree').is(":checked")) {
            $.fn.alert({
                text: 'Вы должны принять условия соглашения и конфиденциальности'
            });
            return false;
        }
    });
    $('.add-company .btn-box a').hover(function(){
        $(this).parent().addClass('hovered');
    },function(){
        $(this).parent().removeClass('hovered');
    });

    $('.star, .header-cp + div .company-stars').raty({
        starOff: '/media/static/new_img/star-off.png',
        starOn: '/media/static/new_img/star-on.png',
        starHalf: '/media/static/new_img/star-half.png',
        readOnly: true,
        score: function() {
            return $(this).attr('data-rating');
        }
    });

    $('.company-stars-big').raty({
        starOff: '/media/static/new_img/big_star_off.png',
        starOn: '/media/static/new_img/big_star_on.png',
        starHalf: '/media/static/new_img/big_star_half.png',
        readOnly: true,
        score: function() {
            return $(this).attr('data-rating');
        }
    });

    var movingBlock = $('.news_comments .add-comment-form, .comments .add-comment-form').hide();
    $('.add-comment').click(function(e){
        e.preventDefault();
        if (!$(this).data('not-user')) {
            movingBlock.slideToggle();
        }
    });

    $('.function-block .lock, .edit_password .change_password_b').click(function(e){
        e.preventDefault();
        _toggle(this);
    });


    $(".fancybox").fancybox({
        padding:[0, 0, 0, 0],
        openEffect  : 'elastic',
        closeEffect : 'elastic',
        wrapCSS: 'make-rate',
        autoResize: true,
        helpers: {
            overlay: {
                css: {'background': 'rgba(35,72,88, 0.5)'},
            }
        }
    });

    $(".log").fancybox({
        padding:[0, 0, 0, 0],
        openEffect  : 'elastic',
        closeEffect : 'elastic',
        helpers: {
            overlay: {
                css: {'background': 'rgba(35,72,88, 0.5)'},
            }
        }
    });

    (function($) {
        $(function() {
            var scroll, api;
            $('.red-block select, .count-company select, .reviews-page select').styler({
                selectVisibleOptions: '6',
                onSelectOpened: function() {
                    scroll = $('.dropdown > .jq-selectbox__dropdown > ul').jScrollPane({
                        verticalDragMinHeight: 20,
                        verticalDragMaxHeight: 20,
                        autoReinitialise: true
                    });
                },
                onSelectClosed: function(){
                    api = scroll.data('jsp');
                    if (api) { api.destroy(); }
                }
            });
            $('body').on('change', '.jqselect select', function () {
                $('select').trigger('refresh');
            });
            $('select').styler();
            $('select').trigger('refresh');
        })
    })(jQuery);

    $('.filters#tabs a').click(function(event) {
        event.preventDefault();
        var target = $(this).attr('href');
        $('.tabs-block > div').removeClass('visible');
        $(target).addClass('visible');
    });

    $('.order_modal').fancybox({
        href: '#order_modal',
        wrapCSS: 'modal_categories',
        helpers: {
            overlay: {
                css: {'background': 'rgba(35,72,88, 0.5)'}
            }
        }
    });

    $('.ordering_form .type a').click(function(event) {
        event.preventDefault();
        $(this).parent().find('input').val($(this).text());
    });

    //$('.add-notice').click(function(event) {
    //    event.preventDefault();
    //    $(this).toggle();
    //    $('.my-bets, .my-notice .blue-table, .add-notice-form').slideToggle();
    //});

    $('body').on('click', '.add-notice-form .cancel', function(event) {
        event.preventDefault();
        $('.add-notice').toggle();
        $('.my-bets, .my-notice .blue-table, .add-notice-form').slideToggle();
    });

    setTimeout(function(){equalHeight($('.news_content > *'))}, 200);

    $('.news_content .left').width(($(window).width()-900)/2+221);
    $('.news_content .news_list').css('margin-left', (($(window).width()-900)/2+221));
    $('.news_content .news_detail').css('margin-left', (($(window).width()-900)/2+221));
    $(window).resize(function(event) {
        $('.news_content .left').width(($(window).width()-900)/2+221);
        $('.news_content .news_list').css('margin-left', (($(window).width()-900)/2+221));
        $('.news_content .news_detail').css('margin-left', (($(window).width()-900)/2+221));
    });
});
$(document).ready(function (){
    $(".respond-text").dotdotdot({
        height: 50
    });
    $(".order-table-name").dotdotdot({
        height: 40,
    });
    $(".news_item .item_desc header a").dotdotdot({
        height: 50,
    });
    $("div.news_item div.item_desc p").dotdotdot({
        height: 70,
    });
    $('.menu-button').click(function () {
        $(this).toggleClass('active');
        $('.top-menu').slideToggle();
    });
    //$('body').on('click', '.checkbox', function() {
    //    // поведение чекбокса как радио(в идеали нужно поправить css в место этого)
    //    var this_input = $(this).next();
    //    if (!this_input.hasClass('radio'))
    //        return false;
    //    var parent_td = $(this).closest('td');
    //    parent_td.find('input').prop('checked', false);
    //    parent_td.find('.checkbox').css('background-position', '0px 0px');
    //    $(this).closest('td').find('input').each(function () {
    //        $(this).next().prop('checked', false);
    //        $(this).next().css('background-position', '0px 0px');
    //    });
    //    this_input.prop('checked', true);
    //    $(this).css('background-position', '0px -62px');
    //});
});