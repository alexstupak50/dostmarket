
function change_route_container(e, container_selector, load_url, is_toggle) {//load route add/edit form and list to container
    e.preventDefault();
    is_toggle = typeof is_toggle !== 'undefined' ? is_toggle : true;
    $(container_selector).load(load_url, function () {
        if (is_toggle) {
            $('.my-bets, .my-notice .blue-table, .add-notice-form, .add-notice').slideToggle();
            $.fn.refresh();
        }
    });
}
function apply_datepicker() {
    var dateToday = new Date();
    $(".datepicker" ).datepicker({
        numberOfMonths: 1,
        buttonImageOnly: true,
        dateFormat: 'dd.mm.yy',
        minDate: dateToday
    });
}

$(function() {
    $('.confirm a').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $.get($this.attr('data-url'), function (data) {
            console.log('1');
            console.log(data);
            $.fn.alert(data);
            if (data.valid){
                $this.closest('td').html(data.status);
            }

        })
    })
    $('.cp-table tbody tr td').not('.confirm').click(function() {
        var url = $(this).parent().data('url');
        if (url) {
            document.location  = url;
        }
    });
    $('.choice-y a, .choice-n a').click(function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var $this = $(this);
        $.get(url, function (data) {
            $($this).closest('td').html(data.status);
            $.fn.alert({
                title: data.title,
                text: data.text
            })
        });
    });

    //$('body').on("click", '.order .pagination a',function(e) {
    //    e.preventDefault();
    //    var url = $(this).attr('href') + ' .order';
    //    $('.order').load(url);
    //    $('body,html').animate({
    //        scrollTop: 300
    //    }, 800);
    //    return false;
    //});

    $('body').on('click', '#submit_route_form', function (e) {
        e.preventDefault();
        var form = $(this).parentForm();
        var button_submit = $(this);
        $.ajax({
            url: $(form).attr("action"),
            type : $(form).attr("method"),
            data: $(form).serialize(),
            beforeSend: function() {
                button_submit.addClass('progress_button');
            },
            success: function (data) {
                button_submit.removeClass('progress_button');
                if (data.valid == true) {
                    $("#routes").load(data.success_url+' .blue-table');
                    $('.add-notice').toggle();
                    $('.my-bets, .my-notice .blue-table, .add-notice-form').slideToggle();
                } else {
                    $('.add-notice-form').html(data);
                    $.fn.refresh();
                    $.fn.alert();
                }
            }
        });
        return false;
    });

    $('body').on('click', '#submit_bet', function(e){
        e.preventDefault();
        var button_submit = $(this);
        var $form = $(this).closest('form');
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: $form.serialize(),
            beforeSend: function () {
                button_submit.addClass('progress_button');
            },
            success: function (data) {
                button_submit.removeClass('progress_button');
                if (!data.valid) {
                    $('#make_rate').html($(data).html());
                    apply_datepicker();
                    $.fn.refresh('#make_rate');
                } else {
                    //reload: true
                    $form.find('input:visible').val('');
                    $.fn.alert(data);
                }
                    //window.location.reload();
            },
            error: function () {
                button_submit.removeClass('progress_button');
            }
        })
    });
    $('#cancel_order').click(function (e) {
        e.preventDefault();
        $.fn.alert({
            title:'Подтверждение',
            text: $('#confirm_cancel_order').html()
        });
    });

    $('body').on('click', '.confirm .no', function (e) {
        e.preventDefault();
        $.fancybox.close();
    });
    $('body').on('click', '.confirm .yes',function (e) {
        e.preventDefault();
        var $this = $(this);
        $.get($this.attr('href'), function (data) {
            $.fn.alert(data);
        });
    });

    $('#submit_order').click(function (e) {
        e.preventDefault();
        var $form = $(this).closest('form');
        var button_submit = $(this);
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: $form.serialize(),
            beforeSend: function () {
                button_submit.addClass('progress_button');
                button_submit.attr("disabled", "disabled");
                $('div.error').html('');
                $('*').removeClass('has_error').removeClass('error');
            },
            success: function (data) {
                button_submit.removeAttr("disabled");
                button_submit.removeClass('progress_button');
                if (!data.valid) {
                    $.fn.alert({
                        'title': 'Ошибка'
                    });
                    $.each(data.errors, function(key, value){
                        $('.'+key).addClass('error').append(value);
                        if (key == '__all__') {
                            $('.form_group.to').addClass('has_error');
                            $('.form_group.from input, .form_group.to input').addClass('error');
                        }
                        $('#id_' + key).addClass('error');
                    });
                    $('.error').each(function() {
                        $(this).html(($(this).html().split('.').join('. ')));
                    });
                }
                else {
                    document.location  = data.redirect_url;
                }
            },
            error: function () {
                button_submit.removeClass('progress_button');
            }
        });
        return false;
    });
});



function add_city_autocomplete(selector, country_selector) {
    $(selector).on("keydown.autocomplete", function() {
        $(this).autocomplete({
            source: function(request, response) {
                if (country_selector)
                    country = $(country_selector).text();
                else
                    country = $(this.element).prev().val();
                switch (country) {
                    case 'Россия':
                        country = 1;
                        break;
                    case 'Украина':
                        country = 2;
                        break;
                    case 'Беларусь':
                        country = 3;
                        break;
                    case 'Казахстан':
                        country = 4;
                        break;
                    default:
                        return false;
                }
                $.ajax({
                    crossDomain: true,
                    url: "/order/city/autocomplete/",
                    dataType: "json",
                    data: {
                        input: request.term,
                        key: "AIzaSyC71GowU5j0LDkO03hBkNArl3WupvhkWOE",
                        country: country,
                    },
                    success: function( data ) {

                        response( $.map( data.predictions, function(item) {
                            var val = item.terms[1].value;
                            var text = '';
                            if (val.indexOf('город')> -1) {
                                text = item.terms[0].value;
                            }
                            else {
                                text = item.terms[0].value + ', '+ item.terms[1].value
                            }
                            return {
                                label: text,
                                value: text
                            }
                        }));
                    }
                });
            },
            minLength: 1,
            open: function() {
                $('.ui-autocomplete').addClass('f-dropdown');
            }
        });
    });
}