$(function () {
    $('.pops .vote-but').click(function () {
        if ($(this).parent().find('.popup:visible').size() > 0) {
            $(this).parent().find('.popup').hide(0);
        }
        else {
            $('.popup').hide(0);
            $(this).parent().find('.popup').show(0);
        }
        return false;
    });

    $('.add_vote').click(function(){
        var laureat = $(this).parents('.laureat').find('.total span');
        $.post('/awards/add/vote/', $(this).parents('form').serializeArray(), function(data){
            if (data['valid']) {
                $('.popup').remove(0);
                $('.pops .vote-but').addClass('noactive');
                if (data['search']) alert('Вы уже голосовали на этой неделе');
                laureat.html(data['qty']);
            }
            else {
                alert('Неправильно заполнена форма');
            }
        });
    });

    $('.radio input').change(function(){
        $('.radio').removeClass('active');
        $(this).parent().addClass('active');
        $('#id_type').val($('.radio.active input').val());
        $('.aw-small').hide(0);
        $('.aw-small.'+$('.radio.active input').val()).show(0);

    });

});


