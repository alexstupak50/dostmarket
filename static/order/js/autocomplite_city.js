$(function() {
    var availableCountry = [
        {'value': 1, 'label': 'Россия'},
        {'value': 2, 'label': 'Украина'},
        {'value': 3, 'label': 'Беларусь'},
        {'value': 4, 'label': 'Казахстан'}
    ];

    $("input[name='country_from'], input[name='country_to']").autocomplete({
        source: availableCountry,
        focus: function(event, ui) {
            event.preventDefault();
            $(this).next().data('data-id', ui.item.value);
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).next().data('data-id', ui.item.value);
            var id = '#'+$(this).attr('id')+'_pk';
            $(id).val(ui.item.value);
            $(this).val(ui.item.label);
        }
    });

    //$("#id_city_from, #id_city_to").autocomplete({
    //    source: function(request, response){
    //        var city = [];
    //        var id = $(this.element).data('data-id');
    //        if (id==undefined){
    //            var input = this.element;
    //            var id_val = $(input).prev().val();
    //            if (id_val) id = id_val;
    //            else id=1;
    //        }
    //        console.log(id)
    //        var url = 'http://api.vk.com/method/database.getCities';
    //        $.ajax({
    //            url: url,
    //            dataType: 'jsonp',
    //            data: {
    //                'v': '5.5',
    //                'country_id': id,
    //                'q': request.term
    //            },
    //            success: function(data){
    //                for(var i=0;i<data.response.count;i++) {
    //                    city.push({
    //                        value: data.response.items[i].id,
    //                        label: data.response.items[i].title
    //                    });
    //                }
    //                response(city);
    //            }
    //        })
    //    },
    //
    //    focus: function(event, ui) {
    //        event.preventDefault();
    //        $(this).val(ui.item.label);
    //    },
    //    select: function (event, ui) {
    //        event.preventDefault();
    //        $(this).val(ui.item.label);
    //    },
    //});
});