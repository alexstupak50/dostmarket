function init() {
    var myMap = new ymaps.Map('map', {
            center: [55.73, 37.75],
            zoom: 3,
            type: 'yandex#map',
            behaviors: ['scrollZoom', 'drag']
        }),
        calculator = new DeliveryCalculator(myMap, myMap.getCenter());

    join_coordinate(calculator);

    $('#search-address').click(function(e) {
        e.preventDefault();
        var selectedIdsArray1 = $(".form_group.from     input").map(function(){
            return $(this).val();
        });
        var selectedIdsArray2 = $(".form_group.to input").map(function(){return $(this).val();});
        var address1 = selectedIdsArray1.get().join(' ');
        var address2 = selectedIdsArray2.get().join(' ');
        if (address1 == '  ' || address2 == '  ') {
            $('.not-valid-text.address').show();
            $(".route input").addClass('not-valid');
            return false;
        }
        else {
            $('.not-valid-text.address').hide();
            $(".route input").removeClass('not-valid');
        }

        ymaps.geocode(address1, {results: 1 }).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0),
                coord_lat1 = firstGeoObject.geometry.getCoordinates();
            calculator.setStartPoint(coord_lat1);
            $('#id_long_position_to').val(coord_lat1[0]);
            $('#id_lat_position_to').val(coord_lat1[1]);
        });

        ymaps.geocode(address2, {results: 1 }).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0),
                coord_lat2 = firstGeoObject.geometry.getCoordinates();
            calculator.setFinishPoint(coord_lat2);
            $('#id_long_position_from').val(coord_lat2[0]);
            $('#id_lat_position_from').val(coord_lat2[1]);
        });
    });
}

function join_coordinate(calculator) {
    var position = $('#map').data('coordinate');
    if (position){
        calculator.setStartPoint([position[1], position[0]] );
        calculator.setFinishPoint([position[3], position[2]]);
    }
}

function DeliveryCalculator(map, finish) {
    this._map = map;
    this._start = null;
    this._route = null;

    //map.events.add('click', this._onClick, this);
}

var ptp = DeliveryCalculator.prototype;

ptp._onClick= function (e) {
    if (this._start) {
        this.setFinishPoint(e.get('coordPosition'));
    } else {
        this.setStartPoint(e.get('coordPosition'));
    }
};

ptp._onDragEnd = function (e) {
    this.getDirection();
}

ptp.getDirection = function () {
    if (this._route) {
        this._map.geoObjects.remove(this._route);
    }

    if (this._start && this._finish) {
        var self = this,
            start = this._start.geometry.getCoordinates(),
            finish = this._finish.geometry.getCoordinates();

        ymaps.geocode(start, { results: 1 })
            .then(function (geocode) {
                var address = geocode.geoObjects.get(0) &&
                    geocode.geoObjects.get(0).properties.get('balloonContentBody') || '';

                ymaps.route([start, finish])
                    .then(function (router) {
                        var distance = Math.round(router.getLength() / 1000),
                            message = '<span>Расстояние: ' + distance + 'км.</span><br/>' +
                                '<span style="font-weight: bold; font-style: italic">Стоимость доставки: %sр.</span>';

                        self._route = router.getPaths();
                        $('#id_distance').val(distance);
                        self._route.options.set({ strokeWidth: 5, strokeColor: '0000ffff', opacity: 0.5 });
                        self._map.geoObjects.add(self._route);
                        self._start.properties.set('balloonContentBody', address + message.replace('%s', self.calculate(distance)));

                    });
            });
        self._map.setBounds(self._map.geoObjects.getBounds())
    }
};

ptp.setStartPoint = function (position) {
    //if(this._start) {
    //    this._start.geometry.setCoordinates(position);
    //}
    //else {
        this._start = new ymaps.Placemark(position, { iconContent: 'А' }, { draggable: false });
        this._start.events.add('dragend', this._onDragEnd, this);
        this._map.geoObjects.add(this._start);
    //}
    this.getDirection();
};

ptp.setFinishPoint = function (position) {
    //if(this._finish) {
    //    this._finish.geometry.setCoordinates(position);
    //}
    //else {
        this._finish = new ymaps.Placemark(position, { iconContent: 'Б' }, { draggable: false });
        this._finish.events.add('dragend', this._onDragEnd, this);
        this._map.geoObjects.add(this._finish);
    //}
    this.getDirection();
};

ptp.calculate = function (len) {
    // Константы.
    var DELIVERY_TARIF = 20,
        MINIMUM_COST = 500;
    return Math.max(len * DELIVERY_TARIF, MINIMUM_COST);
};

ymaps.ready(init);
