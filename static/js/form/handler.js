$(document).ready(function() {
    $('body').on('click', '.reply', function (e) {
        e.preventDefault();
        var $comment_form = $('.add-comment-form');
        $comment_form.fadeOut('fast');
        var parent_id = $(this).attr('data-parent');
        $comment_form.find('input[name=parent]').val(parent_id);
        $comment_form.insertAfter($(this).parents('.comment-info').find('.com-inf')).fadeIn();
        $('.add-comment').click(function (e) {
            e.preventDefault();
            $comment_form.insertAfter($(this)).fadeIn();
            return false;
        });
    });
});
