function Order(option) {
    this.step = 0;
    this.width = 780;
    this.height = 900;
    this.url_order = '/';
}
var ptp = Order.prototype;

ptp.test = function() {
};

ptp.close_dialog = function() {
    var order = this;
    $(order.cur_dialog).dialog('close');
    $( ".selector" ).datepicker( "destroy" );
    $( ".datepicker" ).datepicker( "destroy" );
    $('.dialog-w').dialog('destroy').empty().remove();
    $('.ui-dialog').remove();
    return false;
};

ptp.bet_order = function(elem, selector) {
    var order = this;
    var url = $.data(document.body, 'url');
    $.getJSON(url, function(data) {
        if (data.valid) {
            location.reload();

        }

        if (data.valid && data.status==1 ) {
            $(selector).find('var').html(data.performer_name);
            order.open_static_dialog(selector);
        }
        else if (data.valid && data.status==2) { }
        else {
            alert(data.error)
        }
    });
    return false;
};


ptp.bet_order_form = function(elem, selector, order_id) {
    var order = this;
    var error = $(elem).data('error');
    if (error) {
        alert(error);
        return false;
    }
    order.open_static_dialog(selector);
    return false;
};

ptp.submit_add_bet = function(form) {
    var order = this;
    var form = $(form);

    $.post(form.attr('action'), form.serialize(), function(data) {
        if (data.valid) {
            order.open_static_dialog('.select-perfomer.bet-add');
            setTimeout(function() {
                location.reload();
            }, 3000)
        }
        else
        order._error(data, 'not-valid');
    });
    return false;
};


ptp.dialog = function(params) {
    var order = this;
    var w = typeof params.width !== 'undefined' ? params.width : order.width;
    var h = typeof params.height !== 'undefined' ? params.height : order.height;
    var url = typeof params.url !== 'undefined' ? params.url : '/';
    order.step = typeof params.step !== 'undefined' ? params.step : 0;
    if (order.step) {
        order.cur_dialog = $('.step-'+order.step);
        if (order.step == 1 && params.type != 'trigger') {
            $("<div/>").appendTo("body").append('<div class="order-form-wrap step-1 dialog-w"></div>');
            $('.order-form-wrap').load(url, function() {
                $(this).find('.step-'+order.step).show();
            });
            order.cur_dialog = $('.order-form-wrap');
        }
    }
    else {
        if (!$('.dialog-confirm.step-0').length) {
            $("<div/>").appendTo("body").append('<div class="dialog-w dialog-confirm step-0"></div>');
        }
        order.cur_dialog = $('.dialog-confirm.step-0');
    }
    $(order.cur_dialog).dialog({
        step: order.step,
        height: h,
        width: w,
        resizable: false,
        draggable: false,
        position: {my: "center", at:"center", of: window },
        modal: true,
        create: function (event, ui) {
            $(this).parent().find('.ui-dialog-titlebar').hide();
            if(!order.step){$(this).load(url);}
        },
        open: function () {
            if (order.step == 0) {
                $('.ui-dialog').css(
                    "top", ( $(window).height() - $(this).height() ) / 2+$(window).scrollTop() +130+ "px");
            }
        }
    });
    return false;
};

ptp.trigger_dialog = function(st) {
    var order = this;
    var d = $('.dialog-w.step-' + st);
    if (d.hasClass('ui-dialog-content')) {
        $(order.cur_dialog).dialog('close');

        order.dialog({
            step: st,
            type: 'trigger',
            height: d.dialog('widget').height(),
            width: d.dialog('widget').width()
        });
    }
    return false;
}

ptp.__check_valid = function(step) {
    var is_valid = true;
    var is_test = false;
    if (!is_test) {
        $('.ui-widget-content .dialog-w.step-' + step + ' .field-step-' + step).each(function (i) {
            var $this = $(this);
            if ($this.val() == '') {
                $('.step-' + step + ' span.not-valid-text').not('.address').show();
                $($this).addClass('not-valid');
                is_valid = false;
            }
            else {
                $this.removeClass('not-valid');
            }
        });
    }
    return is_valid;
}

ptp.open_dialog = function(elem, param) {
    var order = this;
    param = typeof param !== 'undefined' ? param : {};
    var step = typeof param.step !== 'undefined' ? param.step : 1;
    step -= 1;
    param.url = $(elem).data('url');
    var is_valid = order.__check_valid(step);
    if (is_valid) {
        $('.step-'+step+' span.not-valid-text').hide();
        $(order.cur_dialog).dialog('close');
        order.dialog(param);
    }
    return false;
}

ptp.alert = function(text) {
    var order = this;
    order.open_static_dialog('.or-err.err');
    $('.or-err.err').find('.text-errors').html(text);
    $('.or-err.err').find('h1').hide();
};

ptp.open_static_dialog = function(selector, elem) {
    var order = this;
    $(selector).show();
    var url = $(elem).data('url');
    $.data(document.body, 'url', url);
    $(selector).dialog({
        resizable: false,
        draggable: false,
        modal: true,
        resize: 'auto',
        width: 'auto',
        dialogClass: "alert1",
        create: function (event, ui) {
            $(this).parent().find('.ui-dialog-titlebar').hide();
        },
        open: function(event, ui) {
            var user = $(elem).data('user');
            $(selector).find('var').html(user);
            var dateToday = new Date();
            $( ".datepicker" ).datepicker({
                numberOfMonths: 1,
                buttonImageOnly: true,
                dateFormat: 'dd.mm.yy',
                minDate: dateToday
            });
        }
    });
    return false;
};

ptp.submit = function(url) {
    var order = this;
    var form_data = $('.dialog-w input, .dialog-w textarea, .dialog-w select').serialize();
    var is_valid = order.__check_valid(3);
    if (is_valid) {
        $.ajax({
            url: url,
            type: 'POST',
            data: form_data,
            success: function(data) {
                if (data.valid) {
                    var $m_complete = $('.complete-order');
                    $('.dialog-w.step-3').dialog('close');
                    $m_complete.show();
                    $m_complete.dialog({
                        step: 4,
                        height: 220,
                        width: 680,
                        resizable: false,
                        draggable: false,
                        modal: true,
                        create: function (event, ui) {
                            $(this).parent().find('.ui-dialog-titlebar').hide();
                        },
                    });
                    order.cur_dialog = $m_complete;
                    setTimeout(function() {
                        order.close_dialog();
                    }, 4000)
                }
                else  {
                    order._error(data, 'not-valid');
                }
            },
            error: function() {
                alert('Извините, произошла ошибка. Попробуйте пожалуйста позже. ' +
                'В случае повторного возникновения ошибок, сообщите администрации сайта.')
            }
        });
    }
};

ptp.close_error_window = function() {
    order.open_static_dialog('.or-err.login');
    $('.or-err.err').dialog('close');
};

ptp._error = function(data, error_lass) {
    var arr = data.field_error;
    for (var i = 0; i < arr.length; i++) {
        $('input[name='+arr[i]+']').addClass(error_lass);
    }
    order.open_static_dialog('.or-err.err');
    var text = data.errors.replace('вход',
        '<a href="#1" class="err-link" onclick="order.close_error_window()">вход</a>');
    $('.or-err.err').find('.text-errors').html(text);
};

ptp.find_order_by_id = function() {
    if (!($("#id_find_detail_order").val() < 1000)) {
        var order_id = $("#id_find_detail_order").val() - 1000;
        if (order_id) {
            window.location.href = "/order/detail/" + order_id + "/";
        }
    }
    return false;
}

//ptp.place_bet = function(order_id) {
//    var order = this;
//
//    return false;
//}

ptp.add_suffix = function(e, selector, value, separator, erase_field, elem) {
    // add text to text field
    e.preventDefault();
    separator = separator || " ";
    value = value.replace(/\s+/, "");
    if ($(elem).hasClass('clone')) selector = '#id_order_name-1-name';
    if($(selector).val() && erase_field == false) {
        $(selector).val($(selector).val()+separator+value);
    } else {
        $(selector).val(value);
    }
}


//get_place({select: '#id_country', type: 'getCities', option: '#id_city2'})
function get_place(params) {
    $(params.select).change(function () {
        var country_id = $('#id_country').val();
        var data_params = {
            'v': '5.5',
            'country_id': country_id,
            'count': 1000,
            'need_all': 0
        };
        if (params.type == 'getCities')
            data_params['region_id'] = $(this).val();
        var url = 'http://api.vk.com/method/database.' + params.type;
        $.ajax({
            url: url,
            dataType: 'jsonp',
            data: data_params,
            success: function (data) {
                $(params.option).find("option").remove();
                for (var i = 0; i < data.response.count; i++) {
                    try {
                        $('<option>').val(
                            data.response.items[i].id).text(data.response.items[i].title).appendTo(params.option);
                    } catch(e) {}
                }
            }
        })
    });
}


function add_city_autocomplete(selector, country_selector) {
    $(selector).live("keydown.autocomplete", function() {
        $(this).autocomplete({
            source: function(request, response) {
                if (country_selector)
                    country = $(country_selector).text();
                else
                    country = $(this.element).prev().val();
                switch (country) {
                    case 'Россия':
                        country = 1;
                        break;
                    case 'Украина':
                        country = 2;
                        break;
                    case 'Беларусь':
                        country = 3;
                        break;
                    case 'Казахстан':
                        country = 4;
                        break;
                    default:
                        return false;
                }
                $.ajax({
                    crossDomain: true,
                    url: "/order/city/autocomplete/",
                    dataType: "json",
                    data: {
                        input: request.term,
                        key: "AIzaSyC71GowU5j0LDkO03hBkNArl3WupvhkWOE",
                        country: country,
                    },
                    success: function( data ) {

                        response( $.map( data.predictions, function(item) {
                            var val = item.terms[1].value;
                            var text = '';
                            if (val.indexOf('город')> -1) {
                                text = item.terms[0].value;
                            }
                            else {
                                text = item.terms[0].value + ', '+ item.terms[1].value
                            }
                            return {
                                label: text,
                                value: text
                            }
                        }));
                    }
                });
            },
            minLength: 1,
            open: function() {
                $('.ui-autocomplete').addClass('f-dropdown');
            }
        });
    });
}

function add_field(selector) {
    var obj = $(selector).prev();
    obj = add_name_inline_form(obj, 'order_name');
    var count = obj.find('.count-items').length;
    if (count < 2){
        var newElement = obj;
        if (newElement.find('.tpl-list') != true) {
            newElement.css('margin-top', '12px');
        }
        newElement.find('li').addClass('clone');
        newElement.find('input').val('');
        $(selector).hide();
    }
}

function add_name_inline_form(selector, type) {
    var newElement = $(selector).clone(true);
    var total = $('#id_' + type + '-TOTAL_FORMS').val();
    $(newElement).find(':input').each(function() {
        var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
        var id = 'id_' + name;
        $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
    });
    $(newElement).find('label').each(function() {
        var newFor = $(this).attr('for').replace('-' + (total-1) + '-','-' + total + '-');
        $(this).attr('for', newFor);
    });
    total++;
    $('#id_' + type + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
    return newElement;
}

function route_form(form) {
    var parent = $(form).parent();

    $.ajax({
        url: $(form).attr("action"),
        type : $(form).attr("method"),
        data: $(form).serialize(),
        beforeSend: function() {
            $(form).find(":input").attr("disabled", true);
        },
        success: function (data) {
            if (data.valid==true) {
               $("#routes").load(data.success_url);
            } else {
                $(form).remove();
                $(parent).append(data);
                $("#routes-notification-accordion").accordion("resize");
            }
        }
    });
    return false;
}

function change_route_container(e, container_selector, load_url) {//load route add/edit form and list to container
    e.preventDefault();
    $(container_selector).load(load_url, function(){
        $("#routes-notification-accordion").accordion("resize");
    });
}