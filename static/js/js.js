/**
 * Created by PyCharm.
 * User: Ivan
 * Date: 17.06.12
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

var templateForm = '<div class="formpoll"><input type="text" class="iname" name="name" placeholder="Ваше имя"><input  class="imail" type="text" name="email" placeholder="E-mail"><button onclick="polling(globalData[\'id\'],globalData[\'choice\']); return false;">Голосовать</button><button onclick="deleteForm();">Отмена</button></div>';
var globalData;

function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var result;

    for(var i = 0; i < hashes.length; i++){
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getCurrentCount(editor) {
    var currentLength = editor.getData()
            .replace(/<[^>]*>/g, '')
            .replace(/\s+/g, ' ')
            .replace(/&\w+;/g, 'X')
            .replace(/^\s*/g, '')
            .replace(/\s*$/g, '')
            .length;
    return currentLength;
}

function checkLength(evt) {
    var stopHandler = false;
    var currentLength = getCurrentCount(evt.editor);
    var maximumLength = 2500;

    if (evt.editor.config.MaxLength) {
        maximumLength = evt.editor.config.MaxLength;
    }

    if (!evt.editor.config.LockedInitialized) {
        evt.editor.config.LockedInitialized = 1;
        evt.editor.config.Locked = 0;
    }

    if (evt.data) {
        if (evt.data.html) {
            currentLength += evt.data.html.length;
        }
        else if (evt.data.text) {
            currentLength += evt.data.text.length;
        }
    }

    if (!stopHandler && currentLength >= maximumLength) {
        if (!evt.editor.config.Locked) {
            // Record the last legal content.
            evt.editor.fire('saveSnapshot');
            evt.editor.config.Locked = 1;
            // Cancel the keystroke.
            evt.cancel();
        }
        else
        // Check after this key has effected.
            setTimeout(function () {
                alert('Превышен лимит символов');
                // Rollback the illegal one.
                if (getCurrentCount(evt.editor) > maximumLength)
                    evt.editor.execCommand('undo');
                else
                    evt.editor.config.Locked = 0;
            }, 0);
    }
}


$(function () {
    $('#colvo, #gorod').change(function () {
        $('#company_list').submit();
    });
});

$(function () {

    $('.rad input').change(function(){
        var v = $('.rad input:checked').val();
        if (v == 'user') {
            $('.only_courier').hide();
            $('.hiden').hide(0);
        }
        else if (v == 'courier') {
            $('.hiden').show(0);
            $('.only_company').hide(0);
            $('.only_courier').show();


        }
        else {
            $('.only_courier').hide();
            $('.only_company').show(0);
            $('.hiden').show(0);
        }
    });

    $('.pole.txt .bottom a').click(function () {
        $('.pole.txt .bottom a').removeClass('active');
        $(this).addClass('active');
        $('#id_mark').val($(this).attr('rel'));
        //if ($(this).attr('rel') == 'negative') {
        //    $('#nomer').slideDown(200);
        //}
        //else {
        //    $('#nomer').slideUp(200);
        //}
        return false;
    });
    var mark = $('#id_mark').val();
    $('.pole.txt .bottom a').removeClass('active');
    $('.pole.txt .bottom a[rel=' + mark + ']').addClass('active');
    if (mark == 'negative') {
        $('#nomer').show(0);
    }

    mark = $('#gmark').val();
    $('.otzyvi .bottom a').removeClass('active');
    $('.otzyvi .bottom a[rel=' + mark + ']').addClass('active');

    mark = $('#sort_list').val();
    $('table a.ar').removeClass('active');
    $('table a.ar[rel=' + mark + ']').addClass('active');


    $('#regform').submit(function () {
        if ($('#id_agree').attr('checked')) {
            if ($('.rad input:checked').val() == 'user') {
                $('input[name=email]').val( $('input[name=email]:eq(0)').val());
                return true;
            }
            else {
            return true;}
        }
        else {
            alert('Вы должны принять условия соглашения и конфиденциальности');
            return false;
        }
    });

});

jQuery(document).ajaxSend(function (event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function sameOrigin(url) {
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
                !(/^(\/\/|http:|https:).*/.test(url));
    }

    function safeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
});


function ajaxDj() {
jQuery(document).ajaxSend(function (event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function sameOrigin(url) {
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
                !(/^(\/\/|http:|https:).*/.test(url));
    }

    function safeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
});
}

ajaxDj();

function polling(id, choice) {
    var d = {
        'id':id,
        'name':$('.formpoll .iname').val(),
        'email':$('.formpoll .imail').val(),
        'choice':choice
    };
    $.post("/api/poll/", d,
            function (data) {
                var mes = $('#lilst_company_'+id).find('.poll .message');
                if (data['valid']) {
                if (data['message']) {
                    setProcent(data['pro'],data['con'],id);
                    mes.html('Ваш голос учтен');
                    mes.css('top','-18px');
                    mes.fadeIn(150);
                    setTimeout(function(){ mes.fadeOut(150);}, 1500);
                }
                else {
                    if (data['limit']) {
                        mes.html('Вы уже голосовали за эту компанию');
                        mes.css('top','-25px');
                        mes.fadeIn(150);
                        setTimeout(function(){ mes.fadeOut(150);}, 1500);
                    }
                    else {
                    mes.html('У вас закончились доступные голоса, возращайтесь через 24 часа');
                    mes.css('top','-35px');
                    mes.fadeIn(150);
                    setTimeout(function(){ mes.fadeOut(150);}, 1500);}
                }}
                else {
                    mes.html('Не правильно заполнена форма');
                    mes.fadeIn(150);
                    mes.css('top','-25px');
                    setTimeout(function(){ mes.fadeOut(150);}, 1500);
                }

            });
    deleteForm();
    return false;
}

function setProcent(pro,con,id) {
    $('#lilst_company_'+id).find('.pro').html(pro+'%');
    $('#lilst_company_'+id).find('.con').html(con+'%');
}

function addMinus(id){
    globalData = {'id':id, 'choice':'con'};
    createForm('min', id);

//    polling(id, 'con');
}

function addPlus(id){
    globalData = {'id':id, 'choice':'pro'};
    createForm('pl', id);
//    polling(id, 'pro');

}

function createForm(classform, id) {
    $('.formpoll').remove();
    $('#lilst_company_'+id).find('.poll').append(templateForm);
    $('#lilst_company_'+id).find('.poll .formpoll').addClass(classform);
}

function deleteForm(){
    $('.formpoll').remove();
}

$(function(){
    /*$('.statistika').fancybox({
        afterClose:ajaxDj
    });*/

    var id_cargo = getUrlVars()["type"];
    if (id_cargo != undefined && decodeURIComponent(id_cargo) == 'Письмо'){
       $('#id_cargo_0').attr('checked', 'checked');
    }
    if (id_cargo != undefined && decodeURIComponent(id_cargo) == 'Бандероль'){
       $('#id_cargo_1').attr('checked', 'checked');
       $('#id_cargo_1').parent().addClass('active');
       $('#id_cargo_0').parent().removeClass('active');
    }



    //$('.statistiks').fancybox({
    //    afterClose: ajaxDj
    //});

    $('.zapros_form .knopki a').click(function(event) {
        event.preventDefault();
        return false;
    });

    $('.knopki_verh a').click(function(){
        $('.knopki_verh li').removeClass('active');
        $(this).parent().addClass('active');
        $('#d_type').val($(this).attr('rel'));
    });

/*    $('.qiwi').bind('click', function(event) {
    	event.preventDefault();
    	if($(this).hasClass('opened')){
            $('#form_qiwi').slideUp(500);
            $(this).removeClass('opened');
            $(this).html('Показать');
        }
        else{
            $('#form_qiwi').slideDown(500);
            $(this).addClass('opened');
            $(this).html('Скрыть');
        }
    });

    $('.yad').bind('click', function(event) {
    	event.preventDefault();
    	if($(this).hasClass('opened')){
            $('#form_yad').slideUp(500);
            $(this).removeClass('opened');
            $(this).html('Показать');
        }
        else{
            $('#form_yad').slideDown(500);
            $(this).addClass('opened');
            $(this).html('Скрыть');
        }
    });

    $('.bank_card').bind('click', function(event) {
    	event.preventDefault();
    	if($(this).hasClass('opened')){
            $('#form_bank_card').slideUp(500);
            $(this).removeClass('opened');
            $(this).html('Показать');
        }
        else{
            $('#form_bank_card').slideDown(500);
            $(this).addClass('opened');
            $(this).html('Скрыть');
        }
    });

    $('.nal').bind('click', function(event) {
    	event.preventDefault();
    	if($(this).hasClass('opened')){
            $('#form_nal').slideUp(500);
            $(this).removeClass('opened');
            $(this).html('Показать');
        }
        else{
            $('#form_nal').slideDown(500);
            $(this).addClass('opened');
            $(this).html('Скрыть');
        }
    });*/

    $('.op').bind('click', function(event) {
    	event.preventDefault();
    	if($(this).hasClass('opened')){
            $(this).parent().next().slideUp(500);
            $(this).removeClass('opened');
            $(this).html('Показать');
        }
        else{
            $(this).parent().next().slideDown(500);
            $(this).addClass('opened');
            $(this).html('Скрыть');
        }
    });

    $('.otpravit.moscow').bind('click', function(event) {
        event.preventDefault();
        $('#modal_region .buttons').fadeOut(400);
        setTimeout(function(){$('#modal_region .type').fadeIn(400)},400)
    });

    $('.otpravit.russia').bind('click', function(event) {
        event.preventDefault();
        $('#modal_region .buttons').fadeOut(400);
        setTimeout(function(){$('#modal_region .type2').fadeIn(400)},400)
    });

    $('.type a').bind('click', function(event) {
        event.preventDefault();
        var type = $(this).html();
        if ($('#modal_region input[name=company]').val() != '')
            window.location.href = "/competition/?type=" + encodeURIComponent(type) + "&company=" + $('#modal_region input[name=company]').val();
        else
            window.location.href = "/competition/?type=" + encodeURIComponent(type);
    });

    $('.type2 a.cur').bind('click', function(event) {
        event.preventDefault();
        var type = $(this).html();
        if ($('#modal_region input[name=company]').val() != '')
            window.location.href = "/tender/?type=courier&company=" + $('#modal_region input[name=company]').val();
        else
            window.location.href = "/tender/?type=courier";
    });

    $('.type2 a.transp').bind('click', function(event) {
        event.preventDefault();
        var type = $(this).html();
        if ($('#modal_region input[name=company]').val() != '')
            window.location.href = "/tender/?type=transport&company=" + $('#modal_region input[name=company]').val();
        else
            window.location.href = "/tender/?type=transport";
    });

    $('#id_from_phone, #id_to_phone').mask("+7 (999) 999-99-99");

    $('#id_cargo_0, #id_cargo_1').change(function(event) {
        $('#id_cargo_0, #id_cargo_1').parent().toggleClass('active');
    });

    $('.zone_name a').click(function(event) {
        event.preventDefault();
        if($(this).hasClass('opened')){
            $(this).parent().next().slideUp(200);
            $(this).removeClass('opened');
            $(this).html('показать');
        }
        else{
            $(this).parent().next().slideDown(200);
            $(this).addClass('opened');
            $(this).html('cкрыть');
        }
    });

    $('.dop_header').click(function(event) {
        event.preventDefault();
        if($(this).hasClass('opened')){
            $('.dop_info').slideUp(200);
            $(this).removeClass('opened');
        }
        else{
             $('.dop_info').slideDown(200);
            $(this).addClass('opened');
        }
    });


    $('input[name=payment_type]').change(function(event) {
        if($('#em').is(':checked')){
            $('ul.emoney').slideDown(400);
        }else{
            $('ul.emoney').slideUp(400);
            $('ul.emoney input[type=radio]').removeAttr('checked')
        }
    });

    $('.payment_type_form input[type=submit]').click(function(event) {
        event.preventDefault();
        var t = $(this);
        $.ajax({
            url: $(this).parent().attr('action'),
            type: 'POST',
            data: $(this).parent().serialize(),
            success: function(data){
                if(data.success == true){
                    $('<p class="success">Данные сохранены!</p>').insertBefore(t);
                    setTimeout(function(){$('p.success').remove()}, 3000);
                }
            }
        });
        
    });

    // $('.modal_rating').click(function(event){
    //     event.preventDefault();
    //     $('input[name=hidden_company_id]').val(($(this).attr('data-company')));
    //     $.fancybox.open({
    //         href: '#modal_rating',
    //         type: 'inline',
    //         beforeShow: function(){
                
    //         }
    //     });
    // });

    $('.modal_rating').fancybox({
        beforeShow: function(){
            var obj = $(this);
            var id = obj['0']['element']['rel'];
            $('input[name=hidden_company_id]').val(id);
        }
    });
});
