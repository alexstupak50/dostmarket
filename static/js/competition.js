function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var result;

    for(var i = 0; i < hashes.length; i++){
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(document).ready(function() {
    if( $.browser.opera ){
      /*alert( "You're using Opera version "+$.browser.version+"!" );*/
    }
    var company = getUrlVars()["company"];
    var qiwi_det, yad_det, bank_card_det, nal_det;
    var beznal_det = [];

    var d = new Date();
    day = d.getDate('dd');
    day = (day < 10 ? "0" : "") + day; 
    year = d.getFullYear();
    month = d.getMonth()+1;
    month = (month < 10 ? "0" : "") + month; 

    var date = year + '-' + month + '-' + day;

    $('#id_delivery_date').val(date);

    $('.button_step1').click(function(event) {
        event.preventDefault();
        $('.forms3 .errors').html('');
        $('body,html').animate({  //К body и html принимаем анимацию
            scrollTop:0  //Скрол на вверх к 0
        },1000);
        $('#tender input[type=text], #tender select, #tender input[type=email]').removeClass('error');
        if ($('input[name=payment_type]:checked').length == 0)
            $('.forms3 .errors').html('Вы не выбрали тип оплаты!');
        else{
            if ($('#em').is(':checked') && $('input[name=epayment_type]:checked').length == 0)
                $('.forms3 .errors').html('Вы не выбрали тип электронных денег для оплаты!');
            else{
                $.ajax({
                    type: 'POST',
                    url: $('#tender').attr('action'),
                    data: $('#tender').serialize(),
                    success: function(data){

                        if(!data.success){
                            $.each(data, function(index, val) {
                                $('#tender #id_' + index).addClass('error');
                            });
                        }
                        if(data.success){
                            if ($('input[name=epayment_type]:checked').length > 0){
                                var company_payment =  $('input[name=epayment_type]:checked').val();
                                $('input[name=payment_type]:checked').val($('input[name=epayment_type]:checked').val());
                            }
                            else
                                var company_payment =  $('input[name=payment_type]:checked').val();
                            var company_payment_key =  $('input[name=payment_type]:checked').attr('data-key');
                            if (company == undefined)
                                var url_params = 'weight='+$('#id_weight').val()+'&dist='+$('#city_dist').val() + '&payment_type=' + company_payment;
                            else
                                var url_params = 'company=' + company + '&weight=' + $('#id_weight').val() + '&dist='+$('#city_dist').val() + '&payment_type=' + company_payment;

                            if (date == $('#id_delivery_date').val())
                                $('#id_urgency').val('on');
                            else
                                $('#id_urgency').val('');

                            $.ajax({
                                type: 'POST',
                                url: '/ajax_get_company/',
                                data: url_params,
                                success: function(data){
                                    delivery_date = $('#id_delivery_date').val();
                                    delivery_time = $('#id_delivery_time').val();
                                    from_address = $('#id_from_address').val();
                                    from_fio = $('#id_from_fio').val();
                                    from_phone = $('#id_from_phone').val();
                                    to_address = $('#id_to_address').val();
                                    to_fio = $('#id_to_fio').val();
                                    to_phone = $('#id_to_phone').val();
                                    var weight = ($('#id_weight').val() == '11') ? 'ot10' : $('#id_weight').val();
                                    $('.step1').slideUp(400);

                                    var table_obj = $('.step2 .tab');
                                    var nn = 1;

                                    switch($('#city_dist').val()){
                                        case'moscow':
                                            var zone = 'zone1';
                                            break;

                                        case'mkad':
                                            var zone = 'zone2';
                                            break;

                                        case'mkad_5km':
                                            var zone = 'zone3';
                                            break;

                                        case'mkad_10km':
                                            var zone = 'zone4';
                                            break;

                                        case'mkad_25km':
                                            var zone = 'zone5';
                                            break;

                                        case'mkad_50km':
                                            var zone = 'zone6';
                                            break;

                                        case'mkad_100km':
                                            var zone = 'zone7';
                                            break;
                                    }

                                    var key = 'deliveryzoneprice__' + zone + '_' + weight + 'kg';

                                    $.each(data, function(index, item) {

                                        var table_cell_nn = $('<td>',{html: nn++});
                                        var table_cell_imag = $('<td>',{html: "<a href='/companies/"+item.type+"/"+item.pk+"/' class='read'> <img src='/media/"+item.logo+"' height='52' width='52' ></a>", class: 'imag'});
                                        var table_cell = $('<td>', {html: item.name});
                                        var table_cell_feedback = $('<td>', {html: "<a target='_blank' href='/companies/" + item.type + "/" + item.pk + "/#respondes'>Отзывы</a>", class: "companies_respondes"});
                                        if (item.deliveryzoneprice__urgency == null && item[key] == null){
                                            var table_cell_price = $('<td>', {html: 'нет данных', class: 'tot_price'});
                                            var table_cell_order = $('<td>', {html: "", class: 'center_tab or'});

                                            if(nn % 2 == 0){
                                                 var table_row = $('<tr>',{class: 'even'});
                                            }else{
                                                var table_row = $('<tr>',{class: 'odd'});
                                            }

                                        }else{

                                            if ($('#id_urgency').val() == 'on'){
                                                var table_cell_price = $('<td>', {html: item[key] + item.deliveryzoneprice__urgency, class: 'tot_price'});
                                            }
                                            else
                                                var table_cell_price = $('<td>', {html: item[key], class: 'tot_price'});
                                            var table_cell_order = $('<td>', {html: "<a href='#' class='order_icon'>Заказать</a>", class: 'center_tab or'});

                                            if(nn % 2 == 0){
                                                 var table_row = $('<tr>',{class: 'even'});
                                            }else{
                                                var table_row = $('<tr>',{class: 'odd'});
                                            }
                                        }

                                        table_row.append(table_cell_nn, table_cell_imag, table_cell, table_cell_feedback, table_cell_price, table_cell_order);
                                        table_obj.append(table_row);
                                    });

                                    $('.from_address span:not(.bold)').html(from_address+' ('+from_fio+')');
                                    $('.to_address span:not(.bold)').html(to_address+' ('+to_fio+')');
                                    $('.to_date span:not(.bold)').html(delivery_time+' '+delivery_date+' ('+to_phone+')');

                                    $('.step2').slideDown(400);

                                    $('.step2 .order_edit, .step2 .back').click(function(event) {
                                        event.preventDefault();

                                        $('.step2').slideUp(400);
                                        $('.step2 .tab tr').not('.first').remove();
                                        $('.step1').slideDown(400);
                                        $('#payment_details p').remove();
                                        $('.controls label').remove();
                                        $('.err p').remove();

                                        $('body').animate({  //К body и html принимаем анимацию
                                            scrollTop:220 //Скрол на вверх к 0
                                        },1000);
                                    });

                                    $('.order_icon').click(function(event) {
                                        event.preventDefault();
                                        var tr = $(this).parent().parent().html();
                                        var tot_price = parseInt($('.tot_price').text());
                                        $('#id_total_price').val(tot_price);
                                        tr = tr.replace('<td class="center_tab or"><a href="#" class="order_icon">Заказать</a></td>','');
                                        var com_href = $(this).parent().parent().find('.imag a').attr('href');
                                        var id_price = $(this).parent().parent().find('td').eq(-2).html();
                                        $('#id_price').val(id_price);
                                        com_arr = com_href.split('/');
                                        $.ajax({
                                            url: '/ajax_get_payment/'+com_arr[3]+'/'+ company_payment + '/',
                                            dataType: 'JSON',
                                            success: function(data){
                                            },
                                            error: function(data ,errorText) {
                                                console.log(errorText);
                                            }
                                        })
                                        .done(function(data) {
                                                $.each(data, function(key, val) {
                                                    if (val.payment__show_fields == true)
                                                        $('#payment_details').append('<p class="bold">' + val.payment__payment_type__name + '</p>');
                                                    else
                                                        $('#payment_details').append('<p class="bold">' + val.payment__payment_type__name + '</p><p>' + val.value + '</p>');
                                                });
                                        })
                                        
                                        
                                        $('#id_company').val(com_arr[3]);

                                        $('.step2').slideUp(400);

                                        $('.step4 .tab tr.odd').html(tr);
                                        if ($.browser.msie)
                                            $('.step4 .tab td.or').css('display', 'none');
                                        /*$('.step4 .tot_price').html(tot_price);*/
                                        $('.step4').slideDown(400);
                                        $('body').animate({ scrollTop:220 },500);

                                        $('.step4 .order_edit, .step4 .back').click(function(event) {
                                            event.preventDefault();
                                            $('#payment_details p').remove();
                                            $('.step4 tr .tot_price').remove();

                                            $('.step4').slideUp(400);
                                            $('.step2').slideDown(400);
                                            $('body').animate({ scrollTop:220 },500);
                                        });

                                    });
                                }
                            });
                        }
                    }
                });
            }
        }
    });

    $('.step4 .submit').click(function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/ajax_delivery_competition/',
            data: $('#tender').serialize(),
            success: function(data){
                url = '<a class="link" href="' + data.url + '">' + data.url + '</a>';
                $('.complite').html('<p>Спасибо за ваш заказ!</p><p>В течении 15 минут с вами свяжется оператор для уточнения деталей заказа. Обращаем ваше внимание, что цена, указанная на сайте может быть не окончательной. Цена может варьироваться в зависимости от условий вашего заказа. ' + url + '</p><p><a href="/" class="btn">На главную</a> </p>');
            }
        });
        $('body').animate({  //К body и html принимаем анимацию
            scrollTop:220 //Скрол на вверх к 0
        },1000);

    });
    

    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $('#id_delivery_date').datepicker({
        minDate: 0,
        dateFormat: "yy-mm-dd"
    });
});
