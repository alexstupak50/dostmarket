# jango settings for fosters project.
import os
import sys

gettext = lambda s: s
PROJECT_PATH = BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Adding modules directory to python path
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

DEBUG = True
TEMPLATE_DEBUG = DEBUG
ADMINS = (('alex', 'sergeydurov.info@gmail.com'), )

MANAGERS = ADMINS


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-ru'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/media/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"

STATIC_ROOT = os.path.join(PROJECT_PATH, "media/static")

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/media/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/media/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'static'),
)
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    'google.com',
    'maps.googleapis.com',
    'maps.googleapis.com/maps/api/place/autocomplete/json'
)
# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'django_ban.middleware.Ban',
    'order.middleware.TmpUsersMiddleware',
)

ROOT_URLCONF = 'urls'

os.path.join(PROJECT_PATH, "templates"),

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.formtools',
    'django.contrib.sitemaps',

    'smart_selects',
    'widget_tweaks',
    'django_crontab',
    'mailer',
    'adminplus',
    'geo',
    'cms',
    'mptt',
    'menus',
    'south',
    'colorful',
    'sekizai',
    'sorl.thumbnail',
    'cms.plugins',
    'cms.plugins.text',
    'cms.plugins.picture',
    'cms.plugins.file',
    'polymorphic',
    'cms.plugins.snippet',
    'news',
    'ckeditor',
    'registration',
    'registration_defaults',
    'company',
    'captcha',
    'pytils',
    'articles',
    'tinymce',
    'robots',
    'awards',
    'django_ban',
    'order',
    'fein_admin_menu',
    'comments',
    'cms_page',
    'email_mailer',
    'import_export',

)

CRONJOBS = [
    ('*/30 * * * *', 'order.cron.order_cancel_performer'),
    ('*/30 * * * *', 'order.cron.order_cancel'),
    ('0 */1 * * *', 'order.cron.order_reset_day_view'),
    ]


BAN_POLICY = 'deny,allow'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
    # 'news.context.news_block',
    # 'company.context.allCity',
    'company.context.new_orders',
    # 'company.context.respond_block',
    # 'company.context.top_company',
    # 'company.context.special_offer',
    'company.context.debug',
    'company.context.banners',
)

EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = 'info@dostmarket.ru'
EMAIL_HOST_PASSWORD = 'superdostmarket'
EMAIL_USE_TLS = True
SERVER_EMAIL = DEFAULT_FROM_EMAIL = EMAIL_CHANGE_FROM_EMAIL = 'info@dostmarket.ru'
# A_EMAIL = 'litewm@gmail.com'
A_EMAIL = SERVER_EMAIL
HOST = 'dostmarket.ru'
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
            },
        }
}
THUMBNAIL_KEY_PREFIX = 'thumb_key_prefix'
THUMBNAIL_PREFIX = 'thumb'

# if DEBUG:
#     # make all loggers use the console.
#     for logger in LOGGING['loggers']:
#         LOGGING['loggers'][logger]['handlers'] = ['console']

SOUTH_MIGRATION_MODULES = {
    'cities_light': 'cities_light.south_migrations',
    }

CMS_TEMPLATES = (
    ('base.html', 'Main Template'),
)

LANGUAGES = [
    ('ru', 'Russian'),
    ]

TEMPLATE_DIRS = (
    PROJECT_PATH + '/templates',
)


CMS_SEO_FIELDS = True

CMS_USE_TINYMCE = True

#comments
COMMENTS_ANONYMOUS = False

TINYMCE_DEFAULT_CONFIG = dict(
    theme = "advanced",
    plugins = """
safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,
inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,
noneditable,visualchars,nonbreaking,xhtmlxtras,template,""",

    theme_advanced_buttons1 = "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 = "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 = "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,|,fullscreen",
    theme_advanced_toolbar_location = "top",
    theme_advanced_toolbar_align = "left",
    theme_advanced_statusbar_location = "bottom",
    theme_advanced_resizing = True,

)


CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            [      'Undo', 'Redo',
                   '-', 'Bold', 'Italic', 'Underline',
                   '-', 'Link', 'Unlink', 'Anchor',
                   '-', 'Format',
                   '-', 'SpellChecker', 'Scayt',
                   '-', 'Maximize', 'Image',
                   ],
            [      'HorizontalRule',
                   '-', 'Table',
                   '-', 'BulletedList', 'NumberedList',
                   '-', 'Cut','Copy','Paste','PasteText','PasteFromWord',
                   '-', 'SpecialChar',
                   '-', 'Source',
                   '-', 'About',
                   ]
        ],
        'width': 840,
        'height': 300,
        'toolbarCanCollapse': False,
        },
    'simple': {
        'toolbar': [
            [      'Undo', 'Redo',
                   '-', 'Bold', 'Italic', 'Underline',
                   '-', 'Table',
                   '-', 'BulletedList', 'NumberedList','CharCount',
                   ],
            ],
        'width': 760,
        'MaxLength': 2500,
        'height': 300,
        'toolbarCanCollapse': False,
        },
    'simple2': {
        'toolbar': [
            [      'Undo', 'Redo',
                   '-', 'Bold', 'Italic', 'Underline',
                   '-', 'Table',
                   '-', 'BulletedList', 'NumberedList','CharCount','Link','Unlink','Anchor','Image',
                   ],
            ],
        'width': 760,
        'MaxLength': 2500,
        'height': 300,
        'toolbarCanCollapse': False,
        },
    'respond': {
        'toolbar': [
            [      'Undo', 'Redo', 'CharCount',
                   '-', 'Bold', 'Italic', 'Underline',
                   ],
            ],
        'width': 760,
        'MaxLength': 2500,
        'height': 90,
        'toolbarCanCollapse': False,
        }
}

ACCOUNT_ACTIVATION_DAYS = 2

CAPTCHA_OUTPUT_FORMAT = u'%(hidden_field)s %(text_field)s %(image)s'
CAPTCHA_FONT_SIZE = 25
# CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'
CAPTCHA_LENGTH = 5
CMS_MENU_TITLE_OVERWRITE = True
CAPTCHA_NOISE_FUNCTIONS = ['captcha.helpers.noise_dots']

CKEDITOR_MEDIA_PREFIX = "/media/static/ckeditor/"
CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'editor')

SESSION_COOKIE_AGE = 86400

AUTH_PROFILE_MODULE = 'company.Company'

try:
    LOCAL_SETTINGS
except NameError:
    try:
        from local_settings import *
    except ImportError:
        pass

