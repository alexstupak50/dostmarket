from fabric.api import *
# from ban_script import process_log

PROJECT_ROOT = '/home/dostmarket_new/dostmarket'
PROJECT_SOURCE = 'git@bitbucket.org:alexstupak50/dostmarket.git'

env.hosts = '194.28.132.165'
env.user = 'dostmarket_new'
env.password = '357Root10'

PYTHON_PATH = '/home/dostmarket_new/env_dostmarket/bin/python'

def test():
    with cd(PROJECT_ROOT):
        # run('set | sort > /tmp/fab_set')
        # local('echo $LANG')
        # run('echo $LANG')
        # with shell_env(PYTHONIOENCODING='utf-8'):
        #     with shell_env(LOCALE='ru_RU.UTF-8'):
        run('/home/test_dostmarket/env_dostmarket/bin/python manage.py collectstatic --noinput')


def ps(t=None):
    try:
        local('git add . -A')
    except:
        pass
    if t:
        local('git ci -m "%s"' % t)
    else:
        local('git ci')
    local('git push')


def pl(mig=False):

    with cd(PROJECT_ROOT):
        run('git pull origin master')
        try:
            run('{python} manage.py collectstatic --noinput'.format(python=PYTHON_PATH))
        except:
            print 'error collectstatic'
        if mig:
            run('{python} manage.py migrate'.format(python=PYTHON_PATH))
        run('touch touch_reload')


def pspl(mig=False):
    ps()
    pl(mig)

def ps_server():
    # run('git pull origin new')
    run('{python} dostmarket/manage.py collectstatic --noinput'.format(python=PYTHON_PATH))
    run('touch touch_reload')


def remove_massage_log():
    with cd(PROJECT_ROOT):
        run('{python} manage.py delete_messages'.format(python=PYTHON_PATH))


def ban():
    file = '/home/test_dostmarket/log/access.log'
    log_file = open(file, 'r')

    # process_log(log_file)
    sudo('python -c "open(\'%s\', \'w\').close()"' % file)
